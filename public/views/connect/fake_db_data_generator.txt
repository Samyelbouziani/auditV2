http://www.json-generator.com/# 
[
	{
		mandat: 
		[          
          '{{repeat(5, 7)}}',
          {         
            _id: '{{objectId()}}',    
            number: '{{index()}}',  
            name: 'Mandat',
            rum: '{{integer(2005, 2020)}}-{{integer(00000000, 99999999)}}',
            ics: 'FR{{integer(00000000000, 99999999999)}}',

            creditorName: '{{firstName()}} {{surname()}}',
            creditorAddress: '{{integer(100, 999)}} {{street()}}, {{city()}}, {{state()}}, {{integer(100, 10000)}}',

            debtorBankingInfo: {
                BIC: 'PSSTFRPPB',
                IBAN: 'FR{{integer(00000000000, 99999999999)}}'
              },
            debtorName: '{{firstName()}} {{surname()}}',
            debtorAddress: '{{integer(100, 999)}} {{street()}}, {{city()}}, {{state()}}, {{integer(100, 10000)}}',

            signatureInfo: {
                date: '{{date()}}',
                place: '{{street()}}',
				debtorSign: '{{objectId()}}',
                creditorSign: '{{objectId()}}'
            }               
          }
		],
      
		login: 
		[          
          '{{repeat(5, 7)}}',
          {         
            _id: '{{objectId()}}',    
            pubk: '{{integer(0000, 9999)}}',
            prik: '{{integer(0000, 9999)}}',
            type: '{{random("creditor", "debtor")}}'
          }
		]
      }
]