/**
 * Created by selbouziani on 03/04/2017.
 */
/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('BlockchainModel.factory', [])
        .factory('BlockchainModel',  ['httpServ',
            function BlockchainModelFactory(httpServ) {

                var hexaTab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'];
                var instance = {
                    data: {
                        ListOfBlocks: null,
                        ListOfBlocksBk: null,
                        currentBlocksNumber: null
                    },
                    global: {
                        isFabricListening: false,
                        random_hex_generator: random_hex_generator,
                        random_int_generator: random_int_generator,
                        getBlockIndexByNumber: getBlockIndexByNumber,
                        getAppropriateBlockchain: getAppropriateBlockchain,
                        initBlockchain: initBlockchain,
                        updateLocalStorage: updateLocalStorage
                    },
                    offline: {
                        initBlockchainOffline: initBlockchainOffline,
                        addBlock: addBlock,
                        addTransaction: addTransaction,
                        getBlocksDb: getBlocksDb
                    },
                    online: {
                        initBlockchainFromServer: initBlockchainFromServer,
                        addBlockFromServer: addBlockFromServer,
                        addTransactionFromServer: addTransactionFromServer
                    }
                };

                /********************************************************
                                        GLOBAL
                 ********************************************************/

                function random_hex_generator() {
                    var e = "0x";
                    for (var i = 0; i < 24; i++) {
                        var rand = hexaTab[Math.floor(Math.random() * hexaTab.length)];
                        e = e + rand.toString();
                    }
                    return e;
                }

                function random_int_generator() {
                    return Math.floor(Math.random() * (5555555 - 111111 + 1)) + 111111;
                }

                function getBlockIndexByNumber (blockNum) {
                    for (var i = 0; i < instance.data.ListOfBlocks.length; i++) {
                        if (instance.data.ListOfBlocks[i].blockNumber === blockNum) {
                            return i;
                        }
                    }
                    return null;
                }

                function getAppropriateBlockchain (_currentUser) {
                    if ((_currentUser.typeOfUser === "creditorBank")
                        || (_currentUser.typeOfUser === "debitorBank")) {
                        instance.data.ListOfBlocks = instance.data.ListOfBlocksBk;
                    } else {
                        var blockchainSize = 0, publicKey = _currentUser.publicKey;
                        instance.data.ListOfBlocks = [];
                        instance.data.ListOfBlocks.push(instance.data.ListOfBlocksBk[0]);
                        //blockchainSize++;
                        for (var i = 1; i < instance.data.ListOfBlocksBk.length; i++) {
                            //for (var j = 0; j < instance.data.ListOfBlocksBk[i].transaction.length; j++) {
                            // Block isn't purple anymore
                            instance.data.ListOfBlocksBk[i].new = false;
                            if ((instance.data.ListOfBlocksBk[i].transaction.length !== 0)
                             && ((publicKey === instance.data.ListOfBlocksBk[i].transaction[0].from)
                             || (publicKey === instance.data.ListOfBlocksBk[i].transaction[0].to))) {
                                var BlockToCopy = _.clone(instance.data.ListOfBlocksBk[i]);
                                instance.data.ListOfBlocks.push(BlockToCopy);
                                // Copying Block information
                                /*if (instance.data.ListOfBlocks[blockchainSize-1].hash != instance.data.ListOfBlocksBk[i].hash) {
                                 var BlockToCopy = _.clone(instance.data.ListOfBlocksBk[i]);// = items.slice(i, i+1);
                                 BlockToCopy.transaction = [];
                                 blockchainSize++;
                                 instance.data.ListOfBlocks.push(BlockToCopy);
                                 }*/
                                // Copying Transaction information
                                /*var TransactionToCopy = instance.data.ListOfBlocksBk[i].transaction[0];
                                 instance.data.ListOfBlocks[blockchainSize-1].transaction.push(TransactionToCopy);*/
                            }
                            //}
                        }
                    }
                    return instance.data.ListOfBlocks;
                }

                function initBlockchain () {
                    if (instance.global.isFabricListening) {
                        var _blockchain = instance.online.initBlockchainFromServer()
                        .then(function (resp) {
                            console.log("resp", resp);
                            return resp;
                        }, function (err) {
                            console.log(err);
                        });
                        return _blockchain;
                    } else {
                        return instance.offline.initBlockchainOffline();
                    }
                    //if connected : initBlockchainFromServer
                    //else initBlockchainOffline
                }

                function updateLocalStorage () {
                    // ListOfBlocks empty
                    if ((instance.data.ListOfBlocks === null)
                        || (instance.data.ListOfBlocks === undefined)) {
                        instance.data.ListOfBlocks = instance.offline.getBlocksDb();
                    }

                    // LocalStorage empty
                    if ((localStorage.getItem('Blockchain') === null)
                        || (localStorage.getItem('Blockchain') === "null")
                        || (localStorage.getItem('Blockchain') === undefined)) {
                        localStorage.setItem('Blockchain', JSON.stringify(instance.data.ListOfBlocks));
                    }
                    var previousList = JSON.parse(localStorage.getItem('Blockchain'));
                    var blockFounded = false;
                    for (var i = 0; i < instance.data.ListOfBlocks.length; i++) {
                        for (var j = 0; j < previousList.length; j++) {
                            // If modified
                            if (previousList[j].blockNumber == instance.data.ListOfBlocks[i].blockNumber) {
                                blockFounded = true;
                                previousList[j] = instance.data.ListOfBlocks[i];
                                break;
                            }
                        }
                        // If doesn"t exist
                        if (blockFounded === false) {
                            previousList.push(instance.data.ListOfBlocks[i]);
                        }
                        blockFounded = false;
                    }
                    //instance.data.ListOfBlocks = previousList;
                    instance.data.ListOfBlocksBk = previousList;
                    instance.data.currentBlocksNumber = instance.data.ListOfBlocks.length;
                    localStorage.removeItem('Blockchain');
                    localStorage.setItem('Blockchain', JSON.stringify(previousList));
                }

                /********************************************************
                                        OFFLINE
                 ********************************************************/

                function initBlockchainOffline() {
                    instance.data.ListOfBlocks = getBlocksDb();
                    instance.data.currentBlocksNumber = instance.data.ListOfBlocks.length;
                    instance.global.updateLocalStorage();
                    return instance.data;
                }

                function addBlock(data, type) {
                    data['function'] = type ;
                    var date = new Date();
                    var _date = new Date(date.getTime() + (2*1000*60*60)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    instance.data.ListOfBlocks[instance.data.ListOfBlocks.length] = ({
                        blockNumber: instance.data.ListOfBlocksBk.length,
                        hash: random_hex_generator(),
                        previous_hash: random_hex_generator(),
                        nonce: random_hex_generator(),
                        transaction_root: random_hex_generator(),
                        timestamp: _date,
                        showBlockContent: false,
                        showBlockInfo: false,
                        showTransactionsInfo: false,
                        new: true,
                        transaction: [{
                            transactionNumber: 0,
                            hash: random_hex_generator(),
                            nonce: random_hex_generator(),
                            from: data.from,
                            to: data.to,
                            chain_id: random_hex_generator(),
                            data: data
                        }]
                    });
                    //console.log(instance.data.ListOfBlocks[instance.data.ListOfBlocks.length])
                    instance.global.updateLocalStorage();
                }

                // unused
                function addTransaction(data, type) {
                    //console.log("add block transaction");
                    data['function'] = type ;
                    instance.data.ListOfBlocks[instance.data.ListOfBlocks.length - 1]['new'] = true ;
                    instance.data.ListOfBlocks[instance.data.ListOfBlocks.length - 1].transaction.push({
                        transactionNumber: instance.data.ListOfBlocks[instance.data.ListOfBlocks.length - 1].transaction.length - s1,
                        hash: random_hex_generator(),
                        nonce: random_hex_generator(),
                        from: data.from,
                        to: data.to,
                        chain_id: random_hex_generator(),
                        data: data
                    });
                    instance.global.updateLocalStorage();
                }


                function getBlocksDb() {
                    return [{
                            blockNumber: 0,
                            hash: "0x" + "58e35aa1ce977d138485566f",
                            previous_hash: "0x" + "000000000000000000000000",
                            nonce: "0x" + "58e35aa17849027fda81c75f",
                            transaction_root: "0x" + "58e35aa107142b02d5e5244b",
                            timestamp: "2016-05-12 16:26:55",
                            showBlockContent: false,
                            showBlockInfo: false,
                            showTransactionsInfo: false,
                            transaction: [{
                                transactionNumber: 0,
                                hash: "0x" + "58e35aa1d58fe1716286ba3b",
                                nonce: "0x" + "58e35aa19818fbc70d025d2c",
                                from: "0x" + "000000000000000000000000",
                                to: "0x" + "000000000000000000000000",
                                chain_id: "0x" + "000000000000000000000000",
                                showTransaction: false,
                                data: {  
                                    function: "Genesis block"
                                }
                            }]
                        }, {
                                blockNumber: 1,
                                hash: "0x58e35aa1c77daed2eca5b67a",
                                previous_hash: "0x58e35aa1ce977d138485566f",
                                nonce: "0x" + "58e35aa115a00b157b1d40c7",
                                transaction_root: "0x" + "58e35aa1622aead5e2d962bf",
                                timestamp: "2016-07-21 11:43:12",
                                showBlockContent: false,
                                showBlockInfo: false,
                                showTransactionsInfo: false,
                                transaction: [{
                                transactionNumber: 0,
                                hash: "0x" + "58e35aa1236509a5d521b",
                                nonce: "0x" + "58e35aa13772d0795c263839",
                                from: "58f87b7a65f4d8afab2ecf25",
                                to: "58f87b7a87c9f2992b6ac5c7",                                
                                chain_id: "0x" + "58e35aa1abfd8890ac90648a",
                                showTransaction: false,
                                data: { 
                                    function: "Initiate Mandate" ,
                                    rum: "2015-3604819",
                                    ics: "FR36744498634",
                                    amount:"38",
                                    periodicity:"Monthly",
                                    typeOfPayment:"Recurring",
                                    owner:"Bank - BNP Paribas",
                                    creditorName:"Orange",
                                    creditorAddress:"483 Polhemus Place, Hiseville, Rhode Island, 601",
                                    creditorPublicKey:"58f87b7a65f4d8afab2ecf25",
                                    debitorPublicKey:"58f87b7a87c9f2992b6ac5c7",
                                    location:"Paris",
                                    bic:"MPZBTAHRPDG",
                                    MandateID:"84a06d0d2d2a91134ba861dd46de060c2c38e23442980fdc6618e4c766352e8c"
                                }
                            }]
                        },{
                                blockNumber: 2,
                                hash: "0x" + "58e35aa1a4be211d2910fa29",
                                previous_hash: "0x58e35aa1c77daed2eca5b67a",
                                nonce: "0x" + "58e35aa115a00b157b1d40c7",
                                transaction_root: "0x" + "58e35aa1622aead5e2d962bf",
                                timestamp: "2016-07-11 14:02:42",
                                showBlockContent: false,
                                showBlockInfo: false,
                                showTransactionsInfo: false,
                                transaction: [{
                                transactionNumber: 0,
                                hash: "0x" + "58e35aa1236509a5d521b",
                                nonce: "0x" + "58e35aa13772d0795c263839",
                                from: "58f87b7a65f4d8afab2ecf25",
                                to: "58f87b7a7bca47239dba9a4a",                                
                                chain_id: "0x" + "58e35aa1abfd8890ac90648a",
                                showTransaction: false,
                                data: { 
                                    function: "Initiate Mandate" ,
                                    rum: "2012-51759811",
                                    ics: "FR36744498634",
                                    amount:"56",
                                    periodicity:"Monthly",
                                    typeOfPayment:"Recurring",
                                    owner:"Creditor 1 - Orange",
                                    creditorName:"Orange",
                                    creditorAddress:"483 Polhemus Place, Hiseville, Rhode Island, 601",
                                    creditorPublicKey:"58f87b7a65f4d8afab2ecf25",
                                    debitorPublicKey:"58f87b7a7bca47239dba9a4a",
                                    location:"Paris",
                                    bic:"MPZBTAHRPDG",
                                    MandateID:"185d3743b734171eda22ccd5f8cc11d9494c08275bf83236918ff4a772c90ab3"
                                }
                            }]
                        }, {
                            blockNumber: 3,
                            hash: "0x" + "58e35aa1abfd8890ac90648a",
                            previous_hash: "0x58e35aa1a4be211d2910fa29",
                            nonce: "0x" + "58e35aa1a28db13af4ad5ec3",
                            transaction_root: "0x" + "58e35aa1544084e303de2ec4",
                            timestamp: "2016-08-12 7:03:21",
                            showBlockContent: false,
                            new: true,
                            showBlockInfo: false,
                            showTransactionsInfo: false,
                            transaction: [{
                                transactionNumber: 0,
                                hash: "0x" + "58e35aa1171e3a0555af73c3",
                                nonce: "0x" + "58e35aa13e8abaa1eb756dab",
                                from: "58f87b7a87c9f2992b6ac5c7",
                                to: "58f87b7a65f4d8afab2ecf25",                                                                
                                chain_id: "0x" + "58e35aa1144d571ef5e37565",
                                showTransaction: false,
                                data: {
                                    function: "Fill Mandate",
                                    creditorPublicKey:"58f87b7a65f4d8afab2ecf25",
                                    debitorPublicKey:"58f87b7a87c9f2992b6ac5c7",
                                    BIC:"APOELTVZAPO",
                                    IBAN:"FR49000869799",
                                    MandateID:"84a06d0d2d2a91134ba861dd46de060c2c38e23442980fdc6618e4c766352e8c",
                                    debitorAddress:"886 Henderson Walk, Homeland, Virginia, 427",
                                    debitorName: "Letitia Daugherty",
                                    debitorBIC: "APKELTVZAPO",
                                    debitorIBAN: "FR49000869799"
                                }
                            }]
                        }
                    ];
                }

                /********************************************************
                                          ONLINE
                 ********************************************************/

                function initBlockchainFromServer (currentUser) {
                    console.log("initBlockchainFromServer");
                    var result_request = httpServ.getEveryBlock().then(
                        (results) => {
                            if (results === undefined)  results = "";
                            instance.data.ListOfBlocksBk = transformListBlock(results);
                            instance.data.ListOfBlocks = instance.global.getAppropriateBlockchain(currentUser);
                            instance.data.currentBlocksNumber = instance.data.ListOfBlocks.length;
                            console.log(instance.data.ListOfBlocks);
                            console.log("instance.data.ListOfBlocks")
                            return instance.data;
                        },
                        (err) => {
                            console.log("Error in getEveryBlock ");    
                            return instance.data;
                        }
                    );
                    return result_request;
                }


                function transformListBlock (FabricListOfBlocks) {
                    var formatedListOfBlocks = []; 

                    formatedListOfBlocks.push({
                        blockNumber: 0,
                        hash: "0x" + "58e35aa1ce977d138485566f",
                        previous_hash: "0x" + "000000000000000000000000",
                        nonce: "0x" + "58e35aa17849027fda81c75f",
                        transaction_root: "0x" + "58e35aa107142b02d5e5244b",
                        timestamp: "2016-05-12 16:26:55",
                        showBlockContent: false,
                        showBlockInfo: false,
                        showTransactionsInfo: false,
                        transaction: [{
                            transactionNumber: 0,
                            hash: "0x" + "58e35aa1d58fe1716286ba3b",
                            nonce: "0x" + "58e35aa19818fbc70d025d2c",
                            from: "0x" + "000000000000000000000000",
                            to: "0x" + "000000000000000000000000",
                            chain_id: "0x" + "000000000000000000000000",
                            showTransaction: false,
                            data: {  
                                function: "Genesis block"
                            }
                        }]
                    });

                    for(var i = 0 ; i < FabricListOfBlocks.length ; i ++) { 
                        //console.log(FabricListOfBlocks[i].transactions[0].actions[0].proposalResponse.response.payload);

                        var blocktmp = {
                            blockNumber: i+1,
                            hash: FabricListOfBlocks[i].data_hash,
                            previous_hash: FabricListOfBlocks[i].previous_hash,
                            nonce: "none",
                            transaction_root: "0x" + "58e35aa1544084e303de2ec4",
                            timestamp: FabricListOfBlocks[i].transactions[0].timestamp,
                            showBlockContent: false,
                            new: true,
                            showBlockInfo: false,
                            showTransactionsInfo: false,
                            transaction: []
                        };

                        var block = transformBlock(blocktmp, FabricListOfBlocks[i]);
                        
                        formatedListOfBlocks[i+1] = _.clone(block);
                        //formatedListOfBlocks[i] = tmp ; 
                    }
                    return formatedListOfBlocks;
                }

                function transformBlock (blockToUpdate, blockToCopy) {
                    // TODO : Updates functionCalled with next commit
                    var functionArgs = blockToCopy.transactions[0].actions[0].proposal.args;
                    var functionCalled = blockToCopy.transactions[0].actions[0].proposal.args["Function called"];
                    var payload = blockToCopy.transactions[0].actions[0].proposalResponse.response.payload;
                    var _payload;
                    // console.log("payload.DebitorPublicKey : ", payload.DebitorPublicKey);

                    if (payload !== "") {
                        _payload = JSON.parse(payload);
                    } else {
                        console.log("payload empty");
                    }
                    
                    if (functionCalled === "createMandate") {
                        blockToUpdate.transaction = [{
                            transactionNumber: 0,
                            hash: blockToCopy.transactions[0].tx_id,
                            nonce: "none",
                            from: functionArgs["Creditor Public Key"],
                            to: functionArgs["Debtor Public Key"],                                          
                            chain_id: "SEPAMandateChaincode",
                            showTransaction: false,
                            data: {
                                function: "Initiate Mandate",
                                debitorPublicKey: functionArgs["Debtor Public Key"],
                                creditorPublicKey: functionArgs["Creditor Public Key"],
                                ics: functionArgs["ICS"],
                                bic: functionArgs["Creditor BIC"],
                                creditorName: functionArgs["Creditor Name"],
                                creditorAddress: functionArgs["Creditor Address"],
                                owner: functionArgs["Mandate Manager"],
                                rum: functionArgs["RUM"],
                                amount: functionArgs["Amount"],
                                periodicity: functionArgs["Periodicity"],
                                location: functionArgs["Location"],
                                typeOfPayment: functionArgs["Periodicity"] != "Once" ? "Recurring" : "Non-recurring"
                                //MandateID:"185d3743b734171eda22ccd5f8cc11d9494c08275bf83236918ff4a772c90ab3"                            
                            }
                        }];

                    } else if (functionCalled === "debitorFillMandate") {
                        blockToUpdate.transaction = [{
                            transactionNumber: 0,
                            hash: blockToCopy.transactions[0].tx_id,
                            nonce: "none",
                            from: functionArgs["Debtor Public Key"],
                            to: functionArgs["Creditor Public Key"],                                                               
                            chain_id: "SEPAMandateChaincode",
                            showTransaction: false,
                            data: {
                                function: "Fill Mandate",
                                MandateID: functionArgs["Mandate ID"],
                                IBAN: functionArgs["Debtor IBAN"],
                                BIC: functionArgs["Debtor BIC"],
                                debitorName: functionArgs["Debtor Name"],
                                debitorAddress: functionArgs["Debtor Address"]
                            }
                        }];

                    } else if (functionCalled === "debitorAmendMandate") {
                        blockToUpdate.transaction = [{
                            transactionNumber: 0,
                            hash: blockToCopy.transactions[0].tx_id,
                            nonce: "none",
                            from: functionArgs["Debtor Public Key"],
                            to: functionArgs["Creditor Public Key"],                                                               
                            chain_id: "SEPAMandateChaincode",
                            showTransaction: false,
                            data: {
                                function: "Amend Mandate",
                                MandateID: functionArgs["Mandate ID"],
                                IBAN: functionArgs["Debtor IBAN"],
                                BIC: functionArgs["Debtor BIC"],
                                debitorAddress: functionArgs["Debtor Address"]
                            }
                        }];


                    } else if (functionCalled === "revokeMandate") {
                        blockToUpdate.transaction = [{
                            transactionNumber: 0,
                            hash: blockToCopy.transactions[0].tx_id,
                            nonce: "none",
                            from: functionArgs["Debtor Public Key"],
                            to: functionArgs["Creditor Public Key"],                                                               
                            chain_id: "SEPAMandateChaincode",
                            showTransaction: false,
                            data: {
                                function: "Revoke Mandate",
                                MandateID: functionArgs["Mandate ID"]
                            }
                        }];

                    } else {
                        console.log("Shouldn't happen");
                    }
                    

                    return blockToUpdate;
                }

                function addBlockFromServer () {
                    // TODO
                }

                function addTransactionFromServer () {
                    // TODO
                }

                return instance;
            }
        ]);
}(angular));