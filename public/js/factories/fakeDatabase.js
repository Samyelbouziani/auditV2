/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('FakeDatabase.factory', [])
        .factory('FakeDatabase', [
            function FakeDatabaseFactory() {

                var getDb = function getDb() {

                       return [{
                            mandat: [{
                                Mandate: {
                                    MandateID: "185d3743b734171eda22ccd5f8cc11d9494c08275bf83236918ff4a772c90ab3",
                                    Status: "created",
                                    CreditorPublicKey: "58f87b7a65f4d8afab2ecf25",
                                    DebitorPublicKey: "58f87b7a7bca47239dba9a4a",
                                    MandateData: {
                                        Rum: "2012-51759811",
                                        Timestamp: "2016-07-11 14:02:42",
                                        Location: "Paris",
                                        Amount: "56",
                                        Periodicity: "Monthly",
                                        TypeOfPayment: "Recurring",
                                        Owner: "Creditor 1 - Orange"
                                    },
                                    CreditorData: {
                                        Ics: "FR36744498634",
                                        Name: "Orange",
                                        Address: "483 Polhemus Place, Hiseville, Rhode Island, 601",
                                        Iban: "FR43020868274",
                                        Bic: "MPZBTAHRPDG"
                                    },
                                    DebitorData: {
                                        Name: "",
                                        Address: "",
                                        Iban: "",
                                        Bic: ""
                                    }
                                },
                                lastChaincodeFunctionCalled: 'initiate',
                                infoBank: false,
                                showInfo: false,
                                editMandat: false,
                                infoBlockchain: false
                            }, {
                                Mandate: {
                                    MandateID: "84a06d0d2d2a91134ba861dd46de060c2c38e23442980fdc6618e4c766352e8c",
                                    Status: "validated",
                                    CreditorPublicKey: "58f87b7a65f4d8afab2ecf25",
                                    DebitorPublicKey: "58f87b7a87c9f2992b6ac5c7",
                                    MandateData: {
                                        Rum: "2007-3604819",
                                        Timestamp: "2016-08-12 7:03:21",
                                        Location: "Paris",
                                        Amount: "38",
                                        Periodicity: "Monthly",
                                        TypeOfPayment: "Recurring",
                                        Owner: "Bank - BNP Paribas"
                                    },
                                    CreditorData: {
                                        Ics: "FR36744498634",
                                        Name: "Orange",
                                        Address: "483 Polhemus Place, Hiseville, Rhode Island, 601",
                                        Iban: "FR43020868274",
                                        Bic: "MPZBTAHRPDG"
                                    },
                                    DebitorData: {
                                        Name: "Letitia Daugherty",
                                        Address: "886 Henderson Walk, Homeland, Virginia, 427",
                                        Iban: "FR49000869799",
                                        Bic: "APKELTVZAPO"
                                    }
                                },
                                lastChaincodeFunctionCalled: 'initiate',
                                infoBank: false,
                                showInfo: false,
                                editMandat: false,
                                infoBlockchain: false
                            }],
                            login: [{
                                _id: "58cc07eae7e7f5ea129df893",
                                pubk: 1866,
                                prik: 6550,
                                type: "debitor"
                            }, {
                                _id: "58cc07ea3bc6573c2d7acf8b",
                                pubk: 3279,
                                prik: 284,
                                type: "debitor"
                            }, {
                                _id: "58cc07eaf3fce1d11ec52b93",
                                pubk: 6712,
                                prik: 3839,
                                type: "creditor"
                            }, {
                                _id: "58cc07ea33b7ea83bee0669f",
                                pubk: 536,
                                prik: 4268,
                                type: "debitor"
                            }, {
                                _id: "58cc07eab976908f8f061e46",
                                pubk: 3917,
                                prik: 9150,
                                type: "debitor"
                            }, {
                                _id: "58cc07ea2bb939bfed7b1170",
                                pubk: 4106,
                                prik: 4993,
                                type: "creditor"
                            }, {
                                _id: "58cc07ea870c655261af0bce",
                                pubk: 6851,
                                prik: 9034,
                                type: "creditor"
                            }]
                        }];
                    },
                    // unused
                    getMandatesList = function getMandatesList() {
                        return [{
                            name: "Mandate ",
                            MandateID: 0,
                            Rum: 6789670,
                            Ics: "iecn8909Fyv89",
                            Iban: "nizck",
                            Timestamp: "01/02/2011",
                            location: "Paris",
                            showData: false
                        }, {
                            name: "Mandate ",
                            MandateID: 1,
                            Rum: 6789670,
                            Ics: "iecn8909Fyv89",
                            Iban: "nizck",
                            Timestamp: "01/02/2011",
                            location: "Paris",
                            showData: false
                        }, {
                            name: "Mandate ",
                            MandateID: 2,
                            Rum: 6789670,
                            Ics: "iecn8909Fyv89",
                            Iban: "nizck",
                            Timestamp: "01/02/2011",
                            location: "Paris",
                            showData: false
                        }];
                    },
                    getEmptyMandate = function getEmptyMandate() {
                        var mandateId = random_int_generator();
                        return [{
                                Mandate: {
                                    MandateID: "",
                                    Status: "",
                                    CreditorPublicKey: "",
                                    DebitorPublicKey: "",
                                    MandateData: {
                                        Rum: "",
                                        Timestamp: "",
                                        Location: "",
                                        Amount: "",
                                        Periodicity: "",
                                        TypeOfPayment: "",
                                        Owner: ""
                                    },
                                    CreditorData: {
                                        Ics: "",
                                        Name: "",
                                        Address: "",
                                        Iban: "",
                                        Bic: ""
                                    },
                                    DebitorData: {
                                        Name: "",
                                        Address: "",
                                        Iban: "",
                                        Bic: ""
                                    }
                                },
                                lastChaincodeFunctionCalled: '',
                                infoBank: false,
                                showInfo: false,
                                editMandat: false,
                                infoBlockchain: false
                            }];
                    },
                    getUsersSetDb = function getUsersSetDb() {
                        return [{
                            id: 0,
                            typeOfUser: "debitor",
                            userText: "Debtor 1 (Orange)",
                            publicKey: "58f87b7a7bca47239dba9a4a",
                            Name: "Stacy Aguilar",
                            Address: "698 Raleigh Place, Sehili, California, 7652",
                            Iban: "FR25411473752",
                            Bic: "AZFGKILODVY"
                        }, {
                            id: 1,
                            typeOfUser: "debitor",
                            userText: "Debtor 2 (Orange)",
                            publicKey: "58f87b7a87c9f2992b6ac5c7",
                            Name: "Letitia Daugherty",
                            Address: "886 Henderson Walk, Homeland, Virginia, 427",
                            Iban: "FR49000869799",
                            Bic: "APKELTVZAPO"
                        }, {
                            id: 2,
                            typeOfUser: "creditor",
                            userText: "Creditor 1 (Orange)",
                            publicKey: "58f87b7a65f4d8afab2ecf25",
                            Ics: "FR36744498634",
                            Name: "Orange",
                            Address: "483 Polhemus Place, Hiseville, Rhode Island, 601",
                            Iban: "FR43020868274",
                            Bic: "MPZBTAHRPDG"
                        }, {
                            id: 3,
                            typeOfUser: "creditorBank",
                            userText: "Bank (BNP Paribas)",
                            publicKey: "16oZmpFVHpXVgyegWYXg4zNFhXVxYJemmY",
                            Ics: "FR71111878898",
                            Name: "BNP Paribas",
                            Address: "19 Place des Reflets, 92400 Courbevoie",
                            Iban: "FR28639075260",
                            Bic: "AKOELEHABUI"
                        }];
                    },

                    getMandatesByDebitorPK = function getMandatesByDebitorPK(DebitorPublicKey, listOfMandates) {
                        var completeList = listOfMandates;
                        //console.log(mandat[0].Mandate.DebitorPublicKey)
                        var list =[]; 
                        var j=0;
                        for(var i = 0; i < completeList.length ; i ++ ){
                            //console.log("Mandate.DebitorPK :" +completeList[i].Mandate.DebitorPublicKey+"FF");
                            //console.log("Args :" + DebitorPublicKey+"FF");
                            if(completeList[i].Mandate.DebitorPublicKey == DebitorPublicKey) {
                               list[j] = completeList[i] ;
                               j++;
                            }
                        }
                        //console.log(list);
                        return list ;
                    },

                    getMandatesByCreditorPK = function getMandatesByCreditorPK(CreditorPublicKey, listOfMandates) {
                        var completeList = listOfMandates;
                        var list = [];
                        var j=0;
                        for(var i = 0; i < completeList.length ; i ++ ){
                            if(completeList[i].Mandate.CreditorPublicKey == CreditorPublicKey) {
                               list[j] = completeList[i] ;
                               j++;
                            }
                        }
                        return list ;
                    },

                    getMandatesById = function getMandatesById(MandateID, listOfMandates) {
                        var mandat = listOfMandates;
                        var list ; 
                        var j=0;

                        for(var i = 0; i < mandat.length ; i ++ ){
                            if(mandat[i].Mandate.MandateID == MandateID) {
                               list[j] = mandat[MandateID] ;
                               j++;
                            }
                        }
                        return list ;
                    };

                    function random_int_generator() {
                        return Math.floor(Math.random() * (5555555 - 111111 + 1)) + 111111;
                    }
                
                return {
                    'getDb': getDb,
                    'getMandatesList': getMandatesList,
                    'getEmptyMandate': getEmptyMandate,
                    'getUsersSetDb': getUsersSetDb,
                    'getMandatesByCreditorPK': getMandatesByCreditorPK,
                    'getMandatesByDebitorPK': getMandatesByDebitorPK,
                    'getMandatesById': getMandatesById
                };
            }
        ]);
}(angular));