/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('Home.factory', [])

  .factory('HomeModel', [ '$window', '$q', '$mdDialog', 'FakeDatabase', 'testServerRoute', 'httpServ',
    function HomeModel($window, $q, $mdDialog, FakeDatabase, testServerRoute, httpServ) {

        var instance = {
            data: {
                isServerListening: false,
                currentMandateSelected: null,
                currentUser: null,
                ListOfMandates: null,
                ListOfMandatesBk: null,
                ListOfUsers: null,
                showFunctionnality: {
                    userType: "",
                    showInitMandate: false,
                    showFillMandate: false,
                    showAmendMandate: false,
                    showRevokeMandate: false,
                    showAddMandate: false,
                    showDialogAddMandate: false,
                    showDebitorData: false,
                    showCreditorData: false,
                    showBankData: false
                },
                mandateToggleButtons: {},
                boolAddMandate: false,
                dialogStatus: ""
            },
            /********************************************************
                                Web View functions
            ********************************************************/
            global: {
                checkIfServerConnected: checkIfServerConnected,
                initView: initView,
                updateLocalStorage: updateLocalStorage,
                updateCurrentUser: updateCurrentUser,
                updateAccessRoles: updateAccessRoles,
                hideAll: hideAll,
                showInfo: showInfo,
                bankInfo: bankInfo,
                blockchainInfo: blockchainInfo,
                editMandat: editMandat,
                mandateContentAnimation: mandateContentAnimation,
                resetMandateToggleButtons: resetMandateToggleButtons,
                updateMandateToggleButtons: updateMandateToggleButtons,
                getMandateIndex: getMandateIndex,
                addSettingsToMandate: addSettingsToMandate,
                setupTypeOfPayment: setupTypeOfPayment
            },
            /********************************************************
                                Dialog functions
             ********************************************************/
            offline: {
                updateInstanceInitiateMandate: updateInstanceInitiateMandate,
                updateInstanceFillMandate: updateInstanceFillMandate,
                updateInstanceAmendMandate: updateInstanceAmendMandate,
                updateInstanceRevokeMandate: updateInstanceRevokeMandate
            },
            online: {
                updateServerInitiateMandate: updateServerInitiateMandate,
                updateServerFillMandate: updateServerFillMandate,
                updateServerAmendMandate: updateServerAmendMandate,
                updateServerRevokeMandate: updateServerRevokeMandate
            },
            utils: {
                hexGenerator: hexGenerator
            }
        };

        /********************************************************
                                Web View functions
         ********************************************************/

        function checkIfServerConnected() {
            var resultat = httpServ.testServer()
            .then(function successCallback(response) {
                //console.log("checkIfServerConnected success : ");
                //console.log(response.data);
                if (response.data == true) {
                    instance.data.isServerListening = true;
                } else {
                    instance.data.isServerListening = false;
                }
                return response.data;
            }, function errorCallback(response) {
                console.log("checkIfServerConnected error : ");
                console.log(response);
                instance.data.isServerListening = false;
                return response.data;
            });
        }

        function initView () {
            checkIfServerConnected();
            var list;
            if (instance.data.isServerListening) {
                console.log("ONLINE : getMandate  ");

                // If we are a debitor
                if (instance.data.currentUser.typeOfUser == "debitor") {
                    list = httpServ.getMandatesByDebitorPK({pubk:instance.data.currentUser.publicKey})
                        .then(getMandatesSucessCallback, getMandatesErrorCallback);

                    // If we are a creditor
                } else if(instance.data.currentUser.typeOfUser == "debitor") {
                    list = httpServ.getMandatesByCreditorPK({pubk:instance.data.currentUser.publicKey})
                        .then(getMandatesSucessCallback, getMandatesErrorCallback);

                    // If we are a bank
                } else {
                    // TODO : Implement a specific function for the bank
                    list = httpServ.getMandatesByCreditorPK({pubk:"58f87b7a65f4d8afab2ecf25"})
                        .then(getMandatesSucessCallback, getMandatesErrorCallback);
                }
            } else {
                console.log("OFFLINE : getMandate  ");
                instance.global.updateLocalStorage();
                if(instance.data.currentUser.typeOfUser === "debitor"){
                    instance.data.ListOfMandates = FakeDatabase.getMandatesByDebitorPK(instance.data.currentUser.publicKey, instance.data.ListOfMandatesBk);
                } else if (instance.data.currentUser.typeOfUser === "creditor") {
                    instance.data.ListOfMandates = FakeDatabase.getMandatesByCreditorPK(instance.data.currentUser.publicKey, instance.data.ListOfMandatesBk);
                } else {
                    instance.data.ListOfMandates = instance.data.ListOfMandatesBk;
                }
                initViewData();
            }
        }

        function getMandatesSucessCallback (response) {
            console.log(response.data);
            instance.data.ListOfMandates = response.data;
            instance.data.ListOfMandates = instance.data.ListOfMandates.sort(function(a,b) {
                return a.Mandate.MandateData.Timestamp - b.Mandate.MandateData.Timestamp;
            });
            instance.global.addSettingsToMandate();
            console.log(instance.data.ListOfMandates);
            initViewData();
            return response.data;
        }

        function getMandatesErrorCallback (response) {
            console.log("getMandate error : ", response);
            return response.data;
        }

        function initViewData () {
            instance.data.boolAddMandate = false;
            instance.data.showFunctionnality.userType = instance.data.currentUser.typeOfUser;
            instance.global.updateAccessRoles(instance.data.showFunctionnality.userType);
            instance.data.ListOfUsers = FakeDatabase.getUsersSetDb();
        }

        function updateLocalStorage () {
            // ListOfMandates empty
            if ((instance.data.ListOfMandates === null)
             || (instance.data.ListOfMandates === undefined)) {
                instance.data.ListOfMandates = FakeDatabase.getDb()[0].mandat;
            }

            // LocalStorage empty
            if ((localStorage.getItem('ListOfMandates') === null)
            || (localStorage.getItem('ListOfMandates') === "null")
            || (localStorage.getItem('ListOfMandates') === undefined)) {
                localStorage.setItem('ListOfMandates', JSON.stringify(instance.data.ListOfMandates));
            }
            var previousList = JSON.parse(localStorage.getItem('ListOfMandates'));
            var MandateToCopy, mandateFounded = false;
            for (var i = 0; i < instance.data.ListOfMandates.length; i++) {
                for (var j = 0; j < previousList.length; j++) {
                    // If modified
                    if (previousList[j].Mandate.MandateID == instance.data.ListOfMandates[i].Mandate.MandateID) {
                        mandateFounded = true;
                        MandateToCopy = _.clone(instance.data.ListOfMandates[i]);
                        previousList[j] = MandateToCopy;
                        break;
                    }
                }
                // If doesn"t exist
                if (mandateFounded === false) {
                    MandateToCopy = _.clone(instance.data.ListOfMandates[i]);
                    previousList.push(MandateToCopy);
                }
                mandateFounded = false;
            }
            instance.data.ListOfMandatesBk = _.clone(previousList);
            localStorage.removeItem('ListOfMandates');
            localStorage.setItem('ListOfMandates', JSON.stringify(previousList));
        }

        function addSettingsToMandate () {
            for (var i = 0; i < instance.data.ListOfMandates.length; i++) {
                instance.data.ListOfMandates[i].editMandat = false;
                instance.data.ListOfMandates[i].infoBank = false;
                instance.data.ListOfMandates[i].infoBlockchain = false;
                instance.data.ListOfMandates[i].showInfo = false;
                if (instance.data.ListOfMandates[i].Mandate.Status == "created"){
                    instance.data.ListOfMandates[i].lastChaincodeFunctionCalled = "initiate";
                }
                if (instance.data.ListOfMandates[i].Mandate.Status == "validated"){
                    instance.data.ListOfMandates[i].lastChaincodeFunctionCalled = "fill";
                }
                if (instance.data.ListOfMandates[i].Mandate.Status == "revoked"){
                    instance.data.ListOfMandates[i].lastChaincodeFunctionCalled = "revoke";
                }
                if (instance.data.ListOfMandates[i].Mandate.Status == "amended"){
                    instance.data.ListOfMandates[i].lastChaincodeFunctionCalled == "amend";
                }
            }
        }

        function setupTypeOfPayment (periodicity) {
            var typeOfPayment = "";
            if ((periodicity !== "") && (periodicity !== null) && (periodicity !== undefined)) {
                if (periodicity === "Once") {
                    typeOfPayment = "Non-recurring";
                } else {
                    typeOfPayment = "Recurring";
                }
            } else {
                typeOfPayment = "";
            }
            return typeOfPayment;
        }

        function updateCurrentUser (userNum) {
            var usersList = FakeDatabase.getUsersSetDb();
            if ((userNum != null)&&(userNum != undefined)&&(userNum >= 0)&&(userNum <= 3)){
                instance.data.currentUser = usersList[userNum];
            } else {
                instance.data.currentUser = FakeDatabase.getUsersSetDb()[0];
            }
            instance.data.showFunctionnality.userType = instance.data.currentUser.typeOfUser;
            var res = updateAccessRoles(instance.data.showFunctionnality.userType);
            //initView();
        }

        function updateAccessRoles (userType) {
            var showFunctionnality;
            if (instance.data.currentUser.typeOfUser == "debitor") {
                showFunctionnality = {
                    userType: "debitor",
                    showInitMandate: false,
                    showFillMandate: false,
                    showAmendMandate: false,
                    showRevokeMandate: false,
                    showAddMandate: false,
                    showDialogAddMandate: false,
                    showDebitorData: true,
                    showCreditorData: false,
                    showBankData: false
                };
            } else if (instance.data.currentUser.typeOfUser == "creditor"){
                showFunctionnality = {
                    userType: "creditor",
                    showInitMandate: true,
                    showFillMandate: false,
                    showAmendMandate: false,
                    showRevokeMandate: false,
                    showAddMandate: true,
                    showDialogAddMandate: true,
                    showDebitorData: false,
                    showCreditorData: true,
                    showBankData: false
                };
            } else if (instance.data.currentUser.typeOfUser == "creditorBank") {
                showFunctionnality = {
                    userType: "creditorBank",
                    showInitMandate: false,
                    showFillMandate: false,
                    showAmendMandate: false,
                    showRevokeMandate: false,
                    showAddMandate: false,
                    showDialogAddMandate: false,
                    showDebitorData: false,
                    showCreditorData: false,
                    showBankData: true
                };
            } else if (instance.data.currentUser.typeOfUser == "debitorBank") {
                showFunctionnality = {
                    userType: "debitorBank",
                    showInitMandate: false,
                    showFillMandate: false,
                    showAmendMandate: false,
                    showRevokeMandate: false,
                    showAddMandate: false,
                    showDialogAddMandate: false,
                    showDebitorData: false,
                    showCreditorData: false,
                    showBankData: true
                };
            }
            instance.data.showFunctionnality = showFunctionnality;
            return showFunctionnality;
        }

        function hideAll () {
            for (var i = 0; i < instance.data.ListOfMandates.length; i++) {
                instance.data.ListOfMandates[i].showInfo = false;
                //instance.data.ListOfMandates[i].editMandat = false;
                //instance.data.ListOfMandates[i].infoBank = false;
                //instance.data.ListOfMandates[i].infoBlockchain = false;
            }
        }

        function showInfo (mandateNum) {
            instance.global.hideAll();
            if (mandateNum !== -1) {
                instance.data.currentMandateSelected = instance.data.ListOfMandates[mandateNum];
                instance.data.ListOfMandates[mandateNum].showInfo = true;

                if (instance.data.currentUser.typeOfUser == "debitor") {
                    if (instance.data.currentMandateSelected.Mandate.Status == "created") {
                        instance.data.showFunctionnality.showFillMandate = true;
                        instance.data.showFunctionnality.showRevokeMandate = true;
                        instance.data.showFunctionnality.showInitMandate = false;
                        instance.data.showFunctionnality.showAmendMandate = false;
                    }
                    else if (instance.data.currentMandateSelected.Mandate.Status == "validated"
                          || instance.data.currentMandateSelected.Mandate.Status == "amended") {
                        instance.data.showFunctionnality.showAmendMandate = true;
                        instance.data.showFunctionnality.showFillMandate = false;
                        instance.data.showFunctionnality.showRevokeMandate = true;
                        instance.data.showFunctionnality.showInitMandate = false;
                    }
                    else if (instance.data.currentMandateSelected.Mandate.Status == "revoked") {
                        instance.data.showFunctionnality.showInitMandate = false;
                        instance.data.showFunctionnality.showFillMandate = false;
                        instance.data.showFunctionnality.showRevokeMandate = false;
                        instance.data.showFunctionnality.showAmendMandate = false;
                    }
                }
            } else {
                instance.data.currentMandateSelected = {};
                instance.data.showFunctionnality.showInitMandate = false;
                instance.data.showFunctionnality.showFillMandate = false;
                instance.data.showFunctionnality.showRevokeMandate = false;
                instance.data.showFunctionnality.showAmendMandate = false;
            }
        }

        // unused
        function bankInfo (mandateNum) {
            instance.global.hideAll();
            instance.data.currentMandateSelected = instance.data.ListOfMandates[mandateNum];
            instance.data.ListOfMandates[mandateNum].infoBank = true;
            instance.global.mandateContentAnimation(mandateNum, '.bankInfo');
            instance.global.updateMandateToggleButtons("bank", mandateNum);
        }

        // unused
        function blockchainInfo (mandateNum) {
            instance.global.hideAll();
            instance.data.currentMandateSelected = instance.data.ListOfMandates[mandateNum];
            instance.data.ListOfMandates[mandateNum].infoBlockchain = true;
            instance.global.mandateContentAnimation(mandateNum, '.blockainInfo');
            instance.global.updateMandateToggleButtons("chaincode", mandateNum);
        }

        // unused
        function editMandat (mandateNum) {
            instance.global.hideAll();
            instance.data.currentMandateSelected = instance.data.ListOfMandates[mandateNum];
            instance.data.ListOfMandates[mandateNum].editMandat = true;
            instance.global.mandateContentAnimation(mandateNum, '.edition');
            instance.global.updateMandateToggleButtons("edit", mandateNum);
        }

        //unused -> To modify
        function mandateContentAnimation (mandateNum, typeOfContent) {
            var elementStr = '.mandateLine.mandate-info' + mandateNum + typeOfContent;
            angular.element(elementStr).addClass('bounceInDown animated');
            setTimeout(function() {
                angular.element(elementStr).removeClass('bounceInDown animated');
            }, 400);
        }

        //unused
        function resetMandateToggleButtons () {
            for (var i = 0; i < instance.data.ListOfMandates.length; i++) {
                instance.data.ListOfMandates[i].mandateToggleButtons.edit.isSelected = false;
                instance.data.ListOfMandates[i].mandateToggleButtons.chaincode.isSelected = false;
                instance.data.ListOfMandates[i].mandateToggleButtons.bank.isSelected = false;
                instance.data.ListOfMandates[i].mandateToggleButtons.global.isSelected = false;

                instance.data.ListOfMandates[i].mandateToggleButtons.edit.currentIconSrc =
                    instance.data.ListOfMandates[i].mandateToggleButtons.edit.iconSrc;
                instance.data.ListOfMandates[i].mandateToggleButtons.chaincode.currentIconSrc =
                    instance.data.ListOfMandates[i].mandateToggleButtons.chaincode.iconSrc;
                instance.data.ListOfMandates[i].mandateToggleButtons.bank.currentIconSrc =
                    instance.data.ListOfMandates[i].mandateToggleButtons.bank.iconSrc;
                instance.data.ListOfMandates[i].mandateToggleButtons.global.currentIconSrc =
                    instance.data.ListOfMandates[i].mandateToggleButtons.global.iconSrc;
            }
        }

        //unused
        function updateMandateToggleButtons (buttonToPress, mandateNumber) {
            var previousState =  instance.data.ListOfMandates[mandateNumber].mandateToggleButtons;
            instance.global.resetMandateToggleButtons();
            if (buttonToPress == "edit") {
                if (previousState.edit.isSelected == true) {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.edit.isSelected = false;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.edit.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.edit.iconSrc;
                } else {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.edit.isSelected = true;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.edit.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.edit.iconSelectedSrc;
                }
            } else if (buttonToPress == "chaincode") {
                if (previousState.chaincode.isSelected == true) {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.chaincode.isSelected = false;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.chaincode.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.chaincode.iconSrc;
                } else {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.chaincode.isSelected = true;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.chaincode.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.chaincode.iconSelectedSrc;
                }
            } else if (buttonToPress == "bank") {
                if (previousState.bank.isSelected == true) {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.bank.isSelected = false;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.bank.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.bank.iconSrc;
                } else {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.bank.isSelected = true;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.bank.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.bank.iconSelectedSrc;
                }
            } else if (buttonToPress == "global") {
                if (previousState.global.isSelected == true) {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.global.isSelected = false;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.global.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.global.iconSrc;
                } else {
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.global.isSelected = true;
                    instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.global.currentIconSrc =
                        instance.data.ListOfMandates[mandateNumber].mandateToggleButtons.global.iconSelectedSrc;
                }
            }
        }

        /********************************************************
         *              OFFLINE CHAINCODE FUNCTIONS
         ********************************************************/
        function hexGenerator () {
            var e = "";
            var hexaTab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'];
            for (var i = 0; i < 24; i++) {
                var rand = hexaTab[Math.floor(Math.random() * hexaTab.length)];
                e = e + rand.toString();
            }         
            return e ;   
        }
        function updateInstanceInitiateMandate (_data) {
            /****************************************
             _data template :
             {
                rum:
                ics:
                amount:
                periodicity:
                owner:
                creditorName:
                creditorAddress:
                BIC:
                location:
              }
             ****************************************/
            var emptyMandate = FakeDatabase.getEmptyMandate()[0];
            instance.data.ListOfMandates.push(emptyMandate);
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.MandateID = _data.MandateID;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.MandateData.Rum = _data.rum;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.CreditorData.Ics = _data.ics;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.MandateData.Amount = _data.amount;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.MandateData.Periodicity = _data.periodicity;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.CreditorData.Name = _data.creditorName;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.CreditorData.Address = _data.creditorAddress;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.MandateData.Owner = _data.owner;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.MandateData.Location = _data.location;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.MandateData.Timestamp = _data.timestamp;            
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.CreditorData.Bic = _data.BIC;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.Status = "created";
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.CreditorPublicKey = _data.creditorPublicKey;
            instance.data.ListOfMandates[instance.data.ListOfMandates.length - 1].Mandate.DebitorPublicKey = _data.debitorPublicKey;
            instance.global.updateLocalStorage();
        }



        function updateInstanceFillMandate (_data) {
            /****************************************
             _data template :
             {
                debitorName: "",
                debitorAddress: "",
                BIC: "",
                IBAN: "",
                 serviceTermsAccepted: false
             }
             ****************************************/
            var i = instance.global.getMandateIndex(instance.data.currentMandateSelected.Mandate.MandateID);
            instance.data.ListOfMandates[i].Mandate.DebitorData.Name = _data.debitorName;
            instance.data.ListOfMandates[i].Mandate.DebitorData.Address = _data.debitorAddress;
            instance.data.ListOfMandates[i].Mandate.DebitorData.Bic = _data.BIC;
            instance.data.ListOfMandates[i].Mandate.DebitorData.Iban = _data.IBAN;
            instance.data.ListOfMandates[i].Mandate.Status = "validated";
            //instance.data.ListOfMandates[currentMandateNumber].serviceTermsAccepted = _data.serviceTermsAccepted;
            instance.global.updateLocalStorage();
        }

        function updateInstanceAmendMandate (_data) {
            /****************************************
             _data template :
             {
                Address:"",
                BIC: "",
                IBAN: ""
             }
             ****************************************/
            var i = instance.global.getMandateIndex(instance.data.currentMandateSelected.Mandate.MandateID);
            if(_data.BIC){
                instance.data.ListOfMandates[i].Mandate.DebitorData.Bic = _data.BIC;
            }
            if(_data.IBAN){
                instance.data.ListOfMandates[i].Mandate.DebitorData.Iban = _data.IBAN;
            }
            if(_data.debitorAddress){
                instance.data.ListOfMandates[i].Mandate.DebitorData.Address = _data.debitorAddress;
            }

            instance.data.ListOfMandates[i].Mandate.Status = "amended";
            instance.global.updateLocalStorage();
        }
        function updateInstanceRevokeMandate (_data) {
            /****************************************
             _data template :
             {
                boolRevoke: false
             }
             ****************************************/
            var i = instance.global.getMandateIndex(instance.data.currentMandateSelected.Mandate.MandateID);
            if(_data.boolRevoke) {
                instance.data.ListOfMandates[i].Mandate.Status = "revoked";
            }
            instance.global.updateLocalStorage();
        }


        /********************************************************
         *              ONLINE CHAINCODE FUNCTIONS
         ********************************************************/

        function updateServerInitiateMandate (_data) {
            console.log("Data received by HomeModel :");
            console.log(_data);
            var result_request = httpServ.createMandate(_data);
            console.log(result_request);    
        }

        function updateServerFillMandate (_data) {
            console.log("Data received by HomeModel :");
            console.log(_data);
            var result_request = httpServ.debitorFillMandate(_data);
            console.log(result_request);        
        }

        function updateServerAmendMandate (_data) {
            console.log("Data received by HomeModel :");
            console.log(_data);
            var result_request = httpServ.debitorAmendMandate(_data);
            console.log(result_request);        
        }

        function updateServerRevokeMandate (_data) {
            console.log("Data received by HomeModel :");
            console.log(_data);
            var result_request = httpServ.creditorRevokeMandate(_data);
            console.log(result_request);        
        }

        function getMandateIndex (mandateId) {
            for (var i=0; i<instance.data.ListOfMandates.length; i++){
                if(instance.data.ListOfMandates[i].Mandate.MandateID == mandateId){
                    return i;
                }
            }
        }

        return instance;
  }]);
}(angular));

