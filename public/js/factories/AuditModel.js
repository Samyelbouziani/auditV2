
/**
 * Created by selbouziani on 30/05/2017.
 */
/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('Audit.factory', [])

        .factory('AuditModel', [ '$window', '$q', 'httpServ',
            function AuditModel($window, $q, $mdDialog, httpServ) {

                var instance = {
                    data: {
                        ListOfLots: null,
                        currentLotSelected: null
                    },
                    /********************************************************
                     Web View functions
                     ********************************************************/
                    global: {
                        listOfLotsInitiate: listOfLotsInitiate,
                        showLotInfo: showLotInfo,
                        createLot: createLot,
                        getLotsByState: getLotsByState,
                        getAllLots: getAllLots,
                        getLotsByID: getLotsByID,
                        getLotsByCountryDestination: getLotsByCountryDestination,
                        changeLotState: changeLotState
                    }

                };

                function listOfLotsInitiate(){
                    var list;
                    list = httpServ.getAllLots({Type:"lot"})
                        .then(function successCallback(response) {
                            console.log("getAllLots success : ");
                            console.log(response.data);
                            instance.data.ListOfLots = response.data;
                            for (var i = 0; i < instance.data.ListOfLots.length; i++) {
                                instance.data.ListOfLots[i].showLotInfo = false;
                            }
                            return response.data;
                        }, function errorCallback(response) {
                            console.log("getAllLots error : ");
                            console.log(response);
                            return response.data;
                        });
                }

                function showLotInfo (lotNum){
                    for (var i = 0; i < instance.data.ListOfLots.length; i++) {
                        instance.data.ListOfLots[i].showLotInfo = false;
                    }
                    instance.data.currentLotSelected = instance.data.ListOfLots[lotNum];
                    instance.data.ListOfLots[lotNum].showLotInfo = true;
                }

                /*unused*/
                function createLot (_data) {
                    console.log("data received by model");
                    console.log(_data);
                    var result_request = httpServ.createLot(_data);
                    console.log(result_request);
                }

                function getAllLots (_data) {
                    console.log("data received by model");
                    console.log(_data);
                    var result_request = httpServ.getAllLots(_data);
                    console.log(result_request);

                }
                /*unused*/
                function getLotsByState (_data) {
                    console.log("data received by model");
                    console.log(_data);
                    var result_request = httpServ.getLotsByState(_data)
                        .then(function successCallback(response) {
                            console.log("getLotsByState success : ");
                            console.log(response.data);
                            instance.data.ListOfLots = response.data;
                            return response.data;
                        }, function errorCallback(response) {
                            console.log("getLotsByState error : ");
                            console.log(response);
                            return response.data;
                        });
                    console.log(result_request);
                }

                /*unused*/
                /* function getLotsByID (_data) {
                 console.log("data received by model");
                 console.log(_data);
                 var result_request = httpServ.getLotsByID(_data);
                 console.log(result_request);

                 }*/

                /*unused*/
                function getLotsByCountryDestination (_data) {
                    console.log("data received by model");
                    console.log(_data);
                    var result_request = httpServ.getLotsByCountryDestination(_data);
                    console.log(result_request);

                }

                /*unused*/
                function changeLotState (_data) {
                    console.log("data received by model");
                    console.log(_data);
                    var result_request = httpServ.changeLotState(_data);
                    console.log(result_request);

                }


                return instance;

            }]);
}(angular));
