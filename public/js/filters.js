/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('HyperSEPA.filters', [
      'Filters.filters'
  ]);
}(angular));
