/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('Filters.filters', [])
        .filter('reverse', function() {
            return function(items) {
                if (items !== undefined) {
                    return items.slice().reverse();
                } else {
                    return [];
                }
                
            };
        })
        .filter('userFilter', function() {
            return function(items, currentUserSelected) {
                //console.log(_);
                var filtered = [], filteredIndex = 0;
                filtered[0] = items[0];
                //console.log("currentUserSelected.publicKey : " + currentUserSelected.publicKey);
                for (var i = 1; i < items.length; i++) {
                    if ((items[i].transaction !== undefined) && (items[i].transaction !== null) && (items[i].transaction.length > 0)) {
                        for (var j = 0; j < items[i].transaction.length; j++) {
                            if ((items[i].transaction[j].data.creditorPublicKey == currentUserSelected.publicKey)
                             || (items[i].transaction[j].data.debitorPublicKey == currentUserSelected.publicKey)) {
                                // Copying Block information
                                if (filtered[filteredIndex].hash != items[i].hash) {
                                    var BlockToCopy = _.clone(items[i]);// = items.slice(i, i+1);
                                    BlockToCopy.transaction = [];
                                    filteredIndex++;
                                    filtered[filteredIndex] = BlockToCopy;
                                }
                                // Copying Transaction information
                                var TransactionToCopy = items[i].transaction[j];
                                filtered[filteredIndex].transaction.push(TransactionToCopy);
                            }
                            //var pk1 = items[i].transaction[j].data.creditorPublicKey
                            //  , pk2 = items[i].transaction[j].data.debitorPublicKey;
                            //console.log("Pk inside loop : n°" + [i],pk1,pk2);
                        }
                    }
                }
                return filtered;
            };
        })
}(angular));
