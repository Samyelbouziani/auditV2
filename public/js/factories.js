/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('HyperSEPA.factories', [
    'FakeDatabase.factory',
    'BlockchainModel.factory',
    'Home.factory'
  ]);
}(angular));
