/*global angular*/
(function withAngular(angular) {
  'use strict';

    angular.module('Directives.directive', [])
    /***********************************************************
                            GENERAL
     ***********************************************************/
    .directive('scrollButton', function($window, $document) {
        return {
            restrict: 'E',
            templateUrl: 'views/home/scroll-button.html',
            scope: {
                userType: '@'
            },
            link: function (scope, element, attrs) {
                scope.userType = attrs.userType;
                scope.scrolling = "scrolling";
                scope.fabDesign = "";

                if ((scope.userType == "debitorBank")
                 || (scope.userType == "creditorBank")) {
                    scope.fabDesign = "header-toolbar-bank";
                } else {
                    scope.fabDesign = "header-toolbar-creditor";
                }

                element.on('click', function () {
                    angular.element("body").animate({scrollTop: element.offset().top*0.47}, "slow");
                    scope.scrolling = "";
                });

                angular.element($window).bind("scroll", function() {
                    var offSet = $window.scrollY,
                        docHeight = $document.height(),
                        winHeight = $window.innerHeight,
                        scrolledPercentage = ((offSet) / (docHeight - winHeight)) * 100;

                    if (scrolledPercentage <= 45) {
                        scope.scrolling = "scrolling";
                    } else {
                        scope.scrolling = "";
                    }
                    scope.$apply();
                });
            }
        };
    })
    /***********************************************************
                            HEADER
     ***********************************************************/
    .directive('header', function() {
      return {
        restrict: 'E',
        transclude: true,
        templateUrl :'views/layout/header.html',
        scope: {}
      }
    })
    .directive('headerMain', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/layout/header-main.html'
        };
    })
    .directive('headerSearch', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/layout/header-search.html'
        };
    })
    .directive('headerIcon', function() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl :'views/layout/header-icon.html'
        }
    })

    /************************************************************
                            DASHBOARD
     ***********************************************************/
    .directive('dashboard', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/index.html'
        };
    })
    .directive('dashboardHeader', function() {
        return {
            restrict: 'E',
            scope: {
                headerName: '@'
            },
            link: function (scope, element, attrs) {
                scope.headerName = attrs.headerName;
                scope.iconName = attrs.iconName; 
            },
            templateUrl: 'views/dashboard/dashboard-header.html'
        };
    })
    .directive('dashboardTabs', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-tabs.html'
        };
    })
    .directive('dashboardMandateList', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-mandate-list.html'
        };
    })
    .directive('dashboardMandateData', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-mandate-data.html'
        };
    })
    .directive('dashboardMandateDataEmpty', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-mandate-data-empty.html'
        };
    })
    .directive('dashboardMandateDataContent', function() {
        return {
            restrict: 'E',
            scope: {
                label: '@',
                value: '@'
            },
            link: function (scope, element, attrs) {
                scope.label = attrs.label;
                scope.value = attrs.value;
                scope.currency = attrs.currency;
            },
            templateUrl: 'views/dashboard/dashboard-mandate-data-content.html'
        };
    })
    .directive('dashboardStepBar', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-step-bar.html',
            controller: 'StepBarController'
        };
    })
    .directive('dashboardButtons', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-buttons.html'
        };
    })
    .directive('dashboardButtonAddMandate', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-button-add-mandate.html'
        };
    })
    .directive('dashboardPersonalData', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/dashboard/dashboard-personal-data.html'
        };
    })

    /************************************************************
                            FILTERS
     ***********************************************************/
    .directive('filterAll', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/filters/filter-all.html'
        };
    })
    .directive('filterId', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/filters/filter-id.html'
        };
    })
    .directive('filterIcs', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/filters/filter-ics.html'
        };
    })
    .directive('filterRum', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/filters/filter-rum.html'
        };
    })
    .directive('filterPublicKey', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/filters/filter-public-key.html'
        };
    })
    .directive('filterAmount', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/filters/filter-amount.html'
        };
    })

    /************************************************************
                            BLOCKCHAIN
     ***********************************************************/
    .directive('blockchain', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/blockchain/index.html',
            controller: 'BlocksCtrl'
        };
    })
    .directive('blockchainHeader', function() {
        return {
            restrict: 'E',
            scope: {
                headerName: '@'
            },
            link: function (scope, element, attrs) {
                scope.headerName = attrs.headerName;
                scope.iconName = attrs.iconName;

            },
            templateUrl: 'views/blockchain/blockchain-header.html'
        };
    })
    .directive('blockchainTabs', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/blockchain/blockchain-tabs.html'
        };
    })
    .directive('blockchainBlocks', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/blockchain/blockchain-blocks.html'
        };
    })
    .directive('blockchainBlocksInfo', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/blockchain/blockchain-blocks-info.html'
        };
    })
    .directive('blockchainBlocksInfoContent', function() {
        return {
            restrict: 'E',
            scope: {
                label: '@',
                value: '@'
            },
            link: function (scope, element, attrs) {
                scope.label = attrs.label;
                scope.value = attrs.value;
                scope.currency = attrs.currency;
            },
            templateUrl: 'views/blockchain/blockchain-blocks-info-content.html'
        };
    })
    .directive('blockchainTransactionsInfo', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/blockchain/blockchain-transactions-info.html'
        };
    })
    .directive('blockchainChaincodeInfo', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/blockchain/blockchain-chaincode-info.html'
        };
    })
    .directive('blockchainChaincodeInfoContent', function() {
        return {
            restrict: 'E',
            scope: {
                label: '@',
                value: '@',
                currency: '@',
                transaction: '='
            },
            /*link: function (scope, element, attrs) {
                scope.label = attrs.label;
                scope.value = attrs.value;
                if (attrs.currency === undefined) {
                    scope.currency = "";
                } else {
                    scope.currency = attrs.currency;
                }
            },*/
            templateUrl: 'views/blockchain/blockchain-chaincode-info-content.html'
        };
    })
    .directive('blockchainChaincodeInfoContentPassword', function() {
        return {
            restrict: 'E',
            scope: {
                label: '@',
                value: '@',
                currency: '@',
                transaction: '='
            },
            templateUrl: 'views/blockchain/blockchain-chaincode-info-content-password.html'
        };
    })
    /************************************************************
                                FOOTER
     ***********************************************************/
    .directive('footerApp', function() {
        return {
            restrict: 'E',
            scope: {
                userType: '@'
            },
            templateUrl: 'views/layout/footer.html',
            link: function (scope, element, attrs) {
                scope.userType = attrs.userType;
                // TODO : hide footer when we are not at the bottom of the view
            }
        };
    });
}(angular));