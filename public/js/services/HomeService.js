/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('HomeService.service', [])
  .service('HomeService', [ 'HomeModel', 'BlockchainModel',

    function HomeService(HomeModel,BlockchainModel) {

        var isFabricListening = function isFabricListening (_data) {

            var isServerWorking = HomeModel.data.isServerListening;
            if (isServerWorking) {
                BlockchainModel.global.isFabricListening = true ;
            } else {
                BlockchainModel.global.isFabricListening = false ; 
            }

        }, submitInitiateMandateFromDialogCtrl = function submitInitiateMandateFromDialogCtrl (_data) {
            /****************************************
             _data template :
            {
                rum:
                ics:
                amount:
                periodicity:
                owner:
                creditorName:
                creditorAddress:
                BIC:
                location:
             }
            ****************************************/
            var isServerWorking = HomeModel.data.isServerListening;
            if (isServerWorking) {
                HomeModel.offline.updateInstanceInitiateMandate(_data);
                HomeModel.online.updateServerInitiateMandate(_data);
            } else {
                _data.MandateID = HomeModel.utils.hexGenerator();
                HomeModel.offline.updateInstanceInitiateMandate(_data);
            }
            _data.typeOfPayment = HomeModel.global.setupTypeOfPayment(_data.periodicity);
            BlockchainModel.offline.addBlock(_data,"Initiate Mandate");

        }, submitFillMandateFromDialogCtrl = function submitFillMandateFromDialogCtrl (_data) {
            /****************************************
             _data template :
             {
                debitorName:
                debitorAddress:
                BIC:
                IBAN:
                serviceTermsAccepted: false
             }
             ****************************************/
            var isServerWorking = HomeModel.data.isServerListening;
            if (isServerWorking) {
                HomeModel.offline.updateInstanceFillMandate(_data);
                HomeModel.online.updateServerFillMandate(_data);
            } else {
                HomeModel.offline.updateInstanceFillMandate(_data);
                //BlockchainModel.addTransaction();
            }
            BlockchainModel.offline.addBlock(_data,"Fill Mandate");

        }, submitAmendMandateFromDialogCtrl = function submitAmendMandateFromDialogCtrl (_data) {
            /****************************************
             _data template :
             {
                BIC:
                IBAN:
                debitorAddress:
             }
             ****************************************/
            var isServerWorking = HomeModel.data.isServerListening;
            if (isServerWorking) {
                HomeModel.offline.updateInstanceAmendMandate(_data);
                HomeModel.online.updateServerAmendMandate(_data);
                //BlockchainModel.addTransaction(_data,"Amend");
            } else {
                HomeModel.offline.updateInstanceAmendMandate(_data);
            }
            BlockchainModel.offline.addBlock(_data,"Amend Mandate");

        }, submitRevokeMandateFromDialogCtrl = function submitRevokeMandateFromDialogCtrl (_data) {
            /****************************************
             _data template :
             {
                boolRevoke:
             }
             ****************************************/
            var isServerWorking = HomeModel.data.isServerListening;
            if (isServerWorking) {
                HomeModel.offline.updateInstanceRevokeMandate(_data);
                HomeModel.online.updateServerRevokeMandate(_data);
            } else {
                HomeModel.offline.updateInstanceRevokeMandate(_data);
                //BlockchainModel.addTransaction();
            }
            BlockchainModel.offline.addBlock(_data,"Revoke Mandate");

        };

        var service = {
            'isFabricListening': isFabricListening, 
            'submitInitiateMandateFromDialogCtrl': submitInitiateMandateFromDialogCtrl,
            'submitFillMandateFromDialogCtrl': submitFillMandateFromDialogCtrl,
            'submitAmendMandateFromDialogCtrl': submitAmendMandateFromDialogCtrl,
            'submitRevokeMandateFromDialogCtrl': submitRevokeMandateFromDialogCtrl,
            'submitInitiateMandateToHomeCtrl': null,
            'submitFillMandateToHomeCtrl': null,
            'submitAmendMandateToHomeCtrl': null,
            'submitRevokeMandateToHomeCtrl': null,
            'updateCurrentUserToHomeCtrl': null,
            'updateCurrentUserToBlocksCtrl': null,
            'updateCurrentMandateToHomeCtrl': null,
            'updateCurrentMandateToStepBarCtrl': null
        };

        return service;
  }]);
}(angular));
