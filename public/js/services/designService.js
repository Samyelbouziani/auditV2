/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('designService.service', [])
  .service('designService', [ '$mdToast',

    function designService($mdToast) {

      // unused
      var regexNumber = function regexNumber (str) {
        var regex = new RegExp('^[\-]{0,1}[0-9]+[\.][0-9]+|[\-]{0,1}[0-9]+$');
        var result = regex.exec(str);
        //console.log(result[0]);
        return result[0];

      }, getFABDesign = function getFABDesign () {
          return {
              //isOpen: false,
              //hidden: false,
              //hover: false,
              //selectedMode: 'md-fling',
              //selectedDirection: 'down',
              showInputNumber: 0,
              searchFields: [{
                  name: "Mandate ID",
                  //direction: "bottom",
                  parameter: "MandateID",
                  icon: "fonts/icons/mandate-filter.svg",
                  textToShow: "Look for Mandate ID"
              },{
                  name: "ICS",
                  //direction: "top",
                  parameter: "Ics",
                  icon: "fonts/icons/account_circle.svg",
                  textToShow: "Look for ICS"
              },{
                  name: "RUM",
                  //direction: "bottom",
                  parameter: "Rum",
                  icon: "fonts/icons/assignment_id.svg",
                  textToShow: "Look for RUM"
              },{
                  name: "Public Key",
                  //direction: "top",
                  parameter: "pk",
                  icon: "fonts/icons/assignment_id.svg",
                  textToShow: "Look for Public Key"
              },{
                  name: "Amount",
                  //direction: "top",
                  parameter: "amount",
                  icon: "fonts/icons/euro_symbol.svg",
                  textToShow: "Look for amount"
              }]
          };
      }, updateFABDesign = function updateFABDesign (str) {
          var result = getFABDesign();
          if (str == "MandateID") {
              result.textToShow = "Look for Mandate ID";
              result.showInputNumber = 0;
          } else if (str == "Ics") {
              result.textToShow = "Look for ICS";
              result.showInputNumber = 1;
          } else if (str == "Rum") {
              result.textToShow = "Look for RUM";
              result.showInputNumber = 2;
          } else if (str == "pk") {
              result.textToShow = "Look for public key";
              result.showInputNumber = 3;
          } else if (str == "amount") {
              result.textToShow = "Look for mandate amount";
              result.showInputNumber = 4;
          } else if (str == "all") {
              result.textToShow = "";
              result.showInputNumber = 5;
          }else {
              result.textToShow = "Look for Mandate ID";
              result.showInputNumber = 0;
          }
          return result;

      }, showMandateData = function showMandateData (_typeOfUser, _dataToDisplay) {
            var dataDisplayed = false;
            if ((_dataToDisplay != undefined) && (_dataToDisplay !== null) &&
              (((_typeOfUser === "debitor") && ((_dataToDisplay.showDebitorData)||(_dataToDisplay.showBankData))) ||
               ((_typeOfUser === "creditor") && ((_dataToDisplay.showCreditorData)||(_dataToDisplay.showBankData))) ||
              (((_typeOfUser === "creditorBank")||(_typeOfUser === "debitorBank")) && (_dataToDisplay.showBankData)))) {
                dataDisplayed = true;
            } else {
                dataDisplayed = false;
            }
            return dataDisplayed;
      }, showToast = function showToast (message) {
          // Check navigator compatibility
          navigator.checkWebBrowser= (function(){
              var ua= navigator.userAgent, tem,
                  M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
              if(/trident/i.test(M[1])){
                  tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                  M[0] = "IE";
              }
              if(M[1]=== 'Chrome'){
                  tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                  //if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
              }
              M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
              if(M[0] !== "Chrome") {
                  $mdToast.show({
                      hideDelay   : 3000,
                      position    : 'top',
                      controller  : 'ToastCtrl',
                      templateUrl : 'views/layout/toast.html'
                  });
              }
          })();
      };

      return {
        'regexNumber': regexNumber,
        'getFABDesign': getFABDesign,
        'updateFABDesign': updateFABDesign,
        'showMandateData' : showMandateData,
        'showToast': showToast
      };
  }]);
}(angular));
