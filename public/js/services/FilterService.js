/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('FilterService.service', [])
        .service('FilterService', [
            function FilterService() {

                var defaultSearchMandate = {
                    //selectModel: "MandateID",
                    searchModel: {
                        Mandate: {
                            //MandateID: '',
                            //Status: '',
                            //boolRevoke: '',
                            //CreditorPublicKey: '',
                            //DebitorPublicKey: '',
                            //MandateData: {
                            //    Rum: '',
                            //    Timestamp: '',
                            //    Location: '',
                            //    Amount: '',
                            //    Periodicity: '',
                            //    TypeOfPayment: '',
                            //    Owner: ''
                            //},
                            //CreditorData: {
                            //    Ics: '',
                            //    Name: '',
                            //    Address: '',
                            //    Iban: '',
                            //    Bic: ''
                            //},
                            //DebitorData: {
                            //    Name: '',
                            //    Address: '',
                            //    Iban: '',
                            //    Bic: ''
                            //}
                        },
                        lastChaincodeFunctionCalled: null,
                        infoBank: null,
                        showInfo: null,
                        editMandat: null,
                        infoBlockchain: null
                    }
                }, searchMandate = defaultSearchMandate

                 , resetSearchMandate = {
                    searchModel: {
                        Mandate: {},
                        lastChaincodeFunctionCalled: null,
                        infoBank: null,
                        showInfo: null,
                        editMandat: null,
                        infoBlockchain: null
                    }
                }, defaultSearchBlock = {
                    transaction: {}
                }, searchBlock = defaultSearchBlock;

                var service = {
                    searchMandate: searchMandate,
                    defaultSearchMandate: defaultSearchMandate,
                    resetSearchMandate: resetSearchMandate,
                    searchBlock: searchBlock,
                    defaultSearchBlock: defaultSearchBlock
                };

                return service;
            }]);
}(angular));
