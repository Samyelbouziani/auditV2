/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('testServerRoute.service', [])
        .service('testServerRoute', ['$http',

            function testServerRoute($http) {

                var testGetMandat = function testGetMandat () {
                    var _url = '/db/mandat';//'0.0.0.0:8080/blocks';
                    var _data = JSON.stringify({test: 0});

                    return $http({
                        method: 'GET',
                        url: _url
                    }).then(function successCallback(data, status, headers, config) {
                        console.log("getmandat success : ");
                        console.log(data);
                        return data;
                    }, function errorCallback(data, status, headers, config) {
                        console.log("getmandat error : ");
                        console.log(data);
                        return data;
                    });

                }, postCreateMandate = function postCreateMandate(rum, isWaitingServer) {
                    var _url = '/chaincode/createMandate';
                    if(rum == undefined)  rum = "dd" ;
                    var rum_data =  rum  ;
                    var _data = {rum: 0};
                    _data.rum = rum_data ;
                    isWaitingServer = true;
                    console.log("post :",_data);

                    return $http({
                        method: 'POST',
                        url: _url,
                        data: _data
                    }).then(function successCallback(data, status, headers, config) {
                        console.log("postCreateMandate success : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            seeSmartContractContent: false,
                            seeTestedFunctions: true,
                            unitTestsRes: data.data
                        };
                    }, function errorCallback(data, status, headers, config) {
                        console.log("postCreateMandate error : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            unitTestsRes: data
                        };
                    });

                }, submitCreditorFillMandate = function submitCreditorFillMandate(_rum, _ics, _creditorName, _creditorAddress, isWaitingServer) {
                    var _url = '/chaincode/creditorFillMandate';
                    if(_rum == undefined)  _rum = 0 ;
                    if(_ics == undefined)  _ics = 0 ;
                    if(_creditorName == undefined)  _creditorName = 0 ;
                    if(_creditorAddress == undefined)  _creditorAddress = 0 ;
                    var _pubk = localStorage.getItem('pubk');              

                    var _data = JSON.stringify({rum: _rum, ics: _ics, creditorName: _creditorName, creditorAddress: _creditorAddress, pubk: _pubk});
                    isWaitingServer = true;
                    console.log("post :"+_data);

                    return $http({
                        method: 'POST',
                        url: _url,
                        data: _data
                    }).then(function successCallback(data, status, headers, config) {
                        console.log("submitCreditorFillMandate success : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            seeSmartContractContent: false,
                            seeTestedFunctions: true,
                            unitTestsRes: data.data
                        };
                    }, function errorCallback(data, status, headers, config) {
                        console.log("submitCreditorFillMandate error : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            unitTestsRes: data
                        };
                    });

                }, submitDebitorFillMandate = function submitDebitorFillMandate(_debitorName, _debitorAddress, _coordBancaire, _sign, isWaitingServer) {
                    var _url = '/routes_test/debitorFillMandate';
                    if(_debitorName == undefined)  _debitorName = 0 ;
                    if(_debitorAddress == undefined)  _debitorAddress = 0 ;
                    if(_coordBancaire == undefined)  _coordBancaire = 0 ;
                    var _pubk = localStorage.getItem('pubk');              

                    var _data = JSON.stringify({debtorName: _debitorName, debtorAddress: _debitorAddress, coordBancaire: _coordBancaire, pubk: _pubk});
                    isWaitingServer = true;
                    console.log("post :"+_data);

                    return $http({
                        method: 'POST',
                        url: _url,
                        data: _data
                    }).then(function successCallback(data, status, headers, config) {
                        console.log("submitDebitorFillMandate success : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            seeSmartContractContent: false,
                            seeTestedFunctions: true,
                            unitTestsRes: data.data
                        };
                    }, function errorCallback(data, status, headers, config) {
                        console.log("submitDebitorFillMandate error : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            unitTestsRes: data
                        };
                    });

                }, submitDebitorUpdatesMandate = function submitDebitorUpdatesMandate(_coordBancaire, _sign, isWaitingServer) {
                    var _url = '/chaincode/DebitorUpdatesMandate';
                    if(_coordBancaire == undefined)  _coordBancaire = 0 ;
                    var _pubk = localStorage.getItem('pubk');              

                    var _data = JSON.stringify({coordBancaire: _coordBancaire, pubk: _pubk});
                    isWaitingServer = true;
                    console.log("post :"+_data);

                    return $http({
                        method: 'POST',
                        url: _url,
                        data: _data
                    }).then(function successCallback(data, status, headers, config) {
                        console.log("submitDebitorUpdatesMandate success : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            seeSmartContractContent: false,
                            seeTestedFunctions: true,
                            unitTestsRes: data.data
                        };
                    }, function errorCallback(data, status, headers, config) {
                        console.log("submitDebitorUpdatesMandate error : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            unitTestsRes: data
                        };
                    });

                }, submitKillMandate = function submitKillMandate(_sign, isWaitingServer) {
                    var _url = '/chaincode/killMandate';
                    var _pubk = localStorage.getItem('pubk');              
                    var _data = JSON.stringify({ pubk: _pubk});

                    isWaitingServer = true;
                    console.log("post :"+_data);

                    return $http({
                        method: 'POST',
                        url: _url,
                        data: _data
                    }).then(function successCallback(data, status, headers, config) {
                        console.log("submitKillMandate success : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            seeSmartContractContent: false,
                            seeTestedFunctions: true,
                            unitTestsRes: data.data
                        };
                    }, function errorCallback(data, status, headers, config) {
                        console.log("submitKillMandate error : ");
                        console.log(data);
                        return {
                            isWaitingServer: false,
                            unitTestsRes: data
                        };
                    });
                };

                return {
                    'testGetMandat': testGetMandat,
                    'postCreateMandate': postCreateMandate,
                    'submitCreditorFillMandate': submitCreditorFillMandate,
                    'submitDebitorFillMandate': submitDebitorFillMandate,
                    'submitDebitorUpdatesMandate': submitDebitorUpdatesMandate,
                    'submitKillMandate': submitKillMandate
                };
            }]);
}(angular));
