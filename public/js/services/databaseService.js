/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('dbService.service', [])
  .service('dbService', [ '$window', 'httpServ', 'HomeModel', 'FakeDatabase',

    function dbService($window, httpServ, HomeModel, FakeDatabase) {

      var loginOnline = function loginOnline (pubk, prik) {
          var result = httpServ.login(pubk, prik)
          .then(function successCallback(response) {
              console.log("loginOnline success : ");
              console.log(response);
              if (response.status == 404) {
                  HomeModel.data.isServerListening = false;
              } else {
                  HomeModel.data.isServerListening = true;
              }
              return response.data;
          }, function errorCallback(response) {
              console.log("loginOnline error : ");
              console.log(response);
              HomeModel.data.isServerListening = false;
              return response.data;
          });
      }, loginOffline = function loginOffline (pubk, prik) {
          var found = false;
          var db = FakeDatabase.getDb();

          for(var i = 0; i < db[0].login.length ; i++) {
              if(db[0].login[i].pubk == pubk){
                  if(db[0].login[i].prik == prik) {
                      found = true;
                      HomeModel.data.logged = db[0].login[i].type;
                      HomeModel.data.showFunctionnality.userType = db[0].login[i].type;
                      /*     homeModel.data.logged.push({r
                       name: "Johny",
                       type:  db[0].login[i].type
                       });*/
                  }
              }
          }
          if(found) {
              console.log( "Public key and private key has been found in db and match with an account");
              if (localStorage.length != 0) {
                  localStorage.removeItem('pubk');
                  localStorage.removeItem('typePerson');
              }
              localStorage.setItem('pubk',pubk);
              localStorage.setItem('typePerson',HomeModel.data.logged);
              //$cookies.put('typePerson', homeModel.data.logged);
              //localStorage.setItem('pubk',pubk);
              //localStorage.setItem('typePerson',HomeModel.data.logged);
          }
          else {
              console.log( "Error : Public key and private key not found in database");
          }
      }, getServerState = function getServerState () {
          return HomeModel.data.isServerListening;
      }, getRole = function getRole (userType) {
          var showFunctionnality;
          if (userType == "debitor") {
              showFunctionnality = {
                  typePerson: "debitor",
                  showInitMandate: false,
                  showFillMandate: true,
                  showAmendMandate: true,
                  showRevokeMandate: true,
                  showAddMandate: false,
                  showDialogAddMandate: false,
                  showDebitorData: true,
                  showCreditorData: false,
                  showBankData: false
              };
          } else if (userType == "creditor") {
              showFunctionnality = {
                  typePerson: "creditor",
                  showInitMandate: true,
                  showFillMandate: false,
                  showAmendMandate: false,
                  showRevokeMandate: false,
                  showAddMandate: true,
                  showDialogAddMandate: true,
                  showDebitorData: false,
                  showCreditorData: true,
                  showBankData: false
              };
          } else if (userType == "creditorBank") {
              showFunctionnality = {
                  typePerson: "creditorBank",
                  showInitMandate: false,
                  showFillMandate: false,
                  showAmendMandate: false,
                  showRevokeMandate: false,
                  showAddMandate: false,
                  showDialogAddMandate: false,
                  showDebitorData: false,
                  showCreditorData: false,
                  showBankData: true
              };
          } else if (userType == "debitorBank") {
              showFunctionnality = {
                  typePerson: "debitorBank",
                  showInitMandate: false,
                  showFillMandate: false,
                  showAmendMandate: false,
                  showRevokeMandate: false,
                  showAddMandate: false,
                  showDialogAddMandate: false,
                  showDebitorData: false,
                  showCreditorData: false,
                  showBankData: true
              };
          }
          HomeModel.data.showFunctionnality = showFunctionnality;
          return showFunctionnality;
      };

      return {
          'loginOnline': loginOnline,
          'loginOffline': loginOffline,
          'getServerState': getServerState,
          'getRole': getRole
      };
  }]);
}(angular));
