/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('HyperSEPA.providers', []);
}(angular));
