/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('HyperSEPA.services', [
      'HomeService.service',
      'httpServ.service',
      'designService.service',
      'dbService.service',
      'FilterService.service',
      //'geolocation.service',
      'testServerRoute.service'
  ]);
}(angular));
