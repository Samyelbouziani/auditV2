/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('HyperSEPA.controllers', [
  	    'Main.controller',
        'Header.controller',
  	    'Login.controller',
        'DialogCtrl.controller',
        'Home.controller',
        'BlocksCtrl.controller',
        'Audit.controller',
        'StepBarController.controller']);
}(angular));