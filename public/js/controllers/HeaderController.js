(function withAngular(angular) {
    'use strict';

    angular.module('Header.controller', [])
        .controller('Header', ['$scope', '$rootScope', '$compile', 'httpServ', 'HomeService', 'HomeModel', 'FakeDatabase', 'designService', 'FilterService',
            function HeaderController($scope, $rootScope, $compile, httpServ, HomeService, HomeModel, FakeDatabase, designService, FilterService) {

                $scope.usersList = FakeDatabase.getUsersSetDb();
                $scope.currentUser = $scope.usersList[0];
                $scope.FABData = designService.getFABDesign();
                $scope.searchMandate = FilterService.searchMandate.searchModel.Mandate;

                $scope.viewName = "Hyper SEPA";
                $scope.showSearch = false;

                HomeModel.global.updateCurrentUser(0);
                $scope.FABData.showInputNumber = 5;

                $scope.updateCurrentUser = function (userNum) {
                    HomeModel.global.updateCurrentUser(userNum);
                    $scope.currentUser = HomeModel.data.currentUser;
                    HomeService.updateCurrentUserToHomeCtrl($scope.currentUser);
                    HomeService.updateCurrentUserToBlocksCtrl($scope.currentUser);
                    HomeModel.global.initView();
                };

                $scope.updateFABDesign = function (str) {
                    $scope.resetFilters();
                    $scope.FABData = designService.updateFABDesign(str);
                    $scope.searchMandate = FilterService.searchMandate.searchModel.Mandate;
                };

                $scope.showSearchView = function () {
                    var searchIconId = 'search-icon'
                    ,   closeIconId = 'close-icon'
                    ,   headerSearchId = 'header-search'
                    ,   headerMainId = 'header-main'
                    ,   searchIconClass = 'show-search-bar-search-icon'
                    ,   closeIconClass = 'show-search-bar-close-icon'
                    ,   searchIconTag = document.getElementById(searchIconId)
                    ,   closeIconTag = document.getElementById(closeIconId)
                    ,   headerSearchTag = document.getElementById(headerSearchId)
                    ,   headerMainTag = document.getElementById(headerMainId);

                    $scope.showSearch = !$scope.showSearch;
                    $scope.resetFilters();

                    if ($scope.showSearch) {
                        angular.element(searchIconTag).addClass(searchIconClass);
                        angular.element(closeIconTag).addClass(closeIconClass);

                        if (headerSearchTag.className.indexOf('header-search-out') != -1) {
                            angular.element(headerSearchTag).removeClass('header-search-out');
                        }
                        if (headerMainTag.className.indexOf('header-main-in') != -1) {
                            angular.element(headerMainTag).removeClass('header-main-in');
                        }

                        angular.element(headerSearchTag).toggleClass('header-search-in');
                        angular.element(headerMainTag).removeClass('header-main-out');
                    } else {
                        angular.element(searchIconTag).removeClass(searchIconClass);
                        angular.element(closeIconTag).removeClass(closeIconClass);

                        if (headerSearchTag.className.indexOf('header-search-in') != -1) {
                            angular.element(headerSearchTag).removeClass('header-search-in');
                        }
                        if (headerMainTag.className.indexOf('header-main-out') != -1) {
                            angular.element(headerMainTag).removeClass('header-main-out');
                        }

                        angular.element(headerSearchTag).toggleClass('header-search-out');
                        angular.element(headerMainTag).removeClass('header-main-in');
                    }
                };

                $scope.resetLocalStorage = function () {
                    localStorage.removeItem('ListOfMandates');
                    localStorage.removeItem('Blockchain');
                    location.reload();
                };

                $scope.resetFilters = function () {
                    var checkMandateProperty  = $scope.searchMandate.hasOwnProperty('Mandate');

                    if (checkMandateProperty) {
                        var checkIDProperty   = $scope.searchMandate.Mandate.hasOwnProperty('MandateID'),
                            checkPkProperty   = $scope.searchMandate.Mandate.hasOwnProperty('DebitorPublicKey');
                        if (checkIDProperty)    $scope.searchMandate.Mandate.MandateID = "";
                        if (checkPkProperty)    $scope.searchMandate.Mandate.DebitorPublicKey = "";

                        var checkCreditorDataProperty = $scope.searchMandate.Mandate.hasOwnProperty('CreditorData'),
                            checkMandateDataProperty  = $scope.searchMandate.Mandate.hasOwnProperty('MandateData');
                        if (checkCreditorDataProperty)  $scope.searchMandate.Mandate.CreditorData.Ics = "";

                        if (checkMandateDataProperty) {
                            var checkAmountProperty   = $scope.searchMandate.Mandate.MandateData.hasOwnProperty('Amount'),
                                checkRUMProperty      = $scope.searchMandate.Mandate.MandateData.hasOwnProperty('Rum');
                            if (checkAmountProperty)    $scope.searchMandate.Mandate.MandateData.Amount = "";
                            if (checkRUMProperty)       $scope.searchMandate.Mandate.MandateData.Rum = "";
                        }
                    }
                };

                $scope.unselectMandate = function () {
                    //HomeModel.data.currentMandateSelected = null;
                    HomeService.updateCurrentMandateToHomeCtrl(-1);
                };

                $scope.testServerConnection = function() {
                    var resultat = httpServ.testServer()
                    .then(function successCallback(response) {
                        console.log("testServerConnection success : ");
                        console.log(response);
                        if (response.data == true) {
                            HomeModel.data.isServerListening = true;
                        } else {
                            HomeModel.data.isServerListening = false;
                        }
                        return response.data;
                    }, function errorCallback(response) {
                        console.log("testServerConnection error : ");
                        console.log(response);
                        HomeModel.data.isServerListening = false;
                        return response.data;
                    });
                };
                $scope.testServerConnection();
            }]);
}(angular));
