
/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('Audit.controller', [])
        .controller('AuditCtrl', ['$stateParams', '$state', '$scope', '$rootScope', '$mdDialog',
            function($stateParams, $state, $scope, $rootScope, $mdDialog, AuditModel) {
                console.log("Audit controller" );
                $scope.salut = "ahahah";
                function listOfLotsInitiate() {
                    AuditModel.global.listOfLotsInitiate();
                    $scope.model = AuditModel.data;
                }

                $scope.showLotInfo = function(lotNum) {
                    AuditModel.global.showLotInfo(lotNum);
                    $scope.currentLotSelected = AuditModel.data.ListOfLots[lotNum];
                }

                //listOfLotsInitiate();

            }
        ])
}(angular));