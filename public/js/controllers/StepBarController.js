(function withAngular(angular) {
    'use strict';

    angular.module('StepBarController.controller', [])
        .controller('StepBarController', ['$scope', '$rootScope', '$animate', '$element', '$attrs', '$mdStepper', '$timeout', 'HomeService', 'HomeModel',
            function StepBarController ($scope, $rootScope, $animate, $element, $attrs, $mdStepper, $timeout, HomeService, HomeModel) {
                //console.log("StepBarController called");

                $scope.$timeout = $timeout;
                $scope.isVertical = false;
                $scope.isLinear = true;
                $scope.isAlternative = true;
                $scope.isMobileStepText = true;
                $scope.stepBar = {
                    currentStepNum: 1,
                    lastFunctionCalled: 'create'
                };

                setTimeout(function() {
                    $scope.$apply(); //this triggers a $digest
                    $scope.initStepBar();
                }, 10);

                $scope.initStepBar = function () {
                    $scope.model = HomeModel.data;
                    $scope.getMandateState();
                };

                $scope.getMandateState = function () {
                    if ((HomeModel.data.currentMandateSelected !== undefined)
                      &&(HomeModel.data.currentMandateSelected !== null)
                      &&(angular.equals(HomeModel.data.currentMandateSelected, {}) !== true)) {
                        var mandateNum = HomeModel.global.getMandateIndex(HomeModel.data.currentMandateSelected.Mandate.MandateID);
                        var mandateState = $scope.model.ListOfMandates[mandateNum].Mandate.Status;
                        $scope.stepBar.lastFunctionCalled = mandateState;

                        if (mandateState == 'created') {
                            $scope.stepBar.currentStepNum = 1;
                        } else if (mandateState == 'initiated') {
                            $scope.stepBar.currentStepNum = 2;
                        } else if (mandateState == 'validated') {
                            $scope.stepBar.currentStepNum = 2;
                        } else if (mandateState == 'amended') {
                            $scope.stepBar.currentStepNum = 3;
                        } else if (mandateState == 'revoked') {
                            $scope.stepBar.currentStepNum = 4;
                        } else {
                            $scope.stepBar.currentStepNum = 0;
                        }
                    } else {
                        $scope.stepBar.currentStepNum = 0;
                    }
                    var stepBarId = 'step-bar', _stepBarId = '_step-bar';
                    var steppers = $mdStepper(stepBarId);//, _steppers = $mdStepper(_stepBarId);
                    //steppers.clearError();
                    steppers.goto($scope.stepBar.currentStepNum-1);
                    //_steppers.goto($scope.stepBar.currentStepNum-1);
                    $scope.updateSelectedState();
                };

                $scope.updateSelectedState = function () {
                    var stepId = '', stepClass = '', stepNumberId = '', stepNumberClass = '';
                    $scope.resetSelectedState();
                    if ($scope.stepBar.currentStepNum > 0 && $scope.stepBar.currentStepNum < 5) {
                        if ($scope.stepBar.currentStepNum === 1) {
                            stepId = 'step-1';
                            stepClass = 'step-bar-initiate';
                            stepNumberId = 'step-number-1';
                            stepNumberClass = 'step-bar-number-initiate';
                        } else if ($scope.stepBar.currentStepNum === 2) {
                            stepId = 'step-2';
                            stepClass = 'step-bar-fill';
                            stepNumberId = 'step-number-2';
                            stepNumberClass = 'step-bar-number-fill';
                            $scope.hideState(1);
                        } else if ($scope.stepBar.currentStepNum === 3) {
                            stepId = 'step-3';
                            stepClass = 'step-bar-amend';
                            stepNumberId = 'step-number-3';
                            stepNumberClass = 'step-bar-number-amend';
                            $scope.hideState(1);
                            $scope.hideState(2);
                        } else if ($scope.stepBar.currentStepNum === 4) {
                            stepId = 'step-4';
                            stepClass = 'step-bar-revoke';
                            stepNumberId = 'step-number-4';
                            stepNumberClass = 'step-bar-number-revoke';
                            $scope.hideState(1);
                            $scope.hideState(2);
                            $scope.hideState(3);
                        }
                        var stateLabelTag = document.getElementById(stepId)
                        ,   stateNumberLabelTag = document.getElementById(stepNumberId);
                        angular.element(stateLabelTag).addClass(stepClass);
                        angular.element(stateNumberLabelTag).addClass(stepNumberClass);
                    }
                };

                $scope.resetSelectedState = function () {
                    var stepIdTab = ['step-1', 'step-2', 'step-3', 'step-4']
                    ,   stepClassTab = ['step-bar-initiate', 'step-bar-fill', 'step-bar-amend', 'step-bar-revoke']
                    ,   stepNumberIdTab = ['step-number-1', 'step-number-2', 'step-number-3', 'step-number-4']
                    ,   stepNumberClassTab = ['step-bar-number-initiate', 'step-bar-number-fill', 'step-bar-number-amend', 'step-bar-number-revoke']
                    ,   stateLabelTag, stateNumberLabelTag;

                    for (var idIndex = 0; idIndex < stepIdTab.length; idIndex++) {
                        for (var classIndex = 0; classIndex < stepClassTab.length; classIndex++) {
                            stateLabelTag = document.getElementById(stepIdTab[idIndex]);
                            stateNumberLabelTag = document.getElementById(stepNumberIdTab[idIndex]);
                            if ((stateLabelTag.className.indexOf('step-bar-initiate') != -1)
                             || (stateLabelTag.className.indexOf('step-bar-fill') != -1)
                             || (stateLabelTag.className.indexOf('step-bar-amend') != -1)
                             || (stateLabelTag.className.indexOf('step-bar-revoke') != -1)) {
                                angular.element(stateLabelTag).removeClass(stepClassTab[classIndex]);
                                angular.element(stateNumberLabelTag).removeClass(stepNumberClassTab[classIndex]);
                            }
                            if ((stateLabelTag.className.indexOf('step-bar-not-selected') != -1)) {
                                angular.element(stateLabelTag).removeClass('step-bar-not-selected');
                            }
                            if ((stateNumberLabelTag.className.indexOf('step-bar-number-not-selected') != -1)) {
                                angular.element(stateNumberLabelTag).removeClass('step-bar-number-not-selected');
                            }
                        }
                    }
                };

                $scope.hideState = function (stateNumber) {
                    var stepId = 'step-'+''+stateNumber+''
                    ,   stepClass = 'step-bar-not-selected'
                    ,   stepNumberId = 'step-number-'+''+stateNumber+''
                    ,   stepNumberClass = 'step-bar-number-not-selected';

                    var stateLabelTag = document.getElementById(stepId)
                    ,   stateNumberLabelTag = document.getElementById(stepNumberId);
                    angular.element(stateLabelTag).addClass(stepClass);
                    angular.element(stateNumberLabelTag).addClass(stepNumberClass);
                };

                /***********************************************************
                                    Home Services functions
                 ***********************************************************/

                HomeService.updateCurrentMandateToStepBarCtrl = function(_currentMandate) {
                    if ((_currentMandate !== null) && (_currentMandate !== undefined)) {
                        HomeModel.data.currentMandateSelected = _currentMandate;
                        $scope.model.currentMandateSelected = _currentMandate;
                    }
                    $scope.getMandateState();
                };
            }]);
}(angular));