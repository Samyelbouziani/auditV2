(function withAngular(angular) {
    'use strict';

    angular.module('BlocksCtrl.controller', [])
        .controller('BlocksCtrl', ['$window', '$scope', 'BlockchainModel', 'HomeModel', 'HomeService',
            function BlocksController($window, $scope, BlockchainModel, HomeModel, HomeService) {
                //console.log("BlocksCtrl called");

                HomeService.isFabricListening() ;
                console.log("BlockchainModel.global.isFabricListening")
                console.log(BlockchainModel.global.isFabricListening)
                $scope.blockchain;
                $scope.currentBlock;
                $scope.currentTransaction;
                $scope.currentUserSelected;

                function initView () {
                    $scope.currentUserSelected = HomeModel.data.currentUser;

                    if(BlockchainModel.global.isFabricListening){
                        var _blockchain = BlockchainModel.online.initBlockchainFromServer($scope.currentUserSelected).then(
                            (results)=> {
                                console.log(results);
                                $scope.blockchain = results;
                                $scope.currentBlock = $scope.blockchain.ListOfBlocks[$scope.blockchain.ListOfBlocks.length-1];
                                $scope.currentTransaction = $scope.currentBlock.transaction[0];
                                //$scope.currentUserSelected = HomeModel.data.currentUser;
                                var lastBlockindex = results.ListOfBlocks.length - 1,
                                    lastBlockNum = results.ListOfBlocks[lastBlockindex].blockNumber;
                                $scope.showBlockContent(lastBlockNum);
                            }, (err)=> {
                                console.log("erreur");
                            }
                        );

                    }
                    else {
                        $scope.blockchain = BlockchainModel.offline.initBlockchainOffline();
                        $scope.currentBlock = $scope.blockchain.ListOfBlocks[0];
                        $scope.currentTransaction = $scope.currentBlock.transaction[0];
                    
                        $scope.blockchain.ListOfBlocks = BlockchainModel.global.getAppropriateBlockchain($scope.currentUserSelected);
                        var lastBlockindex = $scope.blockchain.ListOfBlocks.length - 1,
                            lastBlockNum = $scope.blockchain.ListOfBlocks[lastBlockindex].blockNumber;
                        $scope.showBlockContent(lastBlockNum);
                    }
                }

                $scope.showBlockContent = function (blockIndex) {
                    var blockNum = BlockchainModel.global.getBlockIndexByNumber(blockIndex);
                    if (blockNum !== null) {
                        $scope.blockchain.ListOfBlocks[blockNum].new = false;
                        $scope.blockchain.ListOfBlocks[blockNum].showBlockContent = !$scope.blockchain.ListOfBlocks[blockNum].showBlockContent;
                        $scope.currentBlock = $scope.blockchain.ListOfBlocks[blockNum];
                        //$scope.hideAllTransactionsFromCurrentBlock();
                        $scope.currentTransaction = $scope.blockchain.ListOfBlocks[blockNum].transaction[0];
                        //FakeBlocksDatabase.updateLocalStorage();
                    } else {
                        console.log("Shouldn't happen");
                    }
                };

                // unused
                $scope.showBlockInfo = function () {
                    var blockIndex = $scope.currentBlock.blockNumber;
                    $scope.blockchain.ListOfBlocks[blockIndex].showBlockInfo = !$scope.blockchain.ListOfBlocks[blockIndex].showBlockInfo;
                    $scope.currentTransaction = {};
                    //$scope.currentBlock = $scope.blockchain[blockIndex];
                };

                $scope.showTransactionsInfo = function () {
                    var blockIndex = $scope.currentBlock.blockNumber;
                    $scope.blockchain.ListOfBlocks[blockIndex].showTransactionsInfo = !$scope.blockchain.ListOfBlocks[blockIndex].showTransactionsInfo;
                    //$scope.currentBlock = $scope.blockchain[blockIndex];
                };

                $scope.showTransaction = function (transac_number) {
                    var blockIndex = $scope.currentBlock.blockNumber;
                    for (var i=0;i<$scope.blockchain.ListOfBlocks[blockIndex].transaction.length; i++){
                        if($scope.blockchain.ListOfBlocks[blockIndex].transaction[i].transactionNumber == transac_number) {
                            $scope.currentTransaction = $scope.blockchain.ListOfBlocks[blockIndex].transaction[i];
                            var previousState = $scope.blockchain.ListOfBlocks[blockIndex].transaction[i].showTransaction;
                            $scope.hideAllTransactionsFromCurrentBlock();
                            $scope.blockchain.ListOfBlocks[blockIndex].transaction[i].showTransaction = !previousState;
                        }
                    }
                };

                $scope.hideAllTransactionsFromCurrentBlock = function () {
                    var blockIndex = $scope.currentBlock.blockNumber;
                    for (var i=0;i<$scope.blockchain.ListOfBlocks[blockIndex].transaction.length; i++){
                        $scope.blockchain.ListOfBlocks[blockIndex].transaction[i].showTransaction = false;
                    }
                };

                $scope.isChaincodeDataUpdated = function (label) {
                    var showUpdatedData = false;
                    if (($scope.currentTransaction !== undefined) 
                     && ($scope.currentTransaction.data !== null) 
                     && ($scope.currentTransaction.data !== undefined)) {
                        if ($scope.currentTransaction.data.function === "Initiate Mandate") {
                            if ((label === "Debtor Public Key")
                                || (label === "Creditor Public Key")
                                || (label === "Mandate ID")
                                || (label === "RUM")
                                || (label === "ICS")
                                || (label === "Amount")
                                || (label === "Periodicity")
                                || (label === "Type of payment")
                                || (label === "BIC")
                                || (label === "Mandate Manager")
                                || (label === "Location")
                                || (label === "Creditor Name")
                                || (label === "Creditor Address")
                                || (label === "Creditor BIC")
                                || (label === "Creditor IBAN")) {
                                showUpdatedData = true;
                            } else {
                                showUpdatedData = false;
                            }
                        } else if ($scope.currentTransaction.data.function === "Fill Mandate") {
                            if ((label === "Debtor Public Key")
                                || (label === "Creditor Public Key")
                                || (label === "Debtor IBAN")
                                || (label === "Debtor BIC")
                                || (label === "Debtor Name")
                                || (label === "Debtor Address")
                                || (label === "Mandate ID")) {
                                showUpdatedData = true;
                            } else {                            
                            showUpdatedData = false;
                            }
                        } else if ($scope.currentTransaction.data.function === "Amend Mandate") {
                            if ((label === "Debtor Public Key")
                                || (label === "Creditor Public Key")
                                || (label === "Debtor IBAN")
                                || (label === "Debtor BIC")
                                || (label === "Debtor Address")
                                || (label === "Mandate ID")) {
                                showUpdatedData = true;
                            } else {                            
                            showUpdatedData = false;
                            }
                        } else if ($scope.currentTransaction.data.function === "Revoke Mandate") {
                            if ((label === "Debtor Public Key")
                                || (label === "Creditor Public Key")
                                || (label === "Mandate ID")) {
                                showUpdatedData = true;
                            } else {                            
                            showUpdatedData = false;
                            }
                        } else {
                            showUpdatedData = false;
                        }
                    }
                    return showUpdatedData;
                };

                $scope.isLastHorizontalBar = function (blockNum) {
                    if ($scope.blockchain.ListOfBlocks.length == blockNum + 1) return false;
                    else return true;
                };

                $scope.isLastHorizontalBarReverseFilter = function (blockNum) {
                    if (blockNum == 0) return false;
                    else return true;
                };

                //unused
                $scope.addTransaction = function (){
                    BlockchainModel.offline.addTransaction();
                };

                initView();

                /***********************************************************
                                    Home Services functions
                 ***********************************************************/

                HomeService.updateCurrentUserToBlocksCtrl = function (_currentUser) {
                    $scope.currentUserSelected = _currentUser;
                    $scope.blockchain.ListOfBlocks = BlockchainModel.global.getAppropriateBlockchain(_currentUser);
                    initView();
                    //$scope.showBlockContent($scope.blockchain.ListOfBlocks.length - 1);
                };
            }]);
}(angular));
