// unused
(function withAngular(angular) {
  'use strict';
// 'ngResource','ngCookies'
  angular.module('Login.controller', [])
  .controller('LoginController', [ '$stateParams' , '$state', '$scope', 'httpServ', 'dbService',
    function LoginController($stateParams, $state, $scope, httpServ, dbService) {
		$scope.publicKey;
		$scope.privateKey;

		$scope.login = function() {
			var res, isServerListening = dbService.getServerState();
			if (isServerListening) {
                res = dbService.loginOnline($scope.publicKey, $scope.privateKey);
			} else {
                res = dbService.loginOffline($scope.publicKey, $scope.privateKey);
			}
            $state.go('home');
			console.log("User logged");
		}
  }]);
})(angular);