/*global angular*/
(function withAngular(angular) {
    'use strict';

    angular.module('Home.controller', [])
        .controller('HomeCtrl', ['$stateParams', '$state', '$scope', '$rootScope', '$mdDialog', 'HomeService', 'HomeModel', 'testServerRoute', 'designService', 'FilterService',
            function($stateParams, $state, $scope, $rootScope, $mdDialog, HomeService, HomeModel, testServerRoute, designService, FilterService) {
                //console.log("HomeCtrl called");

                $scope.searchMandate = FilterService.searchMandate.searchModel.Mandate;
                $scope.currentMandateSelected = {};
                $scope.currentUserSelected = HomeModel.data.currentUser;
                $scope.typeOfPayment = "";
                $scope.$watch('currentMandateSelected', function() {
                    var isMandateSelected = $scope.isCurrentMandateSelected();
                    if (isMandateSelected) {
                        if (($scope.currentMandateSelected.Mandate !== undefined)
                            && ($scope.currentMandateSelected.Mandate !== null)) {
                            $scope.typeOfPayment = HomeModel.global.setupTypeOfPayment($scope.currentMandateSelected.Mandate.MandateData.Periodicity);
                        } else {
                            $scope.typeOfPayment = "";
                        }
                    } else {
                        $scope.typeOfPayment = "";
                    }
                });

                function initView() {
                    HomeModel.global.initView();
                    $scope.model = HomeModel.data;
                }

                $scope.updateAccessRoles = function () {
                    HomeModel.global.updateAccessRoles();
                };

                $scope.showMandateData = function (typeOfUser) {
                    var dataToDisplay = $scope.model.showFunctionnality;
                    return designService.showMandateData(typeOfUser, dataToDisplay);
                };

                $scope.isCurrentMandateSelected = function () {
                    if (($scope.currentMandateSelected === undefined)
                      ||($scope.currentMandateSelected === null)
                      ||(angular.equals($scope.currentMandateSelected, {}) === true)){
                        return false;
                    } else {
                        return true;
                    }
                };

                // unused
                $scope.consultBlockchain = function (mandateNum) {
                    $state.go('blocks');
                };

                // unused
                $scope.hideAll = function() {
                    HomeModel.global.hideAll();
                };

                $scope.showInfo = function(mandateNum) {
                    HomeModel.global.showInfo(mandateNum);
                    $scope.currentMandateSelected = HomeModel.data.ListOfMandates[mandateNum];
                    //$scope.typeOfPayment = HomeModel.global.setupTypeOfPayment($scope.currentMandateSelected.Mandate.MandateData.Periodicity);
                    HomeService.updateCurrentMandateToStepBarCtrl($scope.currentMandateSelected);
                    //console.log($scope.currentMandateSelected);
                };

                /***********************************************************
                                        Mandate dialogs
                 ***********************************************************/

                $scope.showMandateButtons = function () {
                    if (($scope.model.showFunctionnality.showInitMandate === true)
                     || ($scope.model.showFunctionnality.showFillMandate === true)
                     || ($scope.model.showFunctionnality.showAmendMandate === true)
                     || ($scope.model.showFunctionnality.showRevokeMandate === true)) {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.isDebitor = function () {
                    if ($scope.currentUserSelected.typeOfUser === "debitor") {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.isCreditor = function () {
                    if ($scope.currentUserSelected.typeOfUser === "creditor") {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.isBank = function () {
                    if (($scope.currentUserSelected.typeOfUser === "creditorBank")
                     || ($scope.currentUserSelected.typeOfUser === "debitorBank")) {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.initiateMandate = function(ev) {
                    // HomeModel.offline.initiateMandate(ev, mandateNum);
                    // var initiateDialDirective = HomeModel.directives.initiateMandate;
                    // initiateDialDirective.targetEvent = ev;
                    //$mdDialog.show(initiateDialDirective)
                    $mdDialog.show({
                        controller: 'DialogCtrl',
                        controllerAs: 'DialCtrl',
                        bindToController: true,
                        templateUrl: 'views/dialog/dialog-initiate.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true
                    }).then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
                };

                $scope.fillMandate = function(ev) {
                    /*var fillDialDirective = HomeModel.directives.fillMandate;
                    fillDialDirective.targetEvent = ev;*/
                    $mdDialog.show({
                        controller: 'DialogCtrl',
                        controllerAs: 'DialCtrl',
                        bindToController: true,
                        templateUrl: 'views/dialog/dialog-fill.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true
                    }).then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
                };

                $scope.amendMandate = function(ev) {
                    /*var amendDialDirective = HomeModel.directives.amendMandate;
                    amendDialDirective.targetEvent = ev;*/
                    $mdDialog.show({
                        controller: 'DialogCtrl',
                        controllerAs: 'DialCtrl',
                        bindToController: true,
                        templateUrl: 'views/dialog/dialog-amend.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true
                    }).then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
                };

                $scope.revokeMandate = function(ev) {
                    /*var revokeDialDirective = HomeModel.directives.revokeMandate;
                    revokeDialDirective.targetEvent = ev;*/
                    $mdDialog.show({
                        controller: 'DialogCtrl',
                        controllerAs: 'DialCtrl',
                        bindToController: true,
                        templateUrl: 'views/dialog/dialog-revoke.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true
                    }).then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
                };

                $scope.testGetMandat = function() {
                    var result = HomeModel.online.testGetMandat()
                    .then(function successCallback(data) {
                        console.log("testGetMandat Sucess", data);
                    }, function errorCallback(data) {
                        console.log("testGetMandat Error", data);
                    });
                };

                /***********************************************************
                                Home Services functions
                 ***********************************************************/

                HomeService.submitInitiateMandateToHomeCtrl = function(_data) {
                    console.log('submitInitiateMandateToHomeCtrl called', _data);
                };

                HomeService.submitFillMandateToHomeCtrl = function(_data) {
                    console.log('submitFillMandateToHomeCtrl called', _data);
                };

                HomeService.submitAmendMandateToHomeCtrl = function(_data) {
                    console.log('submitAmendMandateToHomeCtrl called', _data);
                };

                HomeService.submitRevokeMandateToHomeCtrl = function(_data) {
                    console.log('submitRevokeMandateToHomeCtrl called', _data);
                };

                HomeService.updateCurrentUserToHomeCtrl = function (_currentUser) {
                    $scope.currentUserSelected = _currentUser;
                    HomeModel.data.currentUserSelected = _currentUser;
                    $scope.model.currentUserSelected = _currentUser;
                    HomeModel.data.currentUserSelected = {};
                    $scope.currentMandateSelected = {};
                    HomeModel.global.hideAll();
                    HomeService.updateCurrentMandateToStepBarCtrl($scope.currentMandateSelected);
                };

                HomeService.updateCurrentMandateToHomeCtrl = function (mandateNum) {
                    $scope.currentMandateSelected = {};
                    $scope.showInfo(mandateNum);
                    HomeService.updateCurrentMandateToStepBarCtrl($scope.currentMandateSelected);
                };

                // Call this function while loading the controller
                initView();
            }
        ])
}(angular));
