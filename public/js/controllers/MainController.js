// unused
(function withAngular(angular) {
  'use strict';

  angular.module('Main.controller', [])
  .controller('MainCtrl', ['$scope', 'designService',
    function MainController($scope, designService) {
        designService.showToast("We advise you to use Google Chrome for a better user experience.");
  }])

  .controller('ToastCtrl', function($scope, $mdToast, $mdDialog) {
    $scope.closeToast = function() {
      $mdToast
        .hide()
        .then(function() {
          //console.log("Toat closed");
          //isDlgOpen = false;
        });
    };
  });
}(angular));
