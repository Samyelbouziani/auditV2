(function withAngular(angular) {
    'use strict';

    angular.module('DialogCtrl.controller', [])
        .controller('DialogCtrl', ['$scope', '$mdDialog', 'HomeService', 'HomeModel',
            function ($scope, $mdDialog, HomeService, HomeModel) {
                //console.log("DialogController called");
                $scope.debitorKeys = ['58f87b7a7bca47239dba9a4a','58f87b7a87c9f2992b6ac5c7']; /*,'58cc07eae7e7f5ea129df893'*/
                $scope.periodicity = ['Once', 'Daily', 'Weekly', 'Monthly', 'Quarterly', 'Half-yearly', 'Annual'];
                $scope.owner = [];
                $scope.progressCircularState = false;
                $scope.currentUser = HomeModel.data.currentUser;
                $scope.currentMandate = HomeModel.data.currentMandateSelected;

                var date = new Date();
                var _date = new Date(date.getTime() + (2*1000*60*60)).toISOString().replace(/T/, ' ').replace(/\..+/, '');

                $scope.data = {
                    initiateMandate: {
                        from: $scope.currentUser.publicKey,
                        to: "",
                        rum: "FR"+Math.random()*1000000000000000000,
                        ics: $scope.currentUser.Ics,
                        amount: 0,
                        periodicity: "Monthly",
                        owner: $scope.owner[0],
                        creditorName: $scope.currentUser.Name,
                        creditorAddress: $scope.currentUser.Address,
                        creditorPublicKey: $scope.currentUser.publicKey,
                        debitorPublicKey: $scope.debitorKeys[0],
                        bic: $scope.currentUser.Bic,
                        location: "",
                        timestamp: _date
                    },
                    fillMandate: {
                        from: $scope.currentUser.publicKey,
                        to: "",
                        debitorName: $scope.currentUser.Name,
                        debitorAddress: $scope.currentUser.Address,
                        BIC:  $scope.currentUser.Bic,
                        IBAN:  "",
                        serviceTermsAccepted: false
                    },
                    amendMandate: {
                        from: $scope.currentUser.publicKey,
                        to: "",
                        BIC: "",
                        IBAN: "",
                        debitorAddress: ""
                    },
                    revokeMandate: {
                        from: $scope.currentUser.publicKey,
                        to: "",
                        boolRevoke: false
                    }
                };

                function initDialog () {
                    if ((HomeModel.data.currentMandateSelected !== undefined)
                     && (HomeModel.data.currentMandateSelected !== null)
                     && (angular.equals(HomeModel.data.currentMandateSelected, {}) !== true)){
                        $scope.data.amendMandate.BIC = HomeModel.data.currentMandateSelected.Mandate.DebitorData.Bic;
                        $scope.data.amendMandate.IBAN = HomeModel.data.currentMandateSelected.Mandate.DebitorData.Iban;
                        $scope.data.amendMandate.debitorAddress = HomeModel.data.currentMandateSelected.Mandate.DebitorData.Address;
                    }
                    for (var i = 0 ; i < HomeModel.data.ListOfUsers.length ; i++) {
                        if (HomeModel.data.ListOfUsers[i].typeOfUser == "creditor" || HomeModel.data.ListOfUsers[i].typeOfUser == "creditorBank") {
                            $scope.owner.push(HomeModel.data.ListOfUsers[i].userText)
                        }
                    }
                    $scope.data.initiateMandate.owner = $scope.owner[0];
                }

                $scope.hide = function() {
                    $mdDialog.hide();
                };

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.answer = function(answer) {
                    $mdDialog.hide(answer);
                };

                $scope.submitCreditorInitiateMandate = function (form) {
                    $scope.formState = false;
                    if (form.$valid) {
                        $scope.data.initiateMandate.to = $scope.data.initiateMandate.debitorPublicKey;
                        HomeService.submitInitiateMandateFromDialogCtrl($scope.data.initiateMandate);
                        HomeService.updateCurrentMandateToStepBarCtrl(null);
                        HomeModel.global.updateAccessRoles();
                        //HomeModel.global.showInfo(HomeModel.global.getMandateIndex($scope.currentMandate.Mandate.MandateID));
                        $scope.hide();
                    } else {
                        $scope.formState = true;
                    }
                };

                $scope.submitDebitorFillMandate = function (form) {
                    $scope.formState = false;
                    if (form.$valid) {
                        $scope.data.fillMandate.to = $scope.currentMandate.Mandate.CreditorPublicKey;
                        $scope.progressCircularState = true;
                        setTimeout(function () {
                            $scope.progressCircularState = false;
                            $scope.data.fillMandate.MandateID = HomeModel.data.currentMandateSelected.Mandate.MandateID;
                            HomeService.submitFillMandateFromDialogCtrl($scope.data.fillMandate);
                            HomeService.updateCurrentMandateToStepBarCtrl(null);
                            HomeModel.global.updateAccessRoles();
                            HomeModel.global.showInfo(HomeModel.global.getMandateIndex($scope.currentMandate.Mandate.MandateID));
                            $scope.hide();
                        }, 4000);
                    } else {
                        $scope.formState = true;
                    }
                };

                $scope.submitDebitorUpdatesMandate = function (form) {
                    $scope.formState = false;
                    if(form.$valid) {
                        $scope.data.amendMandate.to = $scope.currentMandate.Mandate.CreditorPublicKey;
                        $scope.data.amendMandate.MandateID = HomeModel.data.currentMandateSelected.Mandate.MandateID;
                        HomeService.submitAmendMandateFromDialogCtrl($scope.data.amendMandate);
                        HomeService.updateCurrentMandateToStepBarCtrl(null);
                        HomeModel.global.updateAccessRoles();
                        HomeModel.global.showInfo(HomeModel.global.getMandateIndex($scope.currentMandate.Mandate.MandateID));
                        $scope.hide();
                    }
                    else {
                        $scope.formState = true;
                    }
                };

                $scope.submitRevokeMandate = function (form) {
                    $scope.formState = false;
                    if (form.$valid) {
                        $scope.data.revokeMandate.to = $scope.currentMandate.Mandate.CreditorPublicKey;
                        $scope.data.revokeMandate.MandateID = HomeModel.data.currentMandateSelected.Mandate.MandateID;
                        HomeService.submitRevokeMandateFromDialogCtrl($scope.data.revokeMandate);
                        HomeService.updateCurrentMandateToStepBarCtrl(null);
                        HomeModel.global.updateAccessRoles();
                        HomeModel.global.showInfo(HomeModel.global.getMandateIndex($scope.currentMandate.Mandate.MandateID));
                        $scope.hide();
                    } else {
                        $scope.formState = true;
                    }
                };

                initDialog();
            }]);
}(angular));
