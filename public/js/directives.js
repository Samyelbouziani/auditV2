/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('HyperSEPA.directives', [
      'Directives.directive'
    ]);
}(angular));
