/*global angular*/
(function withAngular(angular, _) {
  'use strict';

  angular.module('HyperSEPA', [
    'ngMaterial',
    'ngMessages',
    'mdSteppers',
    //'material.svgAssetsCache',
    'ui.router',
    //'underscore',
    'ngAnimate',
    'HyperSEPA.filters',
    'HyperSEPA.providers',
    'HyperSEPA.services',
    'HyperSEPA.factories',
    'HyperSEPA.controllers',
    'HyperSEPA.directives'
  ])
  .constant('_', window._)
  .config(['$stateProvider', '$urlRouterProvider',
    function configurationFunction($stateProvider, $urlRouterProvider) {
      // Define the routes
      $stateProvider
      .state('app', {
        'url': '/app',
        'abstract': true,
        'views': {
          '': {
              'templateUrl': 'views/layout/index.html'
               /*,'controller': 'MainCtrl'*/
          }
        }
      })
      /*.state('connect', {
        'parent':'app',
        'url': '/connect',
        'views': {
          'content@app': {
            'templateUrl': 'views/connect/index.html',
            'controller': 'LoginController'
          }
        }
      })*/

      .state('home', {
          'parent':'app',
          'url': '/auditView',
          'views': {
              'content@app': {
                  'templateUrl': 'views/auditView/index.html',
                  'controller': 'AuditCtrl'
              }
          }
      })
      /*.state('blocks', {
          'parent':'app',
          'url': '/blocks',
          'views': {
              'content@app': {
                  'templateUrl': 'views/blockchain/index.html',
                  'controller': 'BlocksCtrl'
              }
          }
      });*/
      $urlRouterProvider.otherwise('/app/auditView');
  }])

  .config(function($mdThemingProvider) {
      // Extend the red theme with a different color and make the contrast color black instead of white.
      // For example: raised button text will be black instead of white.
      var neonRedMap = {
          '50': 'f9e1e6','100': 'f1b3bf','200': 'e78195','300': 'dd4e6b','400': 'd6284b',
          '500': 'cf022b','600': 'ca0226','700': 'c30120','800': 'bd011a','900': 'b20110',
          'A100': 'ffdbdd','A200': 'ffa8ac','A400': 'ff757b','A700': 'ff5c63',
          'contrastDefaultColor': 'light',
          'contrastDarkColors': ['50','100','200','A100','A200','A400','A700'],
          'contrastLightColors': ['300','400','500','600','700','800','900']
      };

      var neonOrangeMap = {
          '50': 'fef5e5','100': 'fde6bd','200': 'fbd591','300': 'f9c465','400': 'f8b844',
          '500': 'f7ab23','600': 'f6a41f','700': 'f59a1a','800': 'f39115','900': 'f1800c',
          'A100': 'ffffff','A200': 'fff3e8','A400': 'ffd8b5','A700': 'ffca9c',
          'contrastDefaultColor': 'light',
          'contrastDarkColors': ['50','100','200','300','400','500','600','700','800','900','A100','A200','A400','A700'],
          'contrastLightColors': []
      };

      var neonGreyMap = {
          '50': 'f5f5f4', '100': 'e5e5e5', '200': 'd4d4d3', '300': 'c2c2c1', '400': 'b5b5b4',
          '500': 'a8a8a7', '600': 'a0a09f', '700': '979796', '800': '8d8d8c', '900': '7d7d7c',
          'A100': 'ffffff', 'A200': 'fbfbd8', 'A400': 'ffffa1', 'A700': 'ffff87',
          'contrastDefaultColor': 'light',
          'contrastDarkColors': [ '50', '100', '200', '300', '400', '500', '600', '700', '800', 'A100', 'A200', 'A400', 'A700' ],
          'contrastLightColors': ['900']
      };

      // Register the new color palette map with the name <code>neonRed</code>
      $mdThemingProvider.definePalette('neonGrey', neonGreyMap);
      //$mdThemingProvider.definePalette('neonRed', neonRedMap);
      //$mdThemingProvider.definePalette('neonOrange', neonOrangeMap);

      // Use that theme for the primary intentions
      $mdThemingProvider.theme('default')
          .primaryPalette('neonGrey', {
              'default': '500',
              'hue-1': '300', // use shade 100 for the <code>md-hue-1</code> class
              'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
              'hue-3': '900' // use shade A100 for the <code>md-hue-3</code> class
          });
         /*.primaryPalette('neonRed', {
              'default': '500',
              'hue-1': '300', // use shade 100 for the <code>md-hue-1</code> class
              'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
              'hue-3': '900' // use shade A100 for the <code>md-hue-3</code> class
          })*/
          /*.accentPalette('neonOrange', {
              'default': '500',
              'hue-1': '300', // use shade 100 for the <code>md-hue-1</code> class
              'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
              'hue-3': '900' // use shade A100 for the <code>md-hue-3</code> class
          });*/
      /*$mdThemingProvider.theme('default')
          .primaryPalette('redPalette');*/
      /*$mdThemingProvider.theme('default')
          .primaryPalette('#CF022B')
          .accentPalette('orange');*/
  })
  .config(function($mdIconProvider) {
    $mdIconProvider
        .iconSet('info-outline', 'fonts/material-svg-icons/ic_info_outline_black_48px.svg',48)
        .iconSet('list-of-mandates', 'fonts/icons/list.svg',48)
        .iconSet('mandate-information', 'fonts/icons/info.svg',48)
        .iconSet('step-bar', 'fonts/icons/check.svg',48)
        .iconSet('actions', 'fonts/icons/pen.svg',48)
        .iconSet('personal-data', 'fonts/icons/person-black.svg',48)
        .iconSet('blocks-concerned', 'fonts/icons/blockchain.svg',48)
        .iconSet('block-info', 'fonts/icons/blocks.svg',48)
        .iconSet('transactions-info', 'fonts/icons/transaction.svg',48)
        .iconSet('chaincode-info', 'fonts/icons/contract.svg',48);
    // .icon('social:cake', 'fonts/icons/cake.svg',24);
  })

  .run(function($templateRequest) {
        var urls = [
          'fonts/material-svg-icons/ic_info_outline_black_48px.svg'
          // 'fonts/icons/sets/core-icons.svg',
          // 'fonts/icons/cake.svg',
          // 'fonts/icons/android.svg'
        ];

        // Pre-fetch icons sources by URL and cache in the $templateCache...
        // subsequent $templateRequest calls will look there first.

        angular.forEach(urls, function(url) {
          $templateRequest(url);
        });

  });
}(angular, _));
