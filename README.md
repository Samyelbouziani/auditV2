# Project
POC prélèvement de mandat avec Hyperledger

## Lancer la web app
Depuis le dossier dummy, lancer la commande :
```bash
node server.js # Without Hyperledger
node server.js blockchain # With Hyperledger
```
Puis ouvrir Google Chrome à l'URL : http://localhost:8081/#/app/home

## Reset Front-end
Depuis la console du navigateur
```bash
localStorage.removeItem('ListOfMandates');
localStorage.removeItem('Blockchain');
```

# Utiliser l'outil GIT
## Récupérer le projet
```bash
git clone http://slnxbanquearchi01/poc-mandat/dummy.git
```

## Réaliser un commit (après le sien)
```bash
git status
git add .
git status
git commit -m "Nom du commit"
git push origin master
```
## Réaliser un commit (après celui de quelqu'un d'autre)
```bash
git status
git stash
git pull
git stash pop
git add .
git status
git commit -m "Nom du commit"
git push origin master
```

# Documentation
## Material Design
https://material.angularjs.org/latest/

## Material design icons
https://material.io/icons/,

https://materialdesignicons.com/,

http://google.github.io/material-design-icons/,

http://www.flaticon.com/packs/material-design


## $mdThemingProvider
http://mcg.mbitson.com/#!?orange=%23f7ab23&themename=mcgtheme

## $stateProvider
http://slides.com/timkindberg/ui-router#/1

## Step bar
https://github.com/eberlitz/material-steppers

## Couleurs Sopra
| Couleur       |     Valeur      |
| ------------- | --------------- |
| Rouge         |     #CF022B     |
| Rouge foncé   |     #960000     |
| Orange clair  |     #F7A823     |
| Orange        |     #EF7D00     |
| Orange foncé  |     #E14B0F     |
| Gris clair    |     #ECEDED     |
| Gris          |     #A8A8A7     |
| Gris foncé    |     #7B7C7E     |
| Noir          |     #000000     |
| Noir clair    |     #222222     |
| Violet        |     #8C2350     |
| Bleu          |     #41738C     |
| Vert          |     #2DAA64     |

# Note
La librairie material-stepper a été modifiée -> Nous avons rajouté des id aux balises span de chaque step