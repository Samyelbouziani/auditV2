Spec du chaincode à implémenter


Description des structures de données du CC:

Mandate {
    MandateID  // ICS+RUM pour l'instant
    Status // current status of the mandate
    CreditorPublicKey // name+address pour l'instant
    DebitorPublicKey  // name+address pour l'instant 
    MandateData struct // sera crypté
    CreditorData struct // sera crypté
    DebitorData struct // sera crypté
}

Status {
    StatusName string,
    StatusDescription string
}
possibleStatus[Status] = [
    {"created", "mandate created by creditor" },
    {"validated", "mandate validated by debitor"},
    {"revoked", "mandate revoked"}]

MandateData {
    Rum         string
    Amount      float
    Periodicity string
    Timestamp // timestamp de création
}

// Liste of possible periodicity
possiblePeriodicity = [ "monthly", "yearly" ]

CreditorData {
    Ics
    Name
    Address
}

DebitorData {
    Iban
    Name
    Address
}

Liste des fonctions à implémenter:

getMandatesByDebitorPk (debitorPk string) string
getMandatesByCreditorPk (creditorPk string)
    -> JSON({
            mandateId
        }) TODO

getMandateById(string mandateId)
    -> JSON({
            cre
        }) TODO

getMetaDataById(string mandateId)
    -> JSON({
        }) TODO
        

createMandate(debitorPk, creditorPk, ICS, name, address, RUM, amount, periodicity)
    -> err, mandateId

revokeMandate(MandateId)

debitorFillMandate(MandateId, IBAN, name, address)
    -> err

debitorAmendMandate(MandateId, dataName, value)
    -> err


TODO:
    - ajouter status dans le chaincode
