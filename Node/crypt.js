var crypt = function () {};



var util = require('util');
var crypto = require('crypto'),
             algorithm = 'aes-256-ctr';


crypt.encrypt= function (text, password){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
crypt.decrypt = function (text,password){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}
 
module.exports = crypt ; 


/*

var constants = require('constants');


function toBuf(str, encoding) {
  encoding = encoding || 'binary';
  if (util.isString(str)) {
    if (encoding === 'buffer')
      encoding = 'binary';
    str = new Buffer(str, encoding);
  }
  return str;
}

exports._toBuf = toBuf;


var crypto = require('crypto');
var confidential_data = req.body.debtorName+req.body.debtorAddress+req.body.coordBancaire;
console.log("Name :  "+confidential_data); 

var hash = crypto.createHash('md5').update(confidential_data).digest('hex');
console.log("Hash : "+hash); 
 // var key = req.body.key;
var padding = constants.RSA_PKCS1_OAEP_PADDING;

console.log(); 
console.log("Name before encrypt :  "+confidential_data); 
var crypted_data = encrypt(confidential_data,req.body.sign)
console.log("Name after encrypt  :  "+crypted_data); 
var decrypted = decrypt(crypted_data,req.body.sign);
console.log("Name after decrypt  :  "+decrypted); 
console.log(); 

*/
