var dbWrapper = require("./databaseWrapper.js")("http://couchdb:couchdb@127.0.0.1:5984", false);

function basicCb (err) {
    if (err) {
        console.log(err);
        return false;
    } else {
        return true;
    }
};

dbWrapper.destroy("hypersepa", function (err) {
    if (!basicCb(err)) return;
    dbWrapper.create("hypersepa", function (err) {
        if (!basicCb(err)) return;

        dbWrapper.use("hypersepa");
        dbWrapper.insert({ OK: true, Hello: "World"}, "HI0", function (err) {
            if (!basicCb(err)) return;
            dbWrapper.insert({ OK: true, Hello: "Galaxy"}, "HI1", function (err) {
                if (!basicCb(err)) return;
                dbWrapper.insert({ OK: true, Hello: "Universe"}, "HI2", function (err) {
                    if (!basicCb(err)) return;
                    var doc = dbWrapper.getDocByName("HI1", function (err, doc) {
                        if (!basicCb(err)) return;
                        console.log(doc);
                        dbWrapper.createView(
                            "viewOne",
                            {"map": `function(doc){emit(doc._id, doc.Hello)}`},
                            function(err) {
                                if (!basicCb(err)) return;
                                dbWrapper.view(
                                    "viewOne",
                                    {keys: ['HI1']},
                                    function(err, body) {
                                        if (!basicCb(err)) return;
                                        console.log(body.rows);
                                    }
                                );
                            }
                        );
                    });
                });
            });
        });
    });
});
