
// data struct in hypersepa database
//{
//  "RUM": "2014-123ABC35",
//  "ICS" : "FR72ZZZ123456",
//  "creditorName" : {"firstname" : "Gérard", "lastname": "DUPONT"} ,
//  "creditorAddress" : "102 avenue Victor Hugo 75116 Paris",
//  "debtorBankingInfo" : { "BIC" : "PSSTFRPPB",
//                          "IBAN": "FR472004101001117715OZ02201" },
//  "debtorName" : {"firstname" : "Gérard", "lastname": "DUPONT"} ,
//  "debtorAddress" : "128 avenue de Wagram 75017 Paris",
//  "signatureInfo" : { "date" : 1489571416,
//                      "place" : "Paris"},
//  "otherInfo" : {}
//}

module.exports = function (url, debug) {

// Init module dbWrapper struct
var dbWrapper = { };
dbWrapper.db = undefined;

// check argument debug and set default if undefined
if (!(typeof debug === "undefined"))
    dbWrapper.debug = false;
else
    dbWrapper.debug = debug;

// check argument url and set default if undefined
if (typeof url === "undefined")
    dbWrapper.url = 'http://localhost:5984';
else
    dbWrapper.url = url;

// require nano with the database server url
var nano = require('nano')(dbWrapper.url);

/** Creates a database
 *  @param dbName the name of the database to create
 *  @param onCreate the callback called after the creation of the db
 */
dbWrapper.create = function (dbName, onCreate) {
    nano.db.create(dbName, onCreate);
};

/** Destroys a database
 *  @param dbName the name of the database to destroy
 *  @param onDestroy the callback called after the destruction of the db
 */
dbWrapper.destroy = function (dbName, onDestroy) {
    nano.db.destroy(dbName, onDestroy);
};

/** Specifies to the module a specific database to use from now
 *  @param dbName the database to use
 */
dbWrapper.use = function (dbName) {
    dbWrapper.db = nano.use(dbName);
};

/** Inserts the JSON struct to the currently used db
 *  @param data the document to insert
 *  @param onInsert the callback to call after insertion
 */
dbWrapper.insert = function (data, name, onInsert) {
    dbWrapper.db.insert(data, name, onInsert);
};

// TODO useless if generic interface to couchdb
//      check to be done on the user's side of the interface
// can be added as a check before insert
dbWrapper.checkDataStruct = function () {};

/** Deletes the last revision found of the specified document
 *  @param docName the name of teh doc to delete
 *  @param onDelete the callback to call after trying to delete
 */
dbWrapper.delete = function (docName, onDelete) {
    dbWrapper.db.attachment.get(docName, "_rev", function (err, rev) {
        if (err) {
            onDelete(err, rev);
        }
        else {
            dbWrapper.db.destroy(docName, rev, onDelete);
        } 
    });
};

// TODO
// By Name (id ?)
dbWrapper.getDocByName = function (docName, onGet) {
    dbWrapper.db.get(docName, onGet);
};

// TODO
// get by attribute
//  sort ?
// update doc
// get all docs

dbWrapper.createView = function (viewName, functionsStruct, onCreateView) {
    var doc = { "views" : {} };
    doc.views[viewName] = functionsStruct;
    dbWrapper.db.insert(doc, "_design/"+viewName, onCreateView);
    viewName = "_design/" + viewName;
}

dbWrapper.view = function (viewName, params, onGetView) {
    if (typeof params === "function")
        dbWrapper.db.view(viewName, viewName, params);
    else
        dbWrapper.db.view(viewName, viewName, params, onGetView);
}




return dbWrapper;
}
