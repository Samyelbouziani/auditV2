var appRouter = function(app) {
    var dbAccess = require("./databaseWrapper.js");
    var chainCode = require("./databaseWrapper.js");
    var crypt = require("./crypt.js");
    var log_server =  require("./log_server.js");

    var log4js = require('log4js');
    var logger = log4js.getLogger("routes.js");
    var path = require('path');

    var makeLogErr = function (fctName, err) {
        logger.error("");
        logger.error("****************************************");
        logger.error(fctName + ' : FAILED');
        logger.error("----------------------------------------"); 
        logger.error(err.stack ? err.stack : err);
        logger.error("****************************************");
        logger.error("");            
    };
    var makeLogSucc = function (fctName, msg) {
        logger.info("");
        logger.info("****************************************");
        logger.info(fctName + ' : SUCCESSFUL');
        if (msg) {
            logger.info("----------------------------------------"); 
            logger.info(msg);
        }
        logger.info("****************************************");
        logger.info("");            
    };
    
   var args = process.argv.slice(2);
    if(args[0] == "blockchain") {
        var isFabricListening = true ; 
        var blockchain = require("./hyperledger/blockchain.js");

        var configFilePath = path.join(__dirname, './hyperledger/config.json');
        var ccConfigFilePath = path.join(__dirname, './hyperledger/config_cc.json');
        var networkName = "labconfig_default";

        blockchain.init(configFilePath, networkName, ccConfigFilePath)
        .then(
            () => {
                logger.info("blockchain module: init sucessful");
            },
            (err) => {
                logger.error("blockchain module: init Failed");
            });
    }
    else {
        var isFabricListening = false ;         
        console.log("fabric is not Listening");
    }


    function renameMandate (m) {
        var em = {
            MandateID: m.mandateId, //.slice(0,10)
            mandateID: m.mandateId.slice(0,10),            
            Status: m.status,
            CreditorPublicKey: m.creditorPK,
            DebitorPublicKey: m.debitorPK,
            MandateData: {
                Rum: m.mandateData.rum,
                Timestamp: m.mandateData.timestamp,
                Location: m.mandateData.location,
                Amount: m.mandateData.amount,
                Periodicity: m.mandateData.periodicity,
                TypeOfPayment: "",
                Owner: m.mandateData.owner
            },
            CreditorData: {
                Ics: m.creditorData.ics,
                Name: m.creditorData.name,
                Address: m.creditorData.address,
                Iban: m.creditorData.iban,
                Bic: m.creditorData.bic
            },
            DebitorData: {
                Name: m.debitorData.name,
                Address: m.debitorData.address,
                Iban: m.debitorData.iban,
                Bic: m.debitorData.bic
            }
        }
        return em;
    }

    function editMandateList(l) {
        var el = l.map(function (m) {
            var em = {
                Mandate : renameMandate(m)

            };
            return em;
        });
        return el
    }

    app.get('/test', function(req, res, next){
        res.setHeader('Content-Type', 'application/json');
        console.log(isFabricListening);
        logger.info("/test: fabric is listening ?: " + isFabricListening);  
        res.json(isFabricListening);
    });

    app.get('/db/mandat', function(req, res, next){
        console.log("\n---------------------------");
        console.log("GET MANDAT IN DB\n---------------------------\n");
        res.setHeader('Content-Type', 'application/json');
        
        // var results = dbAccess.getMandat();
        var results = "e" ;
        if (results == null) results = [{extraData: "emptyBlockchain"}];
        console.log("getMandat success : ");  
        console.log(results);
        console.log("\n---------------------------\n");
        res.json(results);

    });

    app.get('/db/login', function(req, res, next){
        console.log("\n---------------------------");
        console.log("GET LOGIN IN DB\n---------------------------\n");
        res.setHeader('Content-Type', 'application/json');
        
        // var results = dbAccess.getLogin();
        var results = "e" ;
        if (results == null) results = [{extraData: "emptyBlockchain"}];
        console.log("getLogin success : ");  
        console.log(results);
        console.log("\n---------------------------\n");
        res.json(results);

    });




 
    app.post('/chaincode/getMandatesByCreditorPK', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var _userdata = blockchain.getMandatesByCreditorPK(req.body.pubk)
        .then( 
            (results) => {
                if (results === undefined)  results = "";
                makeLogSucc(
                    "/chaincode/getMandatesByCreditorPK",
                    JSON.stringify(results));
                res.json(editMandateList(results));
            },
            (err) => {
                makeLogErr("/chaincode/getMandatesByCreditorPK", err);
            });
    });

    app.post('/chaincode/getMandatesByDebitorPK', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var _userdata = blockchain.getMandatesByDebitorPK(req.body.pubk)
        .then( 
            (results) => {
                if (results === undefined)  results = "";
                makeLogSucc(
                    "/chaincode/getMandatesByDebitorPK",
                    JSON.stringify(results));
                res.json(editMandateList(results));
            },
            (err) => {
                makeLogErr("/chaincode/getMandatesByDebitorPK", err);
            });
    });

   app.post('/chaincode/getMandateById', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var _userdata = blockchain.getMandateById(req.body)
        .then(
            (results) => {
                if (results === undefined)  results = "";
                makeLogSucc(
                    "/chaincode/getMandateById",
                    JSON.stringify(results));
                res.json(editMandateList(results));
            },
            (err) => {
                makeLogErr("/chaincode/getMandateById", err);
            });
    });

   app.post('/chaincode/getPossibleStatus', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var _userdata = blockchain.getPossibleStatus(req.body)
        .then(
            (results) => {
                if (results === undefined)  results = "";
                makeLogSucc(
                    "/chaincode/getPossibleStatus",
                    JSON.stringify(results));
                res.json(results);
            },
            (err) => {
                makeLogErr("/chaincode/getMandateById", err);
            });
    });

    app.get('/chaincode/getEveryBlock', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var _userdata = blockchain.getEveryBlock()
        .then( 
            (results) => {
                if (results === undefined)  results = "";
                makeLogSucc(
                    "/chaincode/getEveryBlock",
                    JSON.stringify(results));
                res.json(results);
            },
            (err) => {
                makeLogErr("/chaincode/getEveryBlock", err);
            });
    });

    app.post('/chaincode/createMandate', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var data = req.body;
        blockchain.createMandate(
            data.debitorPublicKey,
            data.creditorPublicKey,
            data.ics,
            data.bic,
            data.creditorName,
            data.creditorAddress,
            data.owner,
            data.rum,
            data.amount,
            data.periodicity,
            data.location)
        .then(
            (results) => {
                makeLogSucc(
                    "/chaincode/createMandate",
                    JSON.stringify(results));
                res.json(results);
            },
            (err) => {
                makeLogErr("/chaincode/createMandate", err);
                res.json(err);
                
            });

    });
 
    app.post('/chaincode/debitorFillMandate', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var data = req.body;
        blockchain.debitorFillMandate(
            data.MandateID,
            data.IBAN,
            data.BIC,
            data.debitorName,
            data.debitorAddress
            )
        .then(
            (results) => {
                makeLogSucc(
                    "/chaincode/debitorFillMandate",
                    JSON.stringify(results));

                res.json(results);

            },
            (err) => {
                makeLogErr("/chaincode/debitorFillMandate", err);
                res.json(err);

            });
    });


    app.post('/chaincode/debitorAmendMandate', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var data = req.body;
        blockchain.debitorAmendMandate(
            data.MandateID,
            data.IBAN,
            data.BIC,
            data.debitorAddress
            )
        .then(
            (results) => {
                makeLogSucc(
                    "/chaincode/debitorAmendMandate",
                    JSON.stringify(results));
                res.json(results);

            },
            (err) => {
                makeLogErr("/chaincode/debitorAmendMandate", err);
                res.json(err);

            });
    });

    app.post('/chaincode/creditorRevokeMandate', function (req, res, next) {
        res.setHeader('Content-Type', 'application/json');

        var data = req.body;
        blockchain.creditorRevokeMandate(
            data.MandateID
            )
        .then(
            (results) => {
                makeLogSucc(
                    "/chaincode/creditorRevokeMandate",
                    JSON.stringify(results));
                res.json(results);

            },
            (err) => {
                makeLogErr("/chaincode/creditorRevokeMandate", err);
                res.json(err);

            });
    });
}

module.exports = appRouter;




