'use strict';
// Launch fabirc/launchScript/tesInvokeAndQuery.sh before this one
var log4js = require('log4js');
var logger = log4js.getLogger("initiateMandates.js");

var crypto = require('crypto');

var path = require('path');
var blockchain = require('./blockchain.js');

var makeErr = function (fctName, err) {
    logger.error("main, call "+fctName+" failed:\n"+
        err.stack ? err.stack : err);
};

var configFilePath = path.join(__dirname, './config.json');
var ccConfigFilePath = path.join(__dirname, './config_cc.json');
var networkName = "labconfig_default";

var mList = require('./initialMandates.json');

var promiseArr = [];

blockchain.init(configFilePath, networkName, ccConfigFilePath)
.then(
    () => {
        // Make an array of createMandate promises corresponding to
        // each mandate to add
        for (var i=0, l=mList.length; i<l; i++) {
            var p = blockchain.createMandate(
                    mList[i].DebitorPublicKey,
                    mList[i].CreditorPublicKey,
                    mList[i].CreditorData.Ics,
                    mList[i].CreditorData.Bic,
                    mList[i].CreditorData.Name,
                    mList[i].CreditorData.Address,
                    mList[i].MandateData.Owner,
                    mList[i].MandateData.Rum,
                    mList[i].MandateData.Amount,
                    mList[i].MandateData.Periodicity,
                    mList[i].MandateData.Location);
            promiseArr.push(p);
        }
        // Effectively create Mandates
        return Promise.all(promiseArr);
    },
    (err) => {
        makeErr("setup", err);
        process.exit();
    })
.then(
    (txIdArr) => {

        // Display tx_id for each create
        for (let i=0, l=mList.length; i<l; i++) {
            console.log("mandate created, tx_id:"+i+" : "+txIdArr[i]);
        }

        // Debitor Fill Mandate
        promiseArr = [];
        for (var i=0, l=mList.length; i<l; i++) {
            if (mList[i].DebitorData.Name != "") {
                let s = mList[i].CreditorData.Ics + mList[i].MandateData.Rum;
                let mandateId = crypto.createHash('sha256').update(s, 'ascii').digest('hex');
                var pDebFill = blockchain.debitorFillMandate(
                    mandateId,
                    mList[i].DebitorData.Name,
                    mList[i].DebitorData.Address,
                    mList[i].DebitorData.Iban,
                    mList[i].DebitorData.Bic);
                promiseArr.push(pDebFill);
            }
        }
        return Promise.all(promiseArr);
    },
    (err) => {
        makeErr("createMandate");
        process.exit();
    })
.then(
    (txIdArr) => {
        process.exit();
    },
    (err) => {
        makeErr("debitorFillMandate");
        process.exit();
    })
.catch((err) => {
    logger.error("CAUGHT ERROR in blockchain functions execution !");
    logger.error(err.stack ? err.stack : err);
    logger.error("");
    process.exit();
});

