'use strict';

var log4js = require('log4js');
var logger = log4js.getLogger("test.js");

var path = require('path');
var helper = require("./helper.js");


var makeErr = function (fctName, err) {
    logger.error("main, call "+fctName+" failed:\n"+
        err.stack ? err.stack : err);
};

var configFilePath = path.join(__dirname, './config.json');
var ccConfigFilePath = path.join(__dirname, './config_cc.json');
var networkName = "labconfig_default";

var testDebitorPublicKey = "4testDebitorPublicKey";
var testCreditorPublicKey = "4testCreditorPublicKey";
var testIcs = "4testIcs";
var testCreditorName = "4testCreditorName";
var testCreditorAddress = "4testCreditorAddress";
var testRum = "4testRum";
var testAmount = "444.4";
var testPeriodicity = "monthly";

var argsCreate = [testDebitorPublicKey, testCreditorPublicKey, testIcs, testCreditorName, testCreditorAddress, testRum, testAmount, testPeriodicity];

helper.setup(configFilePath, networkName, ccConfigFilePath)
.then(
    () => {
        logger.info("setup ended")

//          Promise.resolve(helper.query('getMandateById', ['5caaedc1c9a7e8f07d0f36fc3b2361df433f55127cced62d66174429a3a6ec58']));
//          Promise.resolve(helper.query('getMandatesByDebitorPk', ['testDebitorPublicKey']));

        helper.invoke('createMandate', argsCreate).then(
            () => {
                logger.info("invoke successful");
                new Promise(resolve => setTimeout(resolve, 3000)).then(
                    () => {
                        Promise.resolve(helper.query('getMandatesByDebitorPk', ['4testDebitorPublicKey']))
                    });
            });
        

        return;
    },
    (err) => {
        return makeErr("setup", err);
    });
