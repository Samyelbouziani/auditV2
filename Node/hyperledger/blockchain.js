'use strict';

var log4js = require('log4js');
var logger = log4js.getLogger("blockchain.js");
var moment = require("moment");
const crypto = require('crypto');

var helper = require("./helper.js");

module.exports.init = function (networkConfigPath, networkName, ccConfigPath) {
    return helper.setup(networkConfigPath, networkName, ccConfigPath);
};

function editMandateJson(m) {
    function edit (m) {
        return {
            mandateId: m.MandateId,
            status: m.MandateStatus,
            creditorPK: m.CreditorPublicKey,
            debitorPK: m.DebitorPublicKey,
            mandateData: {
                owner: m.MandateData.Owner,
                rum: m.MandateData.Rum,
                amount: m.MandateData.Amount,
                periodicity: m.MandateData.Periodicity,
                timestamp: m.MandateData.Timestamp,
                location: m.MandateData.Location
            },
            creditorData: {
                ics: m.CreditorData.Ics,
                bic: m.CreditorData.Bic,
                name: m.CreditorData.Name,
                address: m.CreditorData.Address
            },
            debitorData: {
                iban: m.DebitorData.Iban,
                bic: m.DebitorData.Bic,
                name: m.DebitorData.Name,
                address: m.DebitorData.Address
            }
        };
    }

    // Call edit if mandate Array as input
    // (We consider an array of Key, Record structure,
    // the Record being the mandate)
    if (m.constructor === Array) {
        // if empty array return immediately
        if (m.length == 0) {
            return [];
        } else {
            var mandateArray = [];
            for (let i=0, l = m.length; i<l; i++) {
                mandateArray[i] = edit(m[i].Record);
            }
            return mandateArray;
        }
    } else {
        return edit(m);
    }
}

// TODO check why array of payloads
// TODO check that only one mandate is returned
module.exports.getMandateById = getMandateById;

function getMandateById (s) {
    return new Promise((resolve, reject) => {
        helper.query("getMandateById", [s])
        .then(
            (payload) => {
                var resJson = JSON.parse(payload[0]);
                return resolve(editMandateJson(resJson));
            })
    });
};

module.exports.getMandatesByDebitorPK = function (s) {
    return new Promise ((resolve, reject) => {
        helper.query("getMandatesByDebitorPk", [s])
        .then(
            (payload) => {
                var resJson = JSON.parse(payload[0]);
                return resolve(editMandateJson(resJson));
            },
            (err) => {
                return reject(err);
            });
    });
}

module.exports.getMandatesByCreditorPK = function (s) {
    return new Promise((resolve, reject) => {
        helper.query("getMandatesByCreditorPk", [s])
        .then(
            (payload) => {
                var resJson = JSON.parse(payload[0]);
                return resolve(editMandateJson(resJson));
            },
            (err) => {
                return reject(err);
            });
    });
}

module.exports.getPossibleStatus = function() {
    return new Promise((resolve, reject) => {
        helper.query("getPossibleStatus", [])
        .then(
            (payload) => {
                var resJson = JSON.parse(payload[0]);
                return resolve(resJson);
            },
            (err) => {
                return reject(err);
            });
    });
}

module.exports.createMandate = function(debitorPK, creditorPK, ics, bic, name, address, owner, rum, amount, periodicity, location) {
    return new Promise((resolve, reject) => {

        // Convert Number type amount to string
        var strAmount = amount.toString();

        helper.invoke(
            "createMandate",
            [debitorPK,
            creditorPK,
            ics,
            bic,
            name,
            address,
            owner,
            rum,
            strAmount,
            periodicity,
            location])
        .then(
            (tx_id) => {
                return resolve(tx_id);
            },
            (err) => {
                return reject(err);
            });
    });
}

module.exports.debitorFillMandate = function (mandateId, iban, bic, name, address) {
    return new Promise((resolve, reject) => {
        helper.invoke(
            "debitorFillMandate",
            [mandateId,
            iban,
            bic,
            name,
            address])
        .then(
            (payload) => {
                return resolve();
            },
            (err) => {
                return reject(err);
            });
    });
}

module.exports.debitorEditMandate = function (mandateId, dataName, value) {
    return new Promise((resolve, reject) => {

        function dataNameToMandateDataName (dataName) {
            switch(dataName) {
                case "iban":
                    return "Iban"
                case "bic":
                    return "Bic"
                case "name":
                    return "Name"
                case "address":
                    return "Address"
            }
        }

        helper.invoke(
            "debitorEditMandate",
            [mandateId,
            dataNameToMandateDataName(dataName),
            value])
        .then(
            (payload) => {
                return resolve();
            },
            (err) => {
                return reject(err);
            });
    });
}

module.exports.debitorAmendMandate = function (mandateId, iban, bic, address) {
    return new Promise((resolve, reject) => {

        helper.invoke(
            "debitorAmendMandate",
            [mandateId,
            iban,
            bic,
            address])
        .then(
            (payload) => {
                return resolve();
            },
            (err) => {
                return reject(err);
            });
    });
}

module.exports.creditorRevokeMandate = function (mandateId) {
    return new Promise((resolve, reject) => {

        helper.invoke("revokeMandate",[mandateId])
        .then(
            (payload) => {
                return resolve();
            },
            (err) => {
                return reject(err);
            });
    });
}

function getBlock (blockNumber) {
    return new Promise((resolve, reject) => {

    // Error when trying to get genesis block
    if (blockNumber == 0) {
        return reject(new Error("Error getBlock: " +
            "Can not get block 0 (genesis block)"));
    }

    helper.getChain().queryBlock(blockNumber)
    .then(
        (block) => {
            var returnedBlock = blockToJson(block);
            var result;

            if (returnedBlock.transactions[0].actions[0].proposal.args["Mandate ID"] !== undefined) {
                var result = getMandateById(returnedBlock.transactions[0].actions[0].proposal.args["Mandate ID"])
                .then(
                    (mandateFromChaincode) => {
                        console.log("*************************************************************************************");
                        console.log(mandateFromChaincode);
                                                console.log("*************************************************************************************");

                        returnedBlock.transactions[0].actions[0].proposal.args["Creditor Public Key"] = mandateFromChaincode.creditorPK;
                        returnedBlock.transactions[0].actions[0].proposal.args["Debtor Public Key"] = mandateFromChaincode.debitorPK;

                        return returnedBlock;
                    },
                    (err) => {
                        makeLogErr("getBlock", err);
                        console.log(err);

                    });
            } else {
                result = returnedBlock;
            }
            
            return resolve(result);  // return resolve(blockToJson(block));
        },
        (err) => {
            return reject(new Error(
                "Error getBlock: " +
                err.stack ? err.stack : err));
        });

    });
}
module.exports.getBlock = getBlock;

function blockToJson (block) {

    var grpc = require('grpc');
    var commonProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/common/common.proto').common;
    var transProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/transaction.proto').protos;
    var responseProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/proposal_response.proto').protos;
    var proposalProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/proposal.proto').protos;
    var eventProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/chaincodeevent.proto').protos;
    var chaincodeProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/chaincode.proto').protos;
    try {

        // Extracting the BlockHeader
        var jsonNumber = block.header.number.toString();
        var jsonPrevious_hash = block.header.previous_hash.toBase64();
        var jsonData_hash = block.header.data_hash.toBase64();


        // Extracting the BlockMetadata
        var metadata = block.metadata.metadata;
        var jsonMetadataArr = [];
        for (let i=0, il=metadata.length; i<il; i++) {
            let m = commonProto.Metadata.decode(metadata[i]);
            var jsonM = {};
            jsonM.value = m.value.toUTF8();
            var jsonSigArr = [];
            for (let j=0, jl=m.signatures.length; j<jl; j++) {
                let ms = m.signatures[j];
                let mssh = commonProto.SignatureHeader.decode(ms.signature_header);
                var jsonSig = {};
                jsonSig.creator = mssh.creator.toUTF8();
                jsonSig.nonce = mssh.nonce.toBase64();
                jsonSig.signature = ms.signature.toBase64();
                jsonSigArr.push(jsonSig);
            }
            jsonM.signatures = jsonSigArr;
            jsonMetadataArr.push(jsonM);
        }


        // Extracting the BlockData
        var jsonEnvelopeArr = [];
        for (var txIndex=0, txArrLength=block.data.data.length; txIndex<txArrLength; txIndex++) {
            var envelope = commonProto.Envelope.decode(block.data.data[txIndex]);
            var payload = commonProto.Payload.decode(envelope.payload);

            var jsonEnvelope = {};

            // Extracting Envelope -> Header
            var channel_header = commonProto.ChannelHeader.decode(payload.header.channel_header);
            jsonEnvelope.signature = envelope.signature.toBase64();
            jsonEnvelope.version = channel_header.version;
            // If there is a timestamp, create a string
            jsonEnvelope.timestamp = null;
            if (channel_header.timestamp != null) {
                let s = channel_header.timestamp.seconds*1000;
                s += (channel_header.timestamp.nanos / 1000000);
                jsonEnvelope.timestamp = moment(s).format('YYYY-MM-DD h:mm:ss');
            }
            jsonEnvelope.channel_id = channel_header.channel_id;
            jsonEnvelope.tx_id = channel_header.tx_id;

            var extension = proposalProto.ChaincodeHeaderExtension.decode(channel_header.extension)
            jsonEnvelope.payload_visibility = extension.payload_visibility.toUTF8();
            jsonEnvelope.chaincode_id = extension.chaincode_id;

            var signature_header = commonProto.SignatureHeader.decode(payload.header.signature_header);

            jsonEnvelope.creator = signature_header.creator.toUTF8();
            jsonEnvelope.nonce = signature_header.nonce.toBase64();


            // Extracting Envelope -> Transaction
            var tx = transProto.Transaction.decode(payload.data);

            // Extracting Envelope -> Transaction -> TransactionActions[]
            var jsonTxActionArr = [];
            for (var txActionIndex=0, txActionL=tx.actions.length;
                txActionIndex < txActionL;
                txActionIndex++) {

                var jsonTxAction = {};

                // Extracting Header
                var txHeader =
                    commonProto.Header.decode(tx.actions[txActionIndex].header);

                jsonTxAction.channelHeader = txHeader.channel_header.toUTF8();
                jsonTxAction.signatureHeader = txHeader.signature_header.toBase64();

                // Extracting ChaincodeActionPayload
                var chaincodeActionPayload = transProto.ChaincodeActionPayload.decode(tx.actions[txActionIndex].payload);

                // Extracting ChaincodeActionPayload -> ChaincodeProposalPayload
                var chaincodeProposalPayload = proposalProto.ChaincodeProposalPayload.decode(chaincodeActionPayload.chaincode_proposal_payload);
                var chaincodeProposalPayloadInput = chaincodeProto.ChaincodeInput.decode(chaincodeProposalPayload.input);
                // TODO verifier pourquoi args[0]
                var chaincodeProposalPayloadInputSpec = chaincodeProto.ChaincodeSpec.decode(chaincodeProposalPayloadInput.args[0]);

                jsonTxAction.proposal = {};
                jsonTxAction.proposal.type = chaincodeProposalPayloadInputSpec.type;
                jsonTxAction.proposal.chaincode_id = chaincodeProposalPayloadInputSpec.chaincode_id;

                jsonTxAction.proposal.args = {};
                // Adding field name for convenience in front-end
                if (chaincodeProposalPayloadInputSpec.input.args.length > 0) {
                    switch (chaincodeProposalPayloadInputSpec.input.args[0].toUTF8()) {
                        case "createMandate":
                            jsonTxAction.proposal.args["Function called"] = chaincodeProposalPayloadInputSpec.input.args[0].toUTF8();
                            jsonTxAction.proposal.args["Debtor Public Key"] = chaincodeProposalPayloadInputSpec.input.args[1].toUTF8();
                            jsonTxAction.proposal.args["Creditor Public Key"] = chaincodeProposalPayloadInputSpec.input.args[2].toUTF8();
                            jsonTxAction.proposal.args["ICS"] = chaincodeProposalPayloadInputSpec.input.args[3].toUTF8();
                            jsonTxAction.proposal.args["Creditor BIC"] = chaincodeProposalPayloadInputSpec.input.args[4].toUTF8();
                            jsonTxAction.proposal.args["Creditor Name"] = chaincodeProposalPayloadInputSpec.input.args[5].toUTF8();
                            jsonTxAction.proposal.args["Creditor Address"] = chaincodeProposalPayloadInputSpec.input.args[6].toUTF8();
                            jsonTxAction.proposal.args["Mandate Manager"] = chaincodeProposalPayloadInputSpec.input.args[7].toUTF8();
                            jsonTxAction.proposal.args["RUM"] = chaincodeProposalPayloadInputSpec.input.args[8].toUTF8();
                            jsonTxAction.proposal.args["Amount"] = chaincodeProposalPayloadInputSpec.input.args[9].toUTF8();
                            jsonTxAction.proposal.args["Periodicity"] = chaincodeProposalPayloadInputSpec.input.args[10].toUTF8();
                            jsonTxAction.proposal.args["Location"] = chaincodeProposalPayloadInputSpec.input.args[11].toUTF8();
                            // Add mandateId to those args for convenience in front-end
                            let s = jsonTxAction.proposal.args["ICS"] + jsonTxAction.proposal.args["RUM"];
                            jsonTxAction.proposal.args["Mandate ID"] = crypto.createHash('sha256').update(s, 'utf8').digest('hex');
                            break;
                        case "debitorFillMandate":
                            jsonTxAction.proposal.args["Function called"] = chaincodeProposalPayloadInputSpec.input.args[0].toUTF8();
                            jsonTxAction.proposal.args["Mandate ID"] = chaincodeProposalPayloadInputSpec.input.args[1].toUTF8();
                            jsonTxAction.proposal.args["Debtor IBAN"] = chaincodeProposalPayloadInputSpec.input.args[2].toUTF8();
                            jsonTxAction.proposal.args["Debtor BIC"] = chaincodeProposalPayloadInputSpec.input.args[3].toUTF8();
                            jsonTxAction.proposal.args["Debtor Name"] = chaincodeProposalPayloadInputSpec.input.args[4].toUTF8();
                            jsonTxAction.proposal.args["Debtor Address"] = chaincodeProposalPayloadInputSpec.input.args[5].toUTF8();
                            break;
                        case "debitorEditMandate":
                            jsonTxAction.proposal.args["Function called"] = chaincodeProposalPayloadInputSpec.input.args[0].toUTF8();
                            jsonTxAction.proposal.args["Mandate ID"] = chaincodeProposalPayloadInputSpec.input.args[1].toUTF8();
                            jsonTxAction.proposal.args["Modified Data Key"] = chaincodeProposalPayloadInputSpec.input.args[2].toUTF8();
                            jsonTxAction.proposal.args["New Value"] = chaincodeProposalPayloadInputSpec.input.args[3].toUTF8();
                            break;
                        case "debitorAmendMandate":
                            jsonTxAction.proposal.args["Function called"] = chaincodeProposalPayloadInputSpec.input.args[0].toUTF8();
                            jsonTxAction.proposal.args["Mandate ID"] = chaincodeProposalPayloadInputSpec.input.args[1].toUTF8();
                            jsonTxAction.proposal.args["Debtor IBAN"] = chaincodeProposalPayloadInputSpec.input.args[2].toUTF8();
                            jsonTxAction.proposal.args["Debtor BIC"] = chaincodeProposalPayloadInputSpec.input.args[3].toUTF8();
                            jsonTxAction.proposal.args["Debtor Address"] = chaincodeProposalPayloadInputSpec.input.args[4].toUTF8();
                            break;
                        case "creditorRevokeMandate":
                            jsonTxAction.proposal.args["Function called"] = chaincodeProposalPayloadInputSpec.input.args[0].toUTF8();
                            jsonTxAction.proposal.args["Mandate ID"] = chaincodeProposalPayloadInputSpec.input.args[1].toUTF8();
                            break;
                    }
                }
//              jsonTxAction.proposal.args = [];
//              for (let i=0; i<chaincodeProposalPayloadInputSpec.input.args.length; i++) {
//                  jsonTxAction.proposal.args[i] = chaincodeProposalPayloadInputSpec.input.args[i].toUTF8();
//              }

                // Extracting ChaincodeEndorsedAction -> endorsements(Endorsement[])
                jsonTxAction.endorsements = [];
                var endorsements = chaincodeActionPayload.action.endorsements;

                for (var enIdx=0, enl=endorsements.length; enIdx<enl; enIdx++) {

                    var endorsement = chaincodeActionPayload.action.endorsements[enIdx];
                    var jsonEndorsement = {};
                    jsonEndorsement.endorser = endorsement.endorser.toUTF8();
                    jsonEndorsement.signature = endorsement.signature.toBase64();
                    jsonTxAction.endorsements.push(jsonEndorsement);
                }

                // Extracting ChaincodeEndorsedAction -> ProposalResponsePayload
                var proposalResponsePayload = responseProto.ProposalResponsePayload.decode(chaincodeActionPayload.action.proposal_response_payload);

                // Extracting hash
                var proposal_hash = proposalResponsePayload.proposal_hash;
                jsonTxAction.proposalResponse = {};
                jsonTxAction.proposalResponse.proposalHash = proposal_hash.toBase64();

                // Extracting extension (Response)
                var chaincodeAction = proposalProto.ChaincodeAction.decode(proposalResponsePayload.extension);
                jsonTxAction.proposalResponse.results = chaincodeAction.results.toUTF8();
                jsonTxAction.proposalResponse.response = chaincodeAction.response;
                jsonTxAction.proposalResponse.response.payload = chaincodeAction.response.payload.toUTF8();

                var chaincodeActionEvents = eventProto.ChaincodeEvent.decode(chaincodeAction.events);
                jsonTxAction.proposalResponse.events = chaincodeActionEvents;


                // Pushing the transaction action to the transaction action array
                jsonTxActionArr.push(jsonTxAction);
            }
            jsonEnvelope.actions = jsonTxActionArr;
        }
        jsonEnvelopeArr.push(jsonEnvelope);
    }
    catch(err) {
        logger.error(err);
        process.exit();
    }

    var ret = {
        number: jsonNumber,
        previous_hash: jsonPrevious_hash,
        data_hash: jsonData_hash,
        metadata: jsonMetadataArr,
        transactions: jsonEnvelopeArr
    };

    return ret;
}

function getChainHeight () {
    return new Promise((resolve, reject) => {
        helper.getChain().queryInfo()
        .then(
            (info) => {
                return resolve(info.height.toNumber());
            },
            (err) => {
                return reject(
                    new Error("Error getChainHeight: "+
                        err.stack ? err.stack : err));
            });
    });
}
module.exports.getChainHeight = getChainHeight;

function getEveryBlock () {

    var arr = [];
    var l;
    return getChainHeight().then(
        (height) => {
            l = height;
            for (var i=1; i<l; i++) {
                arr.push(getBlock(i));
            }
            return Promise.all(arr);
        },
        (err) => {
            return Promise.reject(
                new Error("Error getEveryBlock: " +
                    err.stack ? err.stack : err));
        });
}
module.exports.getEveryBlock = getEveryBlock;

module.exports.testMetaData = function () {
    return new Promise((resolve, reject) => {
        console.log("\n-------------------------");
        console.log(" queryInfo ");
        console.log("-------------------------")
        helper.getChain().queryInfo()
        .then(
            (info) => {
                console.log(info);

                console.log("\n-------------------------");
                console.log(" queryBlock(2) ");
                console.log("-------------------------")
                return helper.getChain().queryBlock(2);
            })
        .then(
            (block) => {
                console.log("-------------------------")
                console.log(" block ");
                console.log("-------------------------")
                console.log(block);

                var grpc = require('grpc');
                var commonProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/common/common.proto').common;
                var transProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/transaction.proto').protos;
                var responseProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/proposal_response.proto').protos;
                var proposalProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/proposal.proto').protos;
                var eventProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/chaincodeevent.proto').protos;
                var chaincodeProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/chaincode.proto').protos;
                try {

                    console.log("-------------------------")
                    console.log(" BlockHeader -> number");
                    console.log("-------------------------")
                    console.log(block.header.number.toString());
                    console.log();

                    console.log("-------------------------")
                    console.log(" BlockHeader -> previous_hash");
                    console.log("-------------------------")
                    console.log(block.header.previous_hash.toBase64());
                    console.log();

                    console.log("-------------------------")
                    console.log(" BlockHeader -> data_hash");
                    console.log("-------------------------")
                    console.log(block.header.data_hash.toBase64());
                    console.log();

                    console.log("-------------------------")
                    console.log(" BlockMetaData -> metadata");
                    console.log("-------------------------")
                    var metadata = block.metadata.metadata;
                    for (let i=0, il=metadata.length; i<il; i++) {
                        let m = commonProto.Metadata.decode(metadata[i]);
                        console.log("----value:");
                        console.log(m.value.toUTF8());
                        console.log("----signatures:");
                        for (let j=0, jl=m.signatures.length; j<jl; j++) {
                            let ms = m.signatures[j];
                            console.log("--------signature_header:");
                            let mssh = commonProto.SignatureHeader.decode(ms.signature_header);
                            console.log("------------creator:", mssh.creator.toUTF8());
                            console.log("------------nonce:", mssh.nonce.toBase64());
                            console.log("--------signature:")
                            console.log(ms.signature.toBase64());
                            console.log("*******")
                        }
                        console.log("-------------------------");
                    }
                    console.log();

                    console.log("-------------------------")
                    console.log(" data[]");
                    console.log("-------------------------")
                    console.log(block.data.data);
                    console.log();

                    for (var txIndex=0, txArrLength=block.data.data.length; txIndex<txArrLength; txIndex++) {

                        var envelope = commonProto.Envelope.decode(block.data.data[txIndex]);
                        var payload = commonProto.Payload.decode(envelope.payload);
                        var channel_header = commonProto.ChannelHeader.decode(payload.header.channel_header);

                        console.log("-------------------------")
                        console.log(" Envelope");
                        console.log("-------------------------")
                        console.log(envelope);
                        console.log();

                        console.log("-------------------------")
                        console.log(" Envelope -> signature");
                        console.log("-------------------------")
                        console.log(envelope.signature.toBase64());
                        console.log();


                        console.log("-------------------------")
                        console.log(" Envelope -> payload -> header -> channel_header ");
                        console.log("-------------------------")
                        console.log(channel_header);
                        console.log();

                        var extension = proposalProto.ChaincodeHeaderExtension.decode(channel_header.extension)
                        console.log("-------------------------")
                        console.log(" Envelope -> payload -> header -> channel_header -> ChaincodeHeaderExtension -> payload_visibility");
                        console.log("-------------------------")
                        console.log(extension.payload_visibility.toUTF8());
                        console.log();

                        console.log("-------------------------")
                        console.log(" Envelope -> payload -> header -> channel_header -> ChaincodeHeaderExtension -> ChaincodeID");
                        console.log("-------------------------")
                        console.log(extension.chaincode_id);
                        console.log();


                        var signature_header = commonProto.SignatureHeader.decode(payload.header.signature_header);
                        console.log("-------------------------")
                        console.log(" Envelope -> payload -> header -> signature_header -> creator");
                        console.log("-------------------------")
                        console.log(signature_header.creator.toUTF8());
                        console.log()

                        console.log("-------------------------")
                        console.log(" Envelope -> payload -> header -> signature_header -> nonce");
                        console.log("-------------------------")
                        console.log(signature_header.nonce.toBase64());
                        console.log()


                        var tx = transProto.Transaction.decode(payload.data);
                        console.log("-------------------------")
                        console.log(" Transaction");
                        console.log("-------------------------")
                        console.log(tx);

                        var txHeader = commonProto.Header.decode(tx.actions[0].header);
                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> Header -> channel_header ");
                        console.log("-------------------------")
                        console.log(txHeader.channel_header.toUTF8());


                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> Header -> signature_header ");
                        console.log("-------------------------")
                        console.log(txHeader.signature_header.toBase64());

                        var chaincodeActionPayload = transProto.ChaincodeActionPayload.decode(tx.actions[0].payload);
                        var chaincodeProposalPayload = proposalProto.ChaincodeProposalPayload.decode(chaincodeActionPayload.chaincode_proposal_payload);
                        var chaincodeProposalPayloadInput = chaincodeProto.ChaincodeInput.decode(chaincodeProposalPayload.input);
                        var chaincodeProposalPayloadInputSpec = chaincodeProto.ChaincodeSpec.decode(chaincodeProposalPayloadInput.args[0]);
                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> ChaincodeActionPayload -> ChaincodeProposalPayload -> ChaincodeInput ");
                        console.log("-------------------------")
                        console.log(chaincodeProposalPayloadInputSpec);
                        var args = chaincodeProposalPayloadInputSpec.input.args;
                        for (let i=0,l=args.length; i<l; i++) {
                            console.log("arg "+i+": "+args[i].toUTF8());
                        }


//                      var chaincodeProposalPayloadTransientMap = chaincodeProposalPayload.TransientMap;
//                      console.log("-------------------------")
//                      console.log(" TransactionAction 0 -> ChaincodeActionPayload -> ChaincodeProposalPayload -> TransientMap ");
//                      console.log("-------------------------")
//                      console.log(chaincodeProposalPayloadTransientMap);


                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> ChaincodeActionPayload -> ChaincodeEndorsedAction -> endorsements");
                        console.log("-------------------------")
                        console.log(chaincodeActionPayload.action.endorsements);

                        var endorsement = chaincodeActionPayload.action.endorsements[0];
                        var endorser = endorsement.endorser;
                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> ChaincodeActionPayload -> ChaincodeEndorsedAction -> Endorsement[0] -> endorser");
                        console.log("-------------------------")
//                      endorser.printDebug();
//                      console.log(endorser.toBuffer().toString('ascii'));
                        console.log(endorser.toUTF8());

                        var signature = endorsement.signature;
                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> ChaincodeActionPayload -> ChaincodeEndorsedAction -> Endorsement[0] -> signature");
                        console.log("-------------------------")
//                      signature.printDebug();
//                      console.log(signature.toBuffer().toString('ascii'));
                        console.log(signature.toBase64());

                        var proposalResponsePayload = responseProto.ProposalResponsePayload.decode(chaincodeActionPayload.action.proposal_response_payload);
                        var proposal_hash = proposalResponsePayload.proposal_hash;
                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> ChaincodeActionPayload -> ChaincodeEndorsedAction -> ProposalResponsePayload -> proposal_hash");
                        console.log("-------------------------")
                        console.log(proposal_hash.toBase64());

                        var chaincodeAction = proposalProto.ChaincodeAction.decode(proposalResponsePayload.extension);
                        console.log("-------------------------")
                        console.log(" TransactionAction 0 -> ChaincodeActionPayload -> ChaincodeEndorsedAction -> ProposalResponsePayload -> extension(ChaincodeAction)");
                        console.log("-------------------------");
                        console.log(chaincodeAction.response.payload.toUTF8());
                        console.log();
                        console.log(chaincodeAction.results.toUTF8());
                        console.log();
                        console.log(chaincodeAction);

                    }

                    process.exit();
                }
                catch(err) {
                    logger.error(err);
                    process.exit();
                }

/*
                console.log("\n-------------------------");
                console.log(" queryTransaction(fb2eddb6e89073fe6cca8e49a640df0fb64a231ff40fc7fe82e29658f799f628) ");
                console.log("-------------------------")
                return helper.getChain().queryTransaction("c5814dfac8d0a1e8c55e63407f5f713eb94a90986550536bc6f9e7826d2b9b3b");
            })
        .then(
            (processed_transaction) => {
                var grpc = require('grpc');
                var commonProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/common/common.proto').common;
                var transProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/transaction.proto').protos;
                var responseProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/proposal_response.proto').protos;
                var proposalProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/proposal.proto').protos;
                var eventProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/chaincodeevent.proto').protos;
                var chaincodeProto = grpc.load(__dirname + '/node_modules/fabric-client/lib/protos/peer/chaincode.proto').protos;
                logger.info(' Chain queryTransaction() returned processed tranaction is valid='+processed_transaction.validationCode);
                try {
                    var payload = commonProto.Payload.decode(processed_transaction.transactionEnvelope.payload);
                    var channel_header = commonProto.ChannelHeader.decode(payload.header.channel_header);
                    console.log("-------------------------")
                    console.log(' Chain queryTransaction - transaction ID :: %s:', channel_header.tx_id);
                    console.log("-------------------------")
                    console.log(" proc_tx ");
                    console.log("-------------------------")
                    console.log(processed_transaction);
                    console.log("-------------------------")
                    console.log(" proc_tx -> transactionEnvelope -> payload ");
                    console.log("-------------------------")
                    console.log(payload);
                    console.log("-------------------------")
                    console.log(" proc_tx -> transactionEnvelope -> payload -> header -> channel_header");
                    console.log("-------------------------")
                    console.log(channel_header);
                    console.log("-------------------------")
                    console.log(" proc_tx -> transactionEnvelope -> payload -> header -> channel_header -> extension");
                    console.log("-------------------------")
                    var ch_ext = proposalProto.ChaincodeHeaderExtension.decode(channel_header.extension);
                    console.log(ch_ext);
                    console.log("-------------------------")
                    console.log(" proc_tx -> transactionEnvelope -> payload -> data (Transaction) ");
                    console.log("-------------------------")
                    var tx = transProto.Transaction.decode(payload.data);
                    console.log(tx);
                }
                catch(err) {
                    logger.error(err);
                    process.exit();
                }
                resolve();
*/
            })
        .catch(
            (err) => {
                reject(err);
                process.exit();
            });

    });
}

/*
TODO:
    - specifier la structure des metadata
        getMandateMetaDataById(string mandateId)
*/
