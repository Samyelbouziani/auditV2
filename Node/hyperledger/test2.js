'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger("test2.js");

var path = require('path');
var blockchain = require('./blockchain.js');

var makeErr = function (fctName, err) {
    logger.error("main, call "+fctName+" failed:\n"+
        err.stack ? err.stack : err);
};

var configFilePath = path.join(__dirname, './config.json');
var ccConfigFilePath = path.join(__dirname, './config_cc.json');
var networkName = "labconfig_default";

blockchain.init(configFilePath, networkName, ccConfigFilePath)
.then(
    () => {
        logger.info("setup ended");
        return blockchain.getPossibleStatus();
    },
    (err) => {
        return makeErr("setup", err);
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getPossibleStatus()');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        return blockchain.createMandate(
            "test2DebitorPK",
            "test2CreditorPK",
            "test2ICS",
            "test2Bic",
            "test2Name",
            "test2Address",
            "test2Owner",
            "test2Rum",
            2.45,
            "test2Periodicity",
            "test2Location"
            );
    })
.then((res) => { return new Promise((resolve,reject) => { return setTimeout(resolve(res), 3000);}) })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.createMandate("test2DebitorPK", "test2CreditorPK", "test2ICS", "test2Bic", "test2Name", "test2Address", "test2Owner", "test2Rum", 2.45, "test2Periodicity", "test2Location");');
        console.log("----------------------------------------");
        console.log("OKAY");
        console.log("****************************************");
        console.log("");

        return blockchain.getMandateById('dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba')
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getMandateById("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba")');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        return blockchain.getMandatesByCreditorPK("test2CreditorPK");
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getMandatesByCreditorPK("test2CreditorPK")');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        return blockchain.getMandatesByDebitorPK("test2DebitorPK");
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getMandatesByDebitorPK("test2DebitorPK")');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        return blockchain.debitorFillMandate(
            "dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba",
            "test2FILLEDIban",
            "test2FILLEDBic",
            "test2FILLEDName",
            "test2FILLEDAddress"
            );
    })
.then((res) => { return new Promise((resolve,reject) => { return setTimeout(resolve(res), 3000);}) })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.debitorFillMandate("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba", "test2FILLEDIban", "test2FILLEDBic", "test2FILLEDName", "test2FILLEDAddress");');
        console.log("----------------------------------------");
        console.log("OKAY");
        console.log("****************************************");
        console.log("");

        return blockchain.getMandatesByCreditorPK("test2CreditorPK");
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getMandatesByCreditorPK("test2CreditorPK")');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        return blockchain.debitorAmendMandate("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba", "edited_iban", "edited_bic", "edited_address");
    })
.then((res) => { return new Promise((resolve,reject) => { return setTimeout(resolve(res), 3000);}) })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.debitorAmendMandate("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba", "edited_iban", "edited_bic", "edited_address")');
        console.log("----------------------------------------");
        console.log("OKAY");
        console.log("****************************************");
        console.log("");

        return blockchain.getMandatesByCreditorPK("test2CreditorPK");
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getMandatesByCreditorPK("test2CreditorPK")');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        return blockchain.debitorEditMandate("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba", "iban", "test2AMENDEDIban");
    })
.then((res) => { return new Promise((resolve,reject) => { return setTimeout(resolve(res), 3000);}) })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.debitorEditMandate("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba", "iban", "test2AMENDEDIban")');
        console.log("----------------------------------------");
        console.log("OKAY");
        console.log("****************************************");
        console.log("");

        return blockchain.getMandatesByCreditorPK("test2CreditorPK");
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getMandatesByCreditorPK("test2CreditorPK")');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        return blockchain.creditorRevokeMandate("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba");
    })
.then((res) => { return new Promise((resolve,reject) => { return setTimeout(resolve(res), 3000);}) })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.creditorRevokeMandate("dedc4d107aa508b48f81698b2c2bbd4d2e1417a2cdb9e54c1580cf94ed6b79ba")');
        console.log("----------------------------------------");
        console.log("OKAY");
        console.log("****************************************");
        console.log("");

        return blockchain.getMandatesByCreditorPK("test2CreditorPK");
    })
.then(
    (res) => {
        console.log("");
        console.log("****************************************");
        console.log('blockchain.getMandatesByCreditorPK("test2CreditorPK")');
        console.log("----------------------------------------");
        console.log(res);
        console.log("****************************************");
        console.log("");

        process.exit();
    })
.catch((err) => {
    console.log("CAUGHT ERROR in blockchain functions execution !");
    console.log(err.stack ? err.stack : err);
    console.log("");
    process.exit();
});

