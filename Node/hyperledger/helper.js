'use strict';

var log4js = require('log4js');
var logger = log4js.getLogger('helper.js');

var path = require('path');
var fs = require('fs');
var util = require('util');

var hfc = require('fabric-client');
var utils = require('fabric-client/lib/utils.js');
var Peer = require('fabric-client/lib/Peer.js');
var Orderer = require('fabric-client/lib/Orderer.js');
var EventHub = require('fabric-client/lib/EventHub.js');
var User = require('fabric-client/lib/User.js');
var copService = require('fabric-ca-client/lib/FabricCAClientImpl.js');

var configFilePath;
var ccConfig;

var client = new hfc();
var configOrgs;
var chain;
var eventhub = new EventHub();
var user;

module.exports.getChain = function () { return chain; }
module.exports.getEventHub = function () { return eventhub; }

function makeMakeErr (callingFctName) {
    return function (fctName, reject, err) {
        return reject(new Error(
            callingFctName + ', call ' + fctName + ' failed:\n' +
            err.stack ? err.stack : err
        ));
    }
}


// getSubmitter
function getSubmitter() {
    return new Promise((resolve, reject) => {

    var makeErr = makeMakeErr("getSubmitter");

    var userOrg = ccConfig.user.userOrg;
    var username = ccConfig.user.username;
    var password = ccConfig.user.userpw;
    var userCaUrl = configOrgs[userOrg].ca;
    var userMspid = configOrgs[userOrg].mspid;

    client.getUserContext(username)
    .then(
        (tmp_user) => {
            if (!(tmp_user && tmp_user.isEnrolled())) {
                // need to enroll it with CA server
                var cop = new copService(userCaUrl);

                var member;
                return cop.enroll({
                    enrollmentID: username,
                    enrollmentSecret: password
                })
                .then(
                    (enrollment) => {
                        logger.info(
                            'Successfully enrolled user \'' +
                            username + '\''
                        );
                        member = new User(username, client);
                        return member.setEnrollment(
                            enrollment.key,
                            enrollment.certificate,
                            userMspid
                        );
                    },
                    (err) => {
                        return makeErr("cop.enroll", reject, err); 
                    })
                .then(
                    () => { return client.setUserContext(member); },
                    (err) => {
                        return makeErr("member.setEnrollment", reject, err); 
                    })
                .then(
                    () => { return resolve(member); },
                    (err) => {
                        return makeErr("client.setUserContext", reject, err); 
                    }
                );
            }
            else { resolve(tmp_user); }
        },
        (err) => {
            return makeErr("client.getUserContext", reject, err); 
        });
    });
}

module.exports.setup = function (
        _configFilePath,
        networkName,
        ccConfigFilePath) {
    return new Promise((resolve, reject) => {

    var makeErr = makeMakeErr("setup");

    // Initializing global variables with config files
    configFilePath = _configFilePath;
    hfc.addConfigFile(configFilePath);
    ccConfig = require(ccConfigFilePath);
    configOrgs = hfc.getConfigSetting(networkName);

    var userPeerName = ccConfig.user.userPeerName;
    var username = ccConfig.user.username;
    var userpw = ccConfig.user.userpw;
    var userOrg = ccConfig.user.userOrg;

    var userCaUrl = configOrgs[userOrg].ca;
    var userMspid = configOrgs[userOrg].mspid;

    chain = client.newChain(ccConfig.channelName);
    eventhub.setPeerAddr(
            configOrgs[userOrg][userPeerName].events
    );
    eventhub.connect();

    // set up the chain to use 'peer0' for
    // both requests and events
    if (configOrgs.hasOwnProperty(userOrg) && typeof configOrgs[userOrg][userPeerName] !== 'undefined') {
        let peer = new Peer(configOrgs[userOrg][userPeerName].requests);
        chain.addPeer(peer);
        let orderer = new Orderer(configOrgs.orderer.url);
        chain.addOrderer(orderer);
    }
    else {
        reject(new Error(
            "setup, organization " + userOrg +
            " or peer " + userPeerName + " doesn't exist\n"
        ));
    }

    hfc.newDefaultKeyValueStore({
        path: '/tmp/hfc-test-kvs_' + configOrgs[userOrg].name
    })
    .then(
        (store) => {
            client.setStateStore(store);
            getSubmitter()
            .then(
                (admin) => {
                    user = admin;
                    return resolve();
                },
                (err) => {
                    return makeErr("getSubmitter", reject, err);
                })
        },
        (err) => {
            return makeErr(
                "hfc.newDefaultKeyValueStore", reject, err
            );
        });
    });
}


module.exports.query = function (fcn, args) {
    return new Promise((resolve, reject) => {

    var makeErr = makeMakeErr("query");

    var chaincodeId = ccConfig.chaincodeId;
    var chaincodeVersion = ccConfig.chaincodeVersion;
    var nonce = utils.getNonce();
    var tx_id = tx_id = chain.buildTransactionID(nonce, user);

    var request = {
        txId: tx_id,
        nonce: nonce,
        chainId: chain._name,
        chaincodeId : chaincodeId,
        chaincodeVersion : chaincodeVersion,
        fcn: fcn,
        args: args
    };

    // send query
    return chain.queryByChaincode(request)
    .then(
        (response_payloads) => {
            if (response_payloads) {
                for(let i = 0; i < response_payloads.length; i++) {
                    logger.info(
                        "Payload " + i + " : " +
                        response_payloads[i].toString('utf8'));
                }
                return resolve(response_payloads);
            } else {
                var err = new Error('response_payloads is null');
                return makeErr("chain.queryByChainCode", reject, err);
            }
        },
        (err) => {
            return makeErr("chain.queryByChainCode", reject, err);
        });
    });
};


function processProposal (tx_id, eventhub, chain, results, proposalType) {
    var proposalResponses = results[0];
    // logger.debug(
    //     'deploy proposalResponses:'+JSON.stringify(proposalResponses));
    var proposal = results[1];
    var header = results[2];
    var all_good = true;
    for (var i in proposalResponses) {
        let one_good = false;
        if (   proposalResponses
            && proposalResponses[i].response
            && proposalResponses[i].response.status === 200) {

            one_good = true;
            logger.info(proposalType + ' proposal was good');
        } else {
            logger.error(proposalType + ' proposal was bad');
        }
        all_good = all_good & one_good;
        //FIXME:  App is supposed to check below things:
        // validate endorser certs, verify endorsement signature,
        // and compare the WriteSet among the responses
        // to make sure they are consistent across all endorsers.
        // SDK will be enhanced to make these checks easier to perform.
    }
    if (all_good) {
        if (proposalType == 'deploy') {
            logger.info(
                util.format(
                    'Successfully sent Proposal and received ProposalResponse:"'
                    + ' Status - %s, message - "%s", metadata - "%s", '
                    + 'endorsement signature: %s',
                    proposalResponses[0].response.status,
                    proposalResponses[0].response.message,
                    proposalResponses[0].response.payload,
                    proposalResponses[0].endorsement.signature));
        } else {
            logger.info('Successfully obtained transaction endorsements.');
        }
        var request = {
            proposalResponses: proposalResponses,
            proposal: proposal,
            header: header
        };

        // set the transaction listener and set a timeout of 30sec
        // if the transaction did not get committed within the timeout period,
        // fail the test
        var deployId = tx_id.toString();
        var txPromise = new Promise((resolve, reject) => {
                var handle = setTimeout(reject, 30000);
    eventhub.registerTxEvent(deployId, (tx) => {
                logger.info(
                        'The chaincode'
                        + (proposalType == 'deploy' ? proposalType: '')
                        + ' transaction has been successfully committed');
                clearTimeout(handle);
                eventhub.unregisterTxEvent(deployId);
                resolve();
            });
        });

        var sendPromise = chain.sendTransaction(request);
        return Promise.all([sendPromise, txPromise]).then((results) => {
            // the first returned value is from the 'sendPromise'
            // which is from the 'sendTransaction()' call
            return results[0];
        }).catch((err) => {
            logger.error(
                'Failed to send transaction and get notifications within '
                + 'the timeout period. ' + err.stack ? err.stack : err);
        });
    } else {
        logger.error(
                'Failed to send Proposal or receive valid response. '
                + 'Response null or status is not 200. exiting...');
        throw new Error('Problems happened when examining proposal responses');
    }
};

module.exports.invoke = function (fcn, args) {
    return new Promise((resolve, reject) => {

    var chaincodeId = ccConfig.chaincodeId;
    var chaincodeVersion = ccConfig.chaincodeVersion;
    var nonce = utils.getNonce();
    var tx_id = chain.buildTransactionID(nonce, user);

    // send proposal to endorser
    var request = {
        txId: tx_id,
        nonce: nonce,
        chainId: chain._name,
        chaincodeId: chaincodeId,
        chaincodeVersion: chaincodeVersion,
        fcn: fcn,
        args: args
    };
    chain.sendTransactionProposal(request)
    .then(
        (results) => {
            logger.info(
                'Successfully obtained proposal responses from endorsers');
            return processProposal(
                    tx_id, eventhub, chain, results, "invoke");
        }
    ).then(
        (response) => {
            if (response.status === 'SUCCESS') {
                logger.info(
                        'The chaincode transaction has been successfully '
                        + 'committed');
                return resolve(tx_id);
            }
        }
    ).catch(
        (err) => {
            eventhub.disconnect();
            logger.error(
                'Failed to invoke transaction due to error: ' +
                err.stack ? err.stack : err);
            return reject();
        });
    });
}


