var log_server = function () {};

log_server.call_log= function (path, fct,body){
	console.log("---------------------------\n Server: "+fct+"() called in "+path+" \n---------------------------\n");
	console.log("Arguments received  : ");
	console.log(body);    
	console.log();        
}
 

log_server.log_callback= function (error, results, path, fct){
	//console.log("Callback from"+fct+" : ");
    if (error) { 
        console.log("\n---------------------------");
        console.log("Error from callback :", error);
        console.log("---------------------------\n");
        throw error;
    }
    if (results === undefined)  results = "";
        console.log("\n---------------------------");
        console.log("Data received from function  :");
        console.log(JSON.stringify(results))
        console.log("---------------------------\n");
}

module.exports = log_server ; 
