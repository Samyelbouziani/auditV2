#!/bin/bash

# stop all fabric 
stopfabric
docker rm $(docker ps -a -q)
killall fabric-ca-server

# launch all fabric 
sleep 3
cd ~/ca/
fabric-ca-server start -b admin:adminpw &>/dev/null &
sleep 3
cd ~/labconfig/
labsetupsecurity
startfabric
docker ps -a
hlogin testuser block4ever
labcreatechannel
labjoinchannel
export CORE_PEER_MSPCONFIGPATH=~/.fabric-ca-client/msp
export CORE_PEER_LOCALMSPID=BlockchainCoCMSP
export CORE_PEER_ID=peer0


# instantiate cc
cd ~/src/dummy/fabric/script
./chaincode.sh