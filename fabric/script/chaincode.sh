#!/bin/bash

pubkCreditor="21"
pubkDebitor="921"
ics="ics2"
rum="ruom"  
creditorName="totp"
creditorAddress="ruefr"
amount="100"
periodicity="monthly"
lastTimeStamp="12decembre"

idMandat=$rum$ics


pubkCreditor2="32"
pubkDebitor2="84"
ics2="ics8"
rum2="rum4" 
creditorName2="alfer"
creditorAddress2="boulevard"
amount2="200"
periodicity2="monthly"
lastTimeStamp2="4decembre"

idMandat2=$rum2$ics2

# pubkCreditor2="734"
# pubkDebitor2="401"
# bankCreditor2="cic"
# bankDebitor2="goa"
# idMandat2=$pubkCreditor2$pubkDebitor2$bankCreditor2$bankDebitor2


rnd_cc=$RANDOM
echo "                                                          "+ $rnd_cc 

peer chaincode install -n $rnd_cc -v 1.0 -p dummy/fabric/chaincode/mandates
sleep 1
peer chaincode instantiate -o $(getip orderer0):7050 -n $rnd_cc -v 1.0 -C labchannel -c '{"Args":["init"]}' -P  "OR ('BlockchainCoCMSP.member')"
sleep 1
peer chaincode invoke -o $(getip orderer0):7050 -C labchannel -n $rnd_cc -c '{"Args":["createMandate","'$pubkCreditor'","'$pubkDebitor'","'$ics'","'$rum'","'$creditorName'","'$creditorAddress'","'$amount'","'$periodicity'"]}'
sleep 1
peer chaincode invoke -o $(getip orderer0):7050 -C labchannel -n $rnd_cc -c '{"Args":["createMandate","'$pubkCreditor2'","'$pubkDebitor2'","'$ics2'","'$rum2'","'$creditorName2'","'$creditorAddress2'","'$amount2'","'$periodicity2'"]}'
sleep 1
# echo "                                                       avant le kill  getMandate", $idMandat 
# peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getMandate", "'$idMandat'"]}'
# sleep 1
# echo "                                                         killMandate", $idMandat2
# peer chaincode invoke -o $(getip orderer0):7050 -C labchannel -n $rnd_cc -c '{"Args":["killMandate", "'$idMandat2'"]}'
# sleep 1
# echo "                                                       apres kill  getMandate", $idMandat2
 peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getAllMandate"]}'
# sleep 1
# # echo "                                                         getAllMandates", "allMandates " 
# # peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getAllMandates", "allMandates"]}'
# # sleep 1


# echo "                                                Avant fillMandate iban  getMandate", $idMandat 
# peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getMandate", "'$idMandat'"]}'
# sleep 1
# echo "                                                         DebitorFillMandate", $idMandat 
# peer chaincode invoke -o $(getip orderer0):7050 -C labchannel -n $rnd_cc -c '{"Args":["DebitorFillMandate", "'$idMandat'", "george", "allée", "simple_iban"]}'
# sleep 1
# echo "                                               Apres fillMandate  iban      getMandate", $idMandat
# peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getMandate", "'$idMandat'"]}'
# sleep 1
# echo "                                                         DebitorAmendMandate", $idMandat 
# peer chaincode invoke -o $(getip orderer0):7050 -C labchannel -n $rnd_cc -c '{"Args":["DebitorAmendMandate", "'$idMandat'", "iban", "super_iban"]}'
# sleep 1
# echo "                                                Apres DebitorAmendMandate  super_iban         getMandate", $idMandat
# peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getMandate", "'$idMandat'"]}'
# sleep 1




# echo "                                                         getAllMandates", "allMandates " 
# peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getAllMandates", "allMandates"]}'
# sleep 1
# echo "                                                         DebitorUpdatesMandate", $idMandat 
# peer chaincode invoke -o $(getip orderer0):7050 -C labchannel -n $rnd_cc -c '{"Args":["CreditorFillMandate", "'$idMandat'", "rum32", "ics32", "hash423"]}'
# sleep 1
# echo "                                                         getMandate", $idMandat
# peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["getMandate", "'$idMandat'"]}'
# sleep 1


#peer chaincode query -C labchannel -n $rnd_cc -c '{"Args":["DebitorUpdatesMandate", "1234", "totoa"]}'
