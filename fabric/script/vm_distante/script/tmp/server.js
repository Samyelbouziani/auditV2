var express = require('express');
var assert = require('assert');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
//var blockchain = require("./blockchain.js");

var app = express();
var router = express.Router();

var urls = ["http://13.81.119.129", "http://172.21.182.131", "http://127.0.0.1", "http://localhost"];
var ports = [":8100", ":3000", ":1337", ":80", ":81"];

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    var allowedOrigins = [
        urls[0]+ports[0], urls[1]+ports[0], urls[2]+ports[0], urls[3]+ports[0],
        urls[0]+ports[1], urls[1]+ports[1], urls[2]+ports[1], urls[3]+ports[1],
        urls[0]+ports[2], urls[1]+ports[2], urls[2]+ports[2], urls[3]+ports[2],
        urls[0]+ports[3], urls[1]+ports[3], urls[2]+ports[3], urls[3]+ports[3],
        urls[0]+ports[4], urls[1]+ports[4], urls[2]+ports[4], urls[3]+ports[4]
    ];
    var headers = 'Cache-Control, Pragma, Origin, X-Requested-With, Content-Type, Accept, Authorization';
    var origin = req.headers.origin;

    res.header('Access-Control-Allow-Origin', '*'); //urls[0]+ports[0]
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', headers);
    res.header('Access-Control-Allow-Credentials', true);

    return next();
}

//process.env.ADDRESS = "http://127.0.0.1";

app.use(bodyParser());
app.use(allowCrossDomain);
app.use(express.static(__dirname + '/public'));

var routes = require("./Node/routes.js")(app);

var server = http.createServer(app).listen(80);
server.on("error", function (err) {
    console.log(err);
    console.log("Cannot start server");
});

//app.listen(3000);
console.log("Server listening on port 8081");
console.log(" ");
console.log("!!!! Use : 'node server.js blockchain' to launch the fabric");
console.log(" ");



