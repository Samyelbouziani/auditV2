/*global angular*/
(function withAngular(angular) {
  'use strict';

  angular.module('httpServ.service', [])
  .service('httpServ', ['$q', '$http',

    function httpServ($q, $http) {

     var urlServer = "http://10.3.58.124";
     var jObj = jQuery.noConflict();

     var testServer = function testServer () {
            var _url = '/test';//'0.0.0.0:8080/blocks';

            return $http({
                method: 'GET',
                url: _url
            }).then(function successCallback(data, status, headers, config) {
               // console.log("testServer success : ");
               // console.log(data);

                return data;
            }, function errorCallback(data, status, headers, config) {
                console.log("testServer error : ");
                console.log(data);
                return data;
            });

     }, login = function login(_pubk, _prvtk) {
         var _url = urlServer + "/login";
         var object = JSON.stringify({ pubk: _pubk, prvtk: _prvtk});
         return $http.post(_url, object,{
             headers: {'Content-Type': 'application/json'},
             timeout: 0,
             cache: false
         }).success(function (response) {
             console.log("success login", response);
             return response;
         }).error(function (response) {
             console.log("error login", response);
         });

     }, getMandatesByCreditorPK = function getMandatesByCreditorPK (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/getMandatesByCreditorPK";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("getMandatesByCreditorPK success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("getMandatesByCreditorPK error : ");
                    console.log(data);
                    return data;
            });

     }, getMandatesByDebitorPK = function getMandatesByDebitorPK (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/getMandatesByDebitorPK";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("getMandatesByDebitorPK success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("getMandatesByDebitorPK error : ");
                    console.log(data);
                    return data;
            });

     }, getMandateById = function getMandateById (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/getMandateById";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("getMandateById success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("getMandateById error : ");
                    console.log(data);
                    return data;
            });

     }, getPeriodicity = function getPeriodicity (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/getPeriodicity";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("getPeriodicity success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("getPeriodicity error : ");
                    console.log(data);
                    return data;
            });

     }, getPossibleStatus = function getPossibleStatus (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/getPossibleStatus";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("getPossibleStatus success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("getPossibleStatus error : ");
                    console.log(data);
                    return data;
            });

     }, createMandate = function createMandate (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/createMandate";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("createMandate success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("createMandate error : ");
                    console.log(data);
                    return data;
            });

     }, debitorFillMandate = function debitorFillMandate (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/debitorFillMandate";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("debitorFillMandate success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("debitorFillMandate error : ");
                    console.log(data);
                    return data;
            });

     }, debitorAmendMandate = function debitorAmendMandate (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/debitorAmendMandate";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("debitorAmendMandate success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("debitorAmendMandate error : ");
                    console.log(data);
                    return data;
            });

     }, creditorRevokeMandate = function creditorRevokeMandate (object) {
            var data = JSON.stringify(object);
            var _url = urlServer + "/chaincode/creditorRevokeMandate";
            console.log("Data received by httpServ");
            console.log(object);
            return $http({
                method: 'POST',
                url: _url, 
                data: object
                }).then(function successCallback(data, status, headers, config) {
                    console.log("creditorRevokeMandate success : ");
                    console.log(data);
                    return data;
                }, function errorCallback(data, status, headers, config) {
                    console.log("creditorRevokeMandate error : ");
                    console.log(data);
                    return data;
            });


     }, resetDatabase = function resetDatabase () {
            //alert("resetDb called");
            var _url = urlServer + '/resetdb';

            return $http.post(_url,{
                    headers: {'Content-Type': 'application/json'},
                    timeout: 0,
                    cache: false
                }).success(function (data, status, headers, config) {
                    //alert("resetDatabase success HttpService : " + JSON.stringify(data));
                }).error(function (data, status, headers, config) {
                    //alert("resetDatabase error HttpService : " + JSON.stringify(data));
                });

     };



       
     return {
        'testServer': testServer,
        'login': login,
       /* 'getDataFromServer': getDataFromServer,
        'sendDataToServer': sendDataToServer,
        'getUserDataFromServer': getUserDataFromServer,
        'sendCredentialsToServer': sendCredentialsToServer,*/
        'resetDatabase': resetDatabase,
        'getMandatesByCreditorPK': getMandatesByCreditorPK, 
        'getMandatesByDebitorPK': getMandatesByDebitorPK, 
        'getMandateById': getMandateById, 
        'getPeriodicity': getPeriodicity, 
        'getPossibleStatus': getPossibleStatus, 
        'createMandate': createMandate, 
        'debitorFillMandate': debitorFillMandate, 
        'debitorAmendMandate': debitorAmendMandate, 
        'creditorRevokeMandate': creditorRevokeMandate

     };
  }]);
}(angular));
