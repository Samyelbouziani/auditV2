package main

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"testing"
)

func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

func checkState(t *testing.T, stub *shim.MockStub, expectedMandate Mandate) {

	// Getting mandate
	bytes := stub.State[expectedMandate.MandateId]
	if bytes == nil {
		fmt.Println("Could not get mandate with id", expectedMandate.MandateId)
		t.FailNow()
	}
	var storedMandate Mandate
	json.Unmarshal([]byte(bytes), &storedMandate)

	// checking equality between expected mandate and stored mandate
	// do not check timestamp
	expectedMandate.MandateData.Timestamp = storedMandate.MandateData.Timestamp
	if storedMandate != expectedMandate {
		fmt.Println("Did not get the expected mandate")
		t.FailNow()
	}
}

func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) []byte {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
	return res.Payload
}

func checkQuery(t *testing.T, stub *shim.MockStub, args [][]byte, expectedRes []byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Query", args[0], "failed", string(res.Message))
		t.FailNow()
	}
	if res.Payload == nil {
		fmt.Println("Query", args[0], "failed to get value")
		t.FailNow()
	}

	fmt.Println("\n\n" + string(res.Payload) + "\n\n")

	if string(args[0]) == "getMandateById" {
		if string(res.Payload) != string(expectedRes) {
			fmt.Println("Query", args[0], "failed, got wrong payload")
			t.FailNow()
		}
	}

	/*
		if string(res.Payload) != expectedRes {
			fmt.Println("Query value", args[0], "was not", expectedRes, "as expected")
			t.FailNow()
		}
	*/
}

func TestMandates_Init(t *testing.T) {
	scc := new(SimpleChaincode)
	stub := shim.NewMockStub("mandates", scc)

	// Init
	checkInit(t, stub, [][]byte{})
}

func TestMandates_Invoke(t *testing.T) {
	scc := new(SimpleChaincode)
	stub := shim.NewMockStub("mandates", scc)

	// Init
	checkInit(t, stub, [][]byte{})

	// Initializing test values
	testMandateStatus := "created"
	testCreditorPublicKey := "testCreditorPublicKey"
	testDebitorPublicKey := "testDebitorPublicKey"
	testOwner := "testOwner"
	testRum := "testRUM"
	testAmount := "123.45"
	testAmountFloat64 := 123.45
	testPeriodicity := "monthly"
	testLocation := "testLocation"
	testIcs := "testIcs"
	testCreditorBic := "testCreditorBic"
	testCreditorName := "testCreditorName"
	testCreditorAddress := "testCreditorAddress"
	testIban := "testIban"
	testDebitorBic := "testDebitorBic"
	testDebitorName := "testDebitorName"
	testDebitorAddress := "testDebitorAddress"

	// Generate testMandateId
	// TODO: TO BE REMOVED when the mandateId is randomly generated
	var testMandateId string
	h := sha256.New()
	h.Write([]byte(testIcs + testRum))
	testMandateId = fmt.Sprintf("%x", h.Sum(nil))
	fmt.Println("testMandateId", testMandateId)

	testMandateData := MandateData{
		Owner:       testOwner,
		Rum:         testRum,
		Amount:      testAmountFloat64,
		Periodicity: testPeriodicity,
		Location:    testLocation}
	testCreditorData := CreditorData{
		testIcs,
		testCreditorBic,
		testCreditorName,
		testCreditorAddress}
	testMandate := Mandate{
		testMandateId,
		testMandateStatus,
		testCreditorPublicKey,
		testDebitorPublicKey,
		testMandateData,
		testCreditorData,
		DebitorData{}}

	// Invoke create
	resBytes := checkInvoke(t, stub, [][]byte{
		[]byte("createMandate"),
		[]byte(testDebitorPublicKey),
		[]byte(testCreditorPublicKey),
		[]byte(testIcs),
		[]byte(testCreditorBic),
		[]byte(testCreditorName),
		[]byte(testCreditorAddress),
		[]byte(testOwner),
		[]byte(testRum),
		[]byte(testAmount),
		[]byte(testPeriodicity),
		[]byte(testLocation)})
	checkState(t, stub, testMandate)

	/*
		// recreate a mandate from the result of createMandate in
		// order to get the timestamp
		var resMandate Mandate
		err = json.Unmarshal(resBytes, resMandate)
		if err != nil {
			fmt.Println("Could not recreate Mandate from result of createMandate")
			t.FailNow()
		}
	*/

	// Query getMandateById
	checkQuery(t, stub,
		[][]byte{
			[]byte("getMandateById"),
			[]byte(testMandateId)},
		resBytes)

	// Invoke debitorFillMandate
	checkInvoke(t, stub, [][]byte{
		[]byte("debitorFillMandate"),
		[]byte(testMandateId),
		[]byte(testIban),
		[]byte(testDebitorBic),
		[]byte(testDebitorName),
		[]byte(testDebitorAddress)})
	testDebitorData := DebitorData{testIban, testDebitorBic, testDebitorName, testDebitorAddress}
	testMandate.DebitorData = testDebitorData
	testMandateStatus = "validated"
	testMandate.MandateStatus = testMandateStatus
	checkState(t, stub, testMandate)

	checkInvoke(t, stub, [][]byte{
		[]byte("debitorEditMandate"),
		[]byte(testMandateId),
		[]byte("Iban"),
		[]byte("newTestIban")})
	testMandate.DebitorData.Iban = "newTestIban"
	checkState(t, stub, testMandate)

	checkInvoke(t, stub, [][]byte{
		[]byte("debitorAmendMandate"),
		[]byte(testMandateId),
		[]byte("EDITED_iban"),
		[]byte("EDITED_bic"),
		[]byte("EDITED_address")})
	testMandate.DebitorData.Iban = "EDITED_iban"
	testMandate.DebitorData.Bic = "EDITED_bic"
	testMandate.DebitorData.Address = "EDITED_address"
	checkState(t, stub, testMandate)

	// Invoke revokeMandate
	checkInvoke(t, stub, [][]byte{[]byte("revokeMandate"), []byte(testMandateId)})
	testMandateStatus = "revoked"
	testMandate.MandateStatus = testMandateStatus
	checkState(t, stub, testMandate)

	/*
	 */
}
