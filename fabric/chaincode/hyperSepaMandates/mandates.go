package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"strconv"
	"time"
)

type SimpleChaincode struct {
}

var possibleStatus = map[string]string{
	"created":   "mandate created by the creditor",
	"validated": "mandate validated by debitor",
	"revoked":   "mandate revoked",
}

type MandateData struct {
	Owner       string  `json:"Owner"`
	Rum         string  `json:"Rum"`
	Amount      float64 `json:"Amount"`
	Periodicity string  `json:"Periodicity"`
	Timestamp   int32   `json:"Timestamp,string"`
	Location    string  `json:"Location"`
}

var possiblePeriodicity = [...]string{"monthly", "yearly"}

type CreditorData struct {
	Ics     string `json:"Ics"`
	Bic     string `json:"Bic"`
	Name    string `json:"Name"`
	Address string `json:"Address"`
}

type DebitorData struct {
	Iban    string `json:"Iban"`
	Bic     string `json:"Bic"`
	Name    string `json:"Name"`
	Address string `json:"Address"`
}

type Mandate struct {
	MandateId         string       `json:"MandateId"`
	MandateStatus     string       `json:"MandateStatus"`
	CreditorPublicKey string       `json:"CreditorPublicKey"`
	DebitorPublicKey  string       `json:"DebitorPublicKey"`
	MandateData       MandateData  `json:"MandateData"`
	CreditorData      CreditorData `json:"CreditorData"`
	DebitorData       DebitorData  `json:"DebitorData"`
}

// =============================================================================
// Main
// =============================================================================
func main() {
	fmt.Printf("START CHAINCODE!")
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// Init
// ===========================
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke
// ===========================
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke is running " + function)

	if function == "createMandate" { // create a mandate
		return t.createMandate(stub, args)
	} else if function == "getMandateById" { // get a mandate
		return t.getMandateById(stub, args)
	} else if function == "revokeMandate" { // delete a mandate
		return t.revokeMandate(stub, args)
	} else if function == "debitorFillMandate" { // filling debitor info
		return t.debitorFillMandate(stub, args)
	} else if function == "debitorEditMandate" { // edit one info of the mandate
		return t.debitorEditMandate(stub, args)
	} else if function == "debitorAmendMandate" { // amend all info of the mandate
		return t.debitorAmendMandate(stub, args)
	} else if function == "getMandatesByDebitorPk" {
		return t.getMandatesByDebitorPk(stub, args)
	} else if function == "getMandatesByCreditorPk" {
		return t.getMandatesByCreditorPk(stub, args)
	} else if function == "getPossibleStatus" {
		return t.getPossibleStatus()
	}

	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error(fmt.Sprintf(
		"Received unknown function invocation ! (%s)", function))
}

func checkArgsEmpty(nbArgs int, args []string) error {

	if len(args) != nbArgs {
		return errors.New(fmt.Sprintf(
			"Incorrect number of arguments, expecting %d arguments", nbArgs))
	}
	for i := 0; i < nbArgs; i++ {
		if len(args[i]) <= 0 {
			return errors.New(fmt.Sprintf(
				"Argument %d must be a non-empty string"))
		}
	}
	return nil
}

// ==================================================================
// createMandate - create a new mandate, store into chaincode state
// ==================================================================
func (t *SimpleChaincode) createMandate(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 11
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** createMandate Start ***")

	// parsing string args
	debitorPk := args[0]
	creditorPk := args[1]
	ics := args[2]
	creditorBic := args[3]
	creditorName := args[4]
	creditorAddress := args[5]
	owner := args[6]
	rum := args[7]
	periodicity := args[9]
	location := args[10]

	// parsing amount
	amount, err := strconv.ParseFloat(args[8], 64)
	if err != nil {
		return shim.Error(fmt.Sprintf(
			"Wrong mandate amount, expecting float, got %s", args[6]))
	}

	// Generating timestamp
	var timestamp int32
	timestamp = int32(time.Now().Unix())

	// Generating unique mandate id
	var mandateId string
	h := sha256.New()
	h.Write([]byte(ics + rum))
	mandateId = fmt.Sprintf("%x", h.Sum(nil))

	// Initializing data structures
	mandateData := MandateData{
		owner,
		rum,
		amount,
		periodicity,
		timestamp,
		location}

	creditorData := CreditorData{
		ics,
		creditorBic,
		creditorName,
		creditorAddress}

	mandate := Mandate{
		MandateId:         mandateId,
		CreditorPublicKey: creditorPk,
		DebitorPublicKey:  debitorPk,
		MandateData:       mandateData,
		CreditorData:      creditorData}

	err = changeMandateStatus(&mandate, "created")
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("MANDATE: ", mandate.MandateStatus)

	mandateJSONasBytes, err := json.Marshal(mandate)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(mandateJSONasBytes)
}

// =============================================================
// getMandateById - get a mandate from chaincode state by its id
// =============================================================
func (t *SimpleChaincode) getMandateById(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var mandateId, jsonResp string
	var err error

	if len(args) != 1 || len(args[0]) == 0 {
		return shim.Error(
			"Incorrect arguments, expecting id of the mandate to query")
	}

	mandateId = args[0]

	valAsBytes, err := stub.GetState(mandateId) // get the mandate from world state
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + mandateId + "\"}"
		return shim.Error(jsonResp)
	} else if valAsBytes == nil {
		jsonResp = "{\"Error\":\"Mandate does not exist: " + mandateId + "\"}"
		return shim.Error(jsonResp)
	}

	return shim.Success(valAsBytes)
}

// ========================================================================
// getPossibleStatus - get a list of possible status with their description
// ========================================================================
func (t *SimpleChaincode) getPossibleStatus() pb.Response {

	possibleStatusJSONasBytes, err := json.Marshal(possibleStatus)
	if err != nil {
		shim.Error("Failed to convert possibleStatus into JSON")
	}

	return shim.Success(possibleStatusJSONasBytes)
}

// Auxiliary function to check and change status
func changeMandateStatus(mandate *Mandate, newStatus string) error {

	if len(newStatus) == 0 {
		return errors.New(fmt.Sprintf("Wrong status, new status is empty"))
	}

	// Checking status
	var found string
	for k := range possibleStatus {
		if k == newStatus {
			found = k
			break
		}
	}
	if found == "" {
		return errors.New(fmt.Sprintf(
			"Wrong status, %s is not an accepted status", newStatus))
	}

	(*mandate).MandateStatus = found

	return nil
}

// ==================================================
// revokeMandate - remove a mandate from state
// ==================================================
func (t *SimpleChaincode) revokeMandate(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 1 || len(args[0]) == 0 {
		return shim.Error(
			"Incorrect arguments, expecting id of the mandate to revoke")
	}

	mandateId := args[0]

	// Get Mandate from world state
	mandateAsBytes, err := stub.GetState(mandateId)
	if err != nil {
		return shim.Error("Failed to get mandate:" + err.Error())
	} else if mandateAsBytes == nil {
		return shim.Error("Mandate does not exist")
	}
	var mandateToUpdate Mandate
	err = json.Unmarshal(mandateAsBytes, &mandateToUpdate)
	if err != nil {
		return shim.Error(err.Error())
	}

	// Update status
	changeMandateStatus(&mandateToUpdate, "revoked")

	// Write Mandate back
	mandateJSONasBytes, err := json.Marshal(mandateToUpdate)

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** revoqueMandate successful ***")
	return shim.Success(nil)
}

// =============================================================
// debitorFillMandate - fill data from chaincode by debitor
// =============================================================
func (t *SimpleChaincode) debitorFillMandate(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 5
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** debitorFillMandate start ***")

	mandateId := args[0]
	iban := args[1]
	bic := args[2]
	name := args[3]
	address := args[4]

	debitorData := DebitorData{
		iban,
		bic,
		name,
		address}

	// Get Mandate from world state
	mandateAsBytes, err := stub.GetState(mandateId)
	if err != nil {
		return shim.Error("Failed to get mandate:" + err.Error())
	} else if mandateAsBytes == nil {
		return shim.Error("Mandate does not exist")
	}
	var mandateToUpdate Mandate
	err = json.Unmarshal(mandateAsBytes, &mandateToUpdate)
	if err != nil {
		return shim.Error(err.Error())
	}

	// Update DebitorData
	mandateToUpdate.DebitorData = debitorData
	// Update status
	changeMandateStatus(&mandateToUpdate, "validated")

	// Write Mandate back
	mandateJSONasBytes, err := json.Marshal(mandateToUpdate)

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** debitorFillMandate successful ***")
	return shim.Success(mandateJSONasBytes)
}

// =============================================================
// debitorEditMandate - modify data from chaincode by debitor
// =============================================================

func (t *SimpleChaincode) debitorEditMandate(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 3
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}

	mandateId := args[0]
	key := args[1]
	newVal := args[2]

	fmt.Println("*** debitorEditMandate start ***")

	mandateAsBytes, err := stub.GetState(mandateId)
	if err != nil {
		return shim.Error("Failed to get mandate:" + err.Error())
	} else if mandateAsBytes == nil {
		return shim.Error("Mandate does not exist")
	}

	var mandateToUpdate Mandate
	err = json.Unmarshal(mandateAsBytes, &mandateToUpdate)
	if err != nil {
		return shim.Error(err.Error())
	}

	if key == "Iban" {
		mandateToUpdate.DebitorData.Iban = newVal
	}
	if key == "Bic" {
		mandateToUpdate.DebitorData.Bic = newVal
	}
	if key == "Name" {
		mandateToUpdate.DebitorData.Name = newVal
	}
	if key == "Address" {
		mandateToUpdate.DebitorData.Address = newVal
	}
	mandateJSONasBytes, err := json.Marshal(mandateToUpdate)

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** debitorEditMandate successful ***")

	return shim.Success(mandateJSONasBytes)
}

// =============================================================
// debitorAmendMandate - modify all data from chaincode by debitor
// =============================================================

func (t *SimpleChaincode) debitorAmendMandate(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 4
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}

	mandateId := args[0]
	iban := args[1]
	bic := args[2]
	address := args[3]

	fmt.Println("*** debitorAmendMandate start ***")

	mandateAsBytes, err := stub.GetState(mandateId)
	if err != nil {
		return shim.Error("Failed to get mandate:" + err.Error())
	} else if mandateAsBytes == nil {
		return shim.Error("Mandate does not exist")
	}

	var mandateToUpdate Mandate
	err = json.Unmarshal(mandateAsBytes, &mandateToUpdate)
	if err != nil {
		return shim.Error(err.Error())
	}

	mandateToUpdate.DebitorData.Iban = iban
	mandateToUpdate.DebitorData.Bic = bic
	mandateToUpdate.DebitorData.Address = address

	mandateJSONasBytes, err := json.Marshal(mandateToUpdate)

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** debitorAmendMandate successful ***")

	return shim.Success(mandateJSONasBytes)
}

// Auxiliary function to get mandates by any attribute
// (Not a function visible from outside the Chaincode)
func getMandatesByAttribute(
	stub shim.ChaincodeStubInterface,
	args []string,
	attributeName string) pb.Response {

	var err error

	const nbArgs = 1
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}
	attributeValue := string(args[0])

	queryString := fmt.Sprintf("{\"selector\":{\"%s\":\"%s\"}}", attributeName, attributeValue)

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

// ===========================================================
// getMandatesByDebitorPk - get Mandates by Debitor Public Key
// ===========================================================
func (t *SimpleChaincode) getMandatesByDebitorPk(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	return getMandatesByAttribute(stub, args, "DebitorPublicKey")
}

// ===========================================================
// getMandatesByCreditorPk - get Mandates by Creditor Public Key
// ===========================================================
func (t *SimpleChaincode) getMandatesByCreditorPk(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	return getMandatesByAttribute(stub, args, "CreditorPublicKey")
}

// ============================================================================
// getQueryResultForQueryString - executes the passed in query string.
// Result set is built and returned as a byte array containing the JSON results
// ============================================================================
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		fmt.Println("NOT OKAY")
		return nil, err
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryRecords
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResultKey, queryResultRecord, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResultKey)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResultRecord))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}
