package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"strconv"
	"time"
)

type SimpleChaincode struct {
}

type MandateData struct {
	Owner       string  `json:"Owner"`
	Rum         string  `json:"Rum"`
	Amount      float64 `json:"Amount"`
	Periodicity string  `json:"Periodicity"`
	Timestamp   int32   `json:"Timestamp"`
}

var possiblePeriodicity = [...]string{"monthly", "yearly"}

type CreditorData struct {
	Ics     string `json:"Ics"`
	Bic     string `json:"Bic"`
	Name    string `json:"Name"`
	Address string `json:"Address"`
}

type DebitorData struct {
	Iban    string `json:"Iban"`
	Name    string `json:"Name"`
	Address string `json:"Address"`
}

type Mandate struct {
	MandateId         string       `json:"MandateId"`
	CreditorPublicKey string       `json:"CreditorPublicKey"`
	DebitorPublicKey  string       `json:"DebitorPublicKey"`
	MandateData       MandateData  `json:"MandateData"`
	CreditorData      CreditorData `json:"CreditorData"`
	DebitorData       DebitorData  `json:"DebitorData"`
	Object            string       `json:"Object"`
}

// =============================================================================
// Main
// =============================================================================
func main() {
	fmt.Printf("START CHAINCODE!")
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// Init
// ===========================
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke
// ===========================
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke is running " + function)

	if function == "createMandate" { // create a mandate
		return t.createMandate(stub, args)
	} else if function == "getMandateById" { // get a mandate
		return t.getMandateById(stub, args)
	} else if function == "revokeMandate" { // delete a mandate
		return t.revokeMandate(stub, args)
	} else if function == "debitorFillMandate" { // filling debitor info
		return t.debitorFillMandate(stub, args)
	} else if function == "debitorAmendMandate" { // amendment of the mandate
		return t.debitorAmendMandate(stub, args)
	} else if function == "getMandatesByDebitorPk" {
		return t.getMandatesByDebitorPk(stub, args)
	} else if function == "getAllMandate" {
		return t.getAllMandate(stub, args)
	}

	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error(fmt.Sprintf(
		"Received unknown function invocation ! (%s)", function))
}

func checkArgsEmpty(nbArgs int, args []string) error {

	if len(args) != nbArgs {
		return errors.New(fmt.Sprintf(
			"Incorrect number of arguments, expecting %d arguments", nbArgs))
	}
	for i := 0; i < nbArgs; i++ {
		if len(args[i]) <= 0 {
			return errors.New(fmt.Sprintf(
				"Argument %d must be a non-empty string"))
		}
	}
	return nil
}

// ==================================================================
// createMandate - create a new mandate, store into chaincode state
// ==================================================================
func (t *SimpleChaincode) createMandate(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 10
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** createMandate Start ***")

	// parsing string args
	debitorPk := args[0]
	creditorPk := args[1]
	ics := args[2]
	creditorName := args[3]
	creditorAddress := args[4]
	rum := args[5]
	periodicity := args[7]
	bic := args[8]	
	owner := args[9]
	
	// Checking periodicity
	found := false
	for _, v := range possiblePeriodicity {
		if v == periodicity {
			found = true
			break
		}
	}
	if !found {
		return shim.Error(fmt.Sprintf(
			"Wrong periodicity, %s is not an accepted periodicity", periodicity))
	}

	// parsing amount
	amount, err := strconv.ParseFloat(args[6], 64)
	if err != nil {
		return shim.Error(fmt.Sprintf(
			"Wrong mandate amount, expecting float, got %s", args[6]))
	}

	// Generating timestamp
	var timestamp int32
	timestamp = int32(time.Now().Unix())

	// Generating unique mandate id
	var mandateId string
	h := sha256.New()
	h.Write([]byte(ics + rum))
	mandateId = fmt.Sprintf("%x", h.Sum(nil))


	// Initializing data structures
	mandateData := MandateData{
		owner,	
		rum,
		amount,
		periodicity,
		timestamp}

	creditorData := CreditorData{
		ics,
		bic,
		creditorName,
		creditorAddress}
    
    var object string 
	object = "mandat"

	mandate := Mandate{
		MandateId:         mandateId,
		CreditorPublicKey: creditorPk,
		DebitorPublicKey:  debitorPk,
		MandateData:       mandateData,
		CreditorData:      creditorData,
		Object:            object	}

	mandateJSONasBytes, err := json.Marshal(mandate)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(mandateJSONasBytes)
}

// =============================================================
// getMandateById - get a mandate from chaincode state by its id
// =============================================================
func (t *SimpleChaincode) getMandateById(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var mandateId, jsonResp string
	var err error

	if len(args) != 1 || len(args[0]) == 0 {
		return shim.Error(
			"Incorrect arguments, expecting id of the mandate to query")
	}

	mandateId = args[0]

	valAsBytes, err := stub.GetState(mandateId) // get the mandate from world state
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + mandateId + "\"}"
		return shim.Error(jsonResp)
	} else if valAsBytes == nil {
		jsonResp = "{\"Error\":\"Mandate does not exist: " + mandateId + "\"}"
		return shim.Error(jsonResp)
	}

	return shim.Success(valAsBytes)
}

// =============================================================
// getAllMandate - get all mandate from chaincode state by object "mandat"
// =============================================================
func (t *SimpleChaincode) getAllMandate(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	object := "mandat"
	queryString := fmt.Sprintf("{\"selector\":{\"Object\":\"%s\"}}", object)

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}


// ==================================================
// revokeMandate - remove a mandate from state
// ==================================================
func (t *SimpleChaincode) revokeMandate(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 1 || len(args[0]) == 0 {
		return shim.Error(
			"Incorrect arguments, expecting id of the mandate to revoke")
	}

	mandateId := args[0]

	err := stub.DelState(mandateId)
	if err != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

// =============================================================
// debitorFillMandate - fill data from chaincode by debitor
// =============================================================
func (t *SimpleChaincode) debitorFillMandate(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 4
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** debitorFillMandate start ***")

	mandateId := args[0]
	iban := args[1]
	name := args[2]
	address := args[3]

	debitorData := DebitorData{
		iban,
		name,
		address}

	mandateAsBytes, err := stub.GetState(mandateId)
	if err != nil {
		return shim.Error("Failed to get mandate:" + err.Error())
	} else if mandateAsBytes == nil {
		return shim.Error("Mandate does not exist")
	}
	var mandateToUpdate Mandate
	err = json.Unmarshal(mandateAsBytes, &mandateToUpdate)
	if err != nil {
		return shim.Error(err.Error())
	}

	mandateToUpdate.DebitorData = debitorData

	mandateJSONasBytes, err := json.Marshal(mandateToUpdate)

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("*** debitorFillMandate successful ***")
	return shim.Success(mandateJSONasBytes)
}

// =============================================================
// debitorAmendMandate - modify data from chaincode by debitor
// =============================================================

func (t *SimpleChaincode) debitorAmendMandate(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 3
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}

	mandateId := args[0]
	key := args[1]
	newVal := args[2]

	fmt.Println("*** debitorAmendMandate start ***")

	mandateAsBytes, err := stub.GetState(mandateId)
	if err != nil {
		return shim.Error("Failed to get mandate:" + err.Error())
	} else if mandateAsBytes == nil {
		return shim.Error("Mandate does not exist")
	}

	var mandateToUpdate Mandate
	err = json.Unmarshal(mandateAsBytes, &mandateToUpdate)
	if err != nil {
		return shim.Error(err.Error())
	}

	if key == "Iban" {
		mandateToUpdate.DebitorData.Iban = newVal
	}
	if key == "Name" {
		mandateToUpdate.DebitorData.Name = newVal
	}
	if key == "Address" {
		mandateToUpdate.DebitorData.Address = newVal
	}
	mandateJSONasBytes, err := json.Marshal(mandateToUpdate)

	err = stub.PutState(mandateId, mandateJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("*** debitorAmendMandate successful ***")

	return shim.Success(mandateJSONasBytes)
}

func (t *SimpleChaincode) getMandatesByDebitorPk(
	stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	const nbArgs = 1
	if checkArgsEmpty(nbArgs, args) != nil {
		return shim.Error(err.Error())
	}
	debitorPK := string(args[0])

	queryString := fmt.Sprintf("{\"selector\":{\"DebitorPublicKey\":\"%s\"}}", debitorPK)

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

// =========================================================================================
// getQueryResultForQueryString executes the passed in query string.
// Result set is built and returned as a byte array containing the JSON results.
// =========================================================================================
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		fmt.Println("NOT OKAY")
		return nil, err
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryRecords
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResultKey, queryResultRecord, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResultKey)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResultRecord))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}

// =============================================================
// CreditorFillMandate - modify data from chaincode by creditor
// =============================================================
/*func (t *SimpleChaincode) CreditorFillMandate(stub shim.ChaincodeStubInterface, args []string) pb.Response {

    if len(args) < 4 {
        return shim.Error("Incorrect number of arguments. Expecting 4")
    }


    mandateId   := args[0]
    rum         := args[1]
    ics         := args[2]
    hash        := args[3]

    fmt.Println("- start CreditorFillMandate ", mandateId)


    mandateAsBytes, err := stub.GetState(mandateId)
    if err != nil {
        return shim.Error("Failed to get mandate:" + err.Error())
    } else if mandateAsBytes == nil {
        return shim.Error("Mandate does not exist")
    }
    var mandateToUpdate mandate
    err = json.Unmarshal(mandateAsBytes, &mandateToUpdate)
    if err != nil {
        return shim.Error(err.Error())
    }
    mandateToUpdate.Rum = rum
    mandateToUpdate.Ics = ics
    mandateToUpdate.HashNameAddress = hash

    //mandateToUpdate.MandateId = mandateId+"2"

    mandateJSONasBytes, err := json.Marshal(mandateToUpdate)

    err = stub.PutState(mandateId, mandateJSONasBytes)
    if err != nil {
        return shim.Error(err.Error())
    }

/*
    object := "allMandates"
    allMandatesasBytes, err := stub.GetState(object)
        if err != nil {
            return shim.Error(err.Error())
        }
    var all_m allMandates
    json.Unmarshal(allMandatesasBytes, &all_m)

    all_m.Mandat = append(all_m.Mandat, (*mndt));
    fmt.Println("! appended open to all_m")
    jsonAsBytes, err := json.Marshal(all_m)
    err = stub.PutState(object, jsonAsBytes)
        if err != nil {
            return shim.Error(err.Error())
        }
*/

/*
    fmt.Println("- end DebitorUpdatesMandate (success)")
    return shim.Success(mandateJSONasBytes)
}
*/
